(asdf:defsystem :cl-depict
  :version "0.0.1"
  :licence "MITish/BSD-ish"
  :description "cl-depict is crude drawing tool and graphic toolkit."
  :long-description
  "cl-depict someday could be used to create some graphics."
  :depends-on (:bordeaux-threads :clx :chanl :bordeaux-threads)
  :components
  ((:static-file "LICENCE")
   (:file package)
   (:file util)
   (:file xdk)
   (:file xdk-window)
   (:file xdk-event)
   (:file main)
   ))

(defmethod operation-done-p ((o test-op) (c (eql (find-system :cl-depict))))
  nil)

(defmethod perform ((o test-op) (c (eql (find-system :cl-depict))))
)
