(in-package :cl-depict)

(defun test-graphic-x ()
  (display-init)
  (graphic-x 400 400 100 100))


(defvar *top-window* nil)
(defvar *red-window* nil)
(defvar *green-window* nil)
(defvar *blue-window* nil)

;; (defun process-events-function (display)
;;   (loop for event =
;;        (xlib:process-event (clx-port-display port)
;; 			   :timeout timeout
;; 			   :handler #'event-handler
;; 			   :discard-p t)
;;        while event
;;        do (format t "~s"  event))



(defun handle-event-interactive (event)
  (xlib:event-case (display :force-output-p t
			    :discard-p t)
    (:button-press (window)
		   (cond ((eq window *red-window*)
			  (xlib:destroy-window *red-window*)
			  nil)
			 ((eq window *green-window*)
			  (xlib:destroy-window *blue-window*)
			  nil)
			 ((eq window *blue-window*)
			  (xlib:destroy-window *green-window*)
			  nil)
			 (t t)))
    (:key-press ()
		(xlib:circulate-window-down *top-window*)
		nil)))

(defun process-events-function2 (display)
  (xlib:event-case (display :force-output-p t
			    :discard-p t)
    (:button-press (window)
		   (cond ((eq window *red-window*)
			  (xlib:destroy-window *red-window*)
			  nil)
			 ((eq window *green-window*)
			  (xlib:destroy-window *blue-window*)
			  nil)
			 ((eq window *blue-window*)
			  (xlib:destroy-window *green-window*)
			  nil)
			 (t t)))
    (:key-press ()
		(xlib:circulate-window-down *top-window*)
		nil)))



(defun create-global-test-vars ()
  (let ((vars '(width height across down)))
    (dolist (var vars)
      (apply 'defparameter (list var 400)))))



(defun graphic-x (width height across down &optional (host ""))
  (setf *top-window* (create-top-window 400 400))
  (setf *red-window* (create-child-window *top-window*
					  (truncate width 4)
					  (truncate height 4)
					  across
					  0
					  :background 'red))
  (setf *green-window* (create-child-window *top-window*
					    (truncate width 4)
					    (truncate height 4)
					    0
					    down
					    :background 'green))
  (setf *blue-window* (create-child-window *top-window*
					   (truncate width 4)
					   (truncate height 4)
					   across
					   down
					   :background 'blue
					   :border-width 5
					   :border 'white))
  (xlib:map-window *top-window*)
  (xlib:map-window *red-window*)
  (xlib:map-window *green-window*)
  (xlib:map-window *blue-window*)
    ;;; (process-events-function2 *display*)
  (bordeaux-threads:join-thread (bordeaux-threads:make-thread #'
				 (lambda ()
				   (setf *channel* (make-instance 'chanl:unbounded-channel))
				   (process-events-function2 *display*))))
  (display-exit))


;; (defclass event-stream (fundamental-stream)
;;   ((event-buffer :initform '() :initarg :stream :reader event-buffer)))

;; (defclass changed-stream (fundamental-character-input-stream)
;;   ((stream :initarg :stream
;; 	   :reader stream-of)
;;    (virtual-position :initform 0
;; 		     :accessor virtual-position)
;;    (last-unchanged-position :initarg :last-unchanged-position
;; 			    :reader last-unchanged-position)
;;    (last-replacement-position :initarg :last-replacement-position
;; 			      :reader last-replacement-position)
;;    (last-modified-position :initarg :last-modified-position
;; 			   :reader last-modified-position)
;;    (removed-characters :initarg :removed-characters
;; 		       :reader removed-characters)
;;    (insert-string :initarg :string
;; 		  :reader insert-string)))


;; (defclass counting-character-input-stream
;;     (wrapped-character-input-stream)
;;   ((char-count :initform 1 :accessor char-count-of)
;;    (line-count :initform 1 :accessor line-count-of)
;;    (col-count :initform 1 :accessor col-count-of)
;;    (prev-col-count :initform 1 :accessor prev-col-count-of)))

;; (defmethod stream-read-char ((stream counting-character-input-stream))
;;   (with-accessors ((inner-stream stream-of) (chars char-count-of)
;; 		   (lines line-count-of) (cols col-count-of)
;; 		   (prev prev-col-count-of)) stream
;;     (let ((char (call-next-method)))
;;       (cond ((eql char :eof)
;; 	     :eof)
;; 	    ((char= char #\Newline)
;; 	     (incf lines)
;; 	     (incf chars)
;; 	     (setf prev cols)
;; 	     (setf cols 1)
;; 	     char)
;; 	    (t
;; 	     (incf chars)
;; 	     (incf cols)
;; 	     char)))))

;; (defmethod stream-unread-event ((stream event-stream) event)
;;   (with-accessors ((inner-stream stream-of) (chars char-count-of)
;; 		   (lines line-count-of) (cols col-count-of)
;; 		   (prev prev-col-count-of)) stream
;;     (cond ((char= char #\Newline)
;; 	   (decf lines)
;; 	   (decf chars)
;; 	   (setf cols prev))
;; 	  (t
;; 	   (decf chars)
;; 	   (decf cols)
;; 	   char))
;;     (call-next-method)))


;; (defclass counting-character-input-stream
;; (wrapped-character-input-stream)
;; ((char-count :initform 1 :accessor char-count-of)
;; (line-count :initform 1 :accessor line-count-of)
;; (col-count :initform 1 :accessor col-count-of)
;; (prev-col-count :initform 1 :accessor prev-col-count-of)))
;; (defmethod stream-read-char ((stream counting-character-input-stream))
;; (with-accessors ((inner-stream stream-of) (chars char-count-of)
;; (lines line-count-of) (cols col-count-of)
;; (prev prev-col-count-of)) stream
;; (let ((char (call-next-method)))
;; (cond ((eql char :eof)
;; :eof)
;; ((char= char #\Newline)
;; (incf lines)
;; (incf chars)
;; (setf prev cols)
;; (setf cols 1)
;; char)
;; (t
;; (incf chars)
;; (incf cols)
;; char)))))
;; (defmethod stream-unread-char ((stream counting-character-input-stream)
;; char)
;; (with-accessors ((inner-stream stream-of) (chars char-count-of)
;; (lines line-count-of) (cols col-count-of)
;; (prev prev-col-count-of)) stream
;; (cond ((char= char #\Newline)
;; (decf lines)
;; (decf chars)
;; (setf cols prev))
;; (t
;; (decf chars)
;; (decf cols)
;; char))
;; (call-next-method)))



;; (defgeneric write-prefix (prefix stream)
;; (:method ((prefix string) stream) (write-string prefix stream))
;; (:method ((prefix function) stream) (funcall prefix stream)))
;; (defmethod stream-write-char ((stream prefixed-character-output-stream)
;; char)
;; (with-accessors ((inner-stream stream-of) (cols col-index-of)
;; (prefix prefix-of)) stream
;; (when (zerop cols)
;; (write-prefix prefix inner-stream))
;; (call-next-method)))
