/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*  Checkerboard is a filter which takes in an image and completely
 *  replace it with a checkerboard of varying widths and heights.
 *
 *  Copyright (C) 1996 Gene Kan and Spencer Kimball.
 *  genehkan@xcf.berkeley.edu, spencer@xcf.berkeley.edu
 */

/* Checkerboard */
#include "gimp.h"

#include <stdio.h>

/* Declare a local function.
 */
static void checkerboard (Image, Image, int, int);

static void scaleCallback( int itemID, void *clientData, void *callData);
static void toggleCallback( int itemID, void *clientData, void *callData);
static void okCallback( int itemID, void *clientData, void *callData);
static void cancelCallback( int itemID, void *clientData, void *callData);

static char *prog_name;

static int dialog_id;

static struct values
{
  int xthickness, ythickness;
  int maintainAspect;
} Values;

static int scaleXID, scaleYID;

Image input, output;

int
main (argc, argv)
     int argc;
     char **argv;
   {
   int row_id;
   int toggleID;
   void *data;

   /* Save the program name so we can use it later in reporting errors
    */
   prog_name = argv[0];

   /* Call 'gimp_init' to initialize this filter.
    * 'gimp_init' makes sure that the filter was properly called and
    *  it opens pipes for reading and writing.
    */
   if (gimp_init (argc, argv))
      {
      /* This is a regular filter. What that means is that it operates
       *  on the input image. Output is put into the ouput image. The
       *  filter should not worry, or even care where these images come
       *  from. The only guarantee is that they are the same size and
       *  depth.
       */
      input = gimp_get_input_image (0);
      output = gimp_get_output_image (0);

      if (input && output)
	 {
	 if ((gimp_image_type (input) == RGB_IMAGE) ||
	     (gimp_image_type (input) == GRAY_IMAGE) ||
	     (gimp_image_type (input) == RGBA_IMAGE) ||
	     (gimp_image_type (input) == GRAYA_IMAGE))
	    {
	      if( !(data = gimp_get_params()))
		{
		Values.xthickness = Values.ythickness = 16;
		Values.maintainAspect = 1;
		}
	      else
		Values = *((struct values *) data);
	    dialog_id = gimp_new_dialog( "Checkerboard");

	    row_id = gimp_new_row_group(dialog_id, DEFAULT, NORMAL, "");

	    gimp_new_label( dialog_id, row_id, "X size:");
	    gimp_add_callback( dialog_id,
			       scaleXID = gimp_new_scale( dialog_id,
							  row_id,
							  1,
							  gimp_image_width( input),
							  Values.xthickness,
							  0),
			       scaleCallback,
			       &Values.xthickness);

	    gimp_new_label( dialog_id, row_id, "Y size:");
	    gimp_add_callback( dialog_id,
			       scaleYID = gimp_new_scale( dialog_id,
							  row_id,
							  1,
							  gimp_image_height( input),
							  Values.ythickness,
							  0),
			       scaleCallback,
			       &Values.ythickness);

	    gimp_add_callback( dialog_id,
			       toggleID = gimp_new_check_button( dialog_id,
								 row_id,
								 "Maintain aspect"),
			       toggleCallback,
			       &Values.maintainAspect);

	    gimp_change_item( dialog_id, toggleID,
			      sizeof( Values.maintainAspect), &Values.maintainAspect);

	    gimp_add_callback( dialog_id, gimp_ok_item_id( dialog_id),
			       okCallback, NULL);
	    gimp_add_callback( dialog_id, gimp_cancel_item_id( dialog_id),
			       cancelCallback, NULL);

	    if( gimp_show_dialog( dialog_id))
	      {
		gimp_set_params( sizeof( Values), &Values);
	    /* do it. */
		checkerboard (input, output, Values.xthickness, Values.ythickness);
		gimp_update_image (output);
	      }
	    }
	 else
	    gimp_message ("checkerboard: cannot operate on indexed color images");
	 }
      
      /* Free both images.
       */
      gimp_free_image (input);
      gimp_free_image (output);

      /* Quit
       */
      gimp_quit ();
      }

   return 0;
   }

static void
checkerboard (input, output, xthickness, ythickness)
     Image input, output;
     int xthickness;
     int ythickness;
{
   long width, height;
   long rowstride;
   ImageType imagetype;
   unsigned char *dest;
   int channels;
   unsigned char foreground[3]; /* R G B alpha. */
   unsigned char background[3]; /* R G B alpha. */
   long int xpos, ypos, colorcount;
   unsigned char *curcol;
   int parity;

   width = gimp_image_width (input);
   height = gimp_image_height (input);
   imagetype = gimp_image_type( input);

   channels = gimp_image_channels( input);
   rowstride = width * channels;

   dest = gimp_image_data (output);

   gimp_foreground_color( &(foreground[0]), &(foreground[1]), &foreground[2]);
   gimp_background_color( &(background[0]), &(background[1]), &background[2]);

   for( ypos = 0; ypos < height; ypos++)
      {
      for( xpos = 0; xpos < width; xpos++)
	 {
	 parity = ((xpos / Values.xthickness) + (ypos / Values.ythickness)) % 2;
	 curcol = ((parity) ? foreground : background);
	 for( colorcount = 0; colorcount < channels; colorcount++)
	    *(dest + ypos * rowstride + xpos * channels + colorcount) = curcol[colorcount];
	 }
      }
   }

static void scaleCallback( int itemID, void *clientData, void *callData)
{
  int *scaleValuePtr;
  int max;

  if( Values.maintainAspect)
    {
      scaleValuePtr = ((itemID == scaleXID) ? &Values.ythickness : &Values.xthickness);
      *scaleValuePtr += *((long *) callData) - *((long *) clientData);
      if( *scaleValuePtr < 1)
	*scaleValuePtr = 1;
      else if( *scaleValuePtr >
	       (max = ((itemID == scaleXID) ?
		       gimp_image_height( input) :
		       gimp_image_width( input))))
	*scaleValuePtr = max;
      gimp_change_item( dialog_id,
			(itemID == scaleXID) ? scaleYID : scaleXID,
			sizeof( Values.xthickness),
			scaleValuePtr);
    }
  *((long *) clientData) = *((long *) callData);
}

static void toggleCallback( int itemID, void *clientData, void *callData)
{
  *((long *) clientData) = *((long *) callData);
}

static void okCallback( int itemID, void *clientData, void *callData)
{
  gimp_close_dialog( dialog_id, 1);
}

static void cancelCallback( int itemID, void *clientData, void *callData)
{
  gimp_close_dialog( dialog_id, 0);
}
