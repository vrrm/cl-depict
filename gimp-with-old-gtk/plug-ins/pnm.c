/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 * PNM reading and writing code Copyright (C) 1996 Erik Nygren
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* $Id: pnm.c 1.1 Thu, 04 Apr 1996 02:45:24 -0800 spencer $ */

/* 
 * The pnm reading and writing code was written from scratch by Erik Nygren
 * (nygren@mit.edu) based on the specifications in the man pages and
 * does not contain any code from the netpbm or pbmplus distributions. 
 */

#include <setjmp.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gimp.h"


/* Declare local data types
 */

typedef struct _PNMScanner {
  int    fd;		      /* The file descriptor of the file being read */
  char   cur;		      /* The current character in the input stream */
  int    eof;		      /* Have we reached end of file? */
  char  *inbuf;		      /* Input buffer - initially 0 */
  int    inbufsize;	      /* Size of input buffer */
  int    inbufvalidsize;      /* Size of input buffer with valid data */
  int    inbufpos;            /* Position in input buffer */
} PNMScanner;

typedef struct _PNMInfo {
  int       xres, yres;		/* The size of the image */
  int       maxval;		/* For ascii image files, the max value
				 * which we need to normalize to */
  int       np;			/* Number of image planes (0 for pbm) */
  int       asciibody;		/* 1 if ascii body, 0 if raw body */
  jmp_buf   jmpbuf;		/* Where to jump to on an error loading */
  /* Routine to use to load the pnm body */
  void     (*loader)(PNMScanner *, struct _PNMInfo *, Image);
} PNMInfo;

/* Contains the information needed to write out PNM rows */
typedef struct _PNMRowInfo {
  int            fd;		/* File descriptor */
  unsigned char *rowbuf;	/* Buffer for writing out rows */
  int xres;			/* X resolution */
  int np;			/* Number of planes */
  unsigned char *red;		/* Colormap red */
  unsigned char *grn;		/* Colormap green */
  unsigned char *blu;		/* Colormap blue */
} PNMRowInfo;

#define BUFLEN 512		/* The input buffer size for data returned
				 * from the scanner.  Note that lines
				 * aren't allowed to be over 256 characters
				 * by the spec anyways so this shouldn't
				 * be an issue. */

#define SAVE_COMMENT_STRING "# CREATOR: The GIMP's PNM Filter Version 1.0\n"

/* Declare some local functions.
 */
static void load_image (char *);
static void save_image (char *);

static void item_callback (int, void *, void *);
static void ok_callback (int, void *, void *);
static void cancel_callback (int, void *, void *);

static void pnm_load_ascii(PNMScanner *scan, PNMInfo *info, Image image);
static void pnm_load_raw(PNMScanner *scan, PNMInfo *info, Image image);
static void pnm_load_rawpbm(PNMScanner *scan, PNMInfo *info, Image image);

static void pnmsaverow_ascii(PNMRowInfo *ri, unsigned char *data);
static void pnmsaverow_raw(PNMRowInfo *ri, unsigned char *data);
static void pnmsaverow_ascii_indexed(PNMRowInfo *ri, unsigned char *data);
static void pnmsaverow_raw_indexed(PNMRowInfo *ri, unsigned char *data);

static PNMScanner *pnmscanner_create(int fd);
static void pnmscanner_destroy(PNMScanner *s);
static void pnmscanner_createbuffer(PNMScanner *s, int bufsize);
static void pnmscanner_getchar(PNMScanner *s);
static void pnmscanner_eatwhitespace(PNMScanner *s);
static void pnmscanner_gettoken(PNMScanner *s, char *buf, int bufsize);
#define pnmscanner_eof(s) ((s)->eof)
#define pnmscanner_fd(s) ((s)->fd)

/* Checks for a fatal error */
#define CHECK_FOR_ERROR(predicate, jmpbuf, errmsg) \
        if ((predicate)) \
        { gimp_message((errmsg)); longjmp((jmpbuf),1); }

static char *prog_name;
static int   dialog_ID;

void
main (argc, argv)
     int argc;
     char **argv;
{
  /* Save the program name so we can use it later in reporting errors
   */
  prog_name = argv[0];

  /* Call 'gimp_init' to initialize this filter.
   * 'gimp_init' makes sure that the filter was properly called and
   *  it opens pipes for reading and writing.
   */
  if (gimp_init (argc, argv))
    {
      /* This is a file filter so all it needs to know about is loading
       *  and saving images. So we'll install handlers for those two
       *  messages.
       */
      gimp_install_load_save_handlers (load_image, save_image);

      /* Run until something happens. That something could be getting
       *  a 'QUIT' message or getting a load or save message.
       */
      gimp_main_loop ();
    }
}  

static struct struct_pnm_types {
  char   name;
  int    np;
  int    asciibody;
  int    maxval;
  void (*loader)(PNMScanner *, struct _PNMInfo *, Image);
} pnm_types[] = {
  { '1', 0, 1, 1,   pnm_load_ascii },  /* ASCII PBM */
  { '2', 1, 1, 255, pnm_load_ascii },  /* ASCII PGM */
  { '3', 3, 1, 255, pnm_load_ascii },  /* ASCII PPM */
  { '4', 0, 0, 1,   pnm_load_rawpbm }, /* RAW   PBM */
  { '5', 1, 0, 255, pnm_load_raw },    /* RAW   PGM */
  { '6', 3, 0, 255, pnm_load_raw },    /* RAW   PPM */
  { 0  , 0, 0, 0,   NULL}
};

static void
load_image (filename)
     char *filename;
{
  int fd;			/* File descriptor */
  Image image;
  char *temp;
  char buf[BUFLEN];		/* buffer for random things like scanning */
  PNMInfo *pnminfo;
  PNMScanner *scan;
  int ctr;

  temp = malloc (strlen (filename) + 11);
  if (!temp)
    gimp_quit ();

  sprintf (temp, "Loading %s:", filename);
  gimp_init_progress (temp);
  free (temp);
  
  /* open the file */
  fd = open(filename, O_RDONLY);
  if (fd == -1)
    {
      gimp_message("pnm filter: can't open file\n");
      gimp_quit ();
    }

  /* allocate the necessary structures */
  pnminfo = (PNMInfo *)malloc(sizeof(PNMInfo));
  if (!pnminfo)
    {
      close (fd);
      gimp_quit ();
    }
  
  scan = NULL;
  image = NULL;
  /* set error handling */
  if (setjmp (pnminfo->jmpbuf))
    {
      /* If we get here, we had a problem reading the file */
      if (image)
	gimp_free_image (image);
      if (scan)
	pnmscanner_destroy (scan);
      close (fd);
      free (pnminfo);
      gimp_quit ();
    }

  if (!(scan = pnmscanner_create(fd)))
    longjmp(pnminfo->jmpbuf,1);

  /* Get magic number */
  pnmscanner_gettoken(scan, buf, BUFLEN);
  CHECK_FOR_ERROR(pnmscanner_eof(scan), pnminfo->jmpbuf,
		  "pnm filter: premature end of file\n");
  CHECK_FOR_ERROR((buf[0] != 'P' || buf[2]), pnminfo->jmpbuf,
		  "pnm filter: %s is not a valid file\n");

  /* Look up magic number to see what type of PNM this is */
  for (ctr=0; pnm_types[ctr].name; ctr++)
    if (buf[1] == pnm_types[ctr].name)
      {
	pnminfo->np        = pnm_types[ctr].np;
	pnminfo->asciibody = pnm_types[ctr].asciibody;
	pnminfo->maxval    = pnm_types[ctr].maxval;
	pnminfo->loader    = pnm_types[ctr].loader;
      }
  if (!pnminfo->loader)
    {
      gimp_message("pnm filter: file not in a supported format\n");
      longjmp(pnminfo->jmpbuf,1);
    }

  pnmscanner_gettoken(scan, buf, BUFLEN);
  CHECK_FOR_ERROR(pnmscanner_eof(scan), pnminfo->jmpbuf,
		  "pnm filter: premature end of file\n");
  pnminfo->xres = isdigit(*buf)?atoi(buf):0;
  CHECK_FOR_ERROR(pnminfo->xres<=0, pnminfo->jmpbuf,
		  "pnm filter: invalid xres while loading\n");

  pnmscanner_gettoken(scan, buf, BUFLEN);
  CHECK_FOR_ERROR(pnmscanner_eof(scan), pnminfo->jmpbuf,
		  "pnm filter: premature end of file\n");
  pnminfo->yres = isdigit(*buf)?atoi(buf):0;
  CHECK_FOR_ERROR(pnminfo->yres<=0, pnminfo->jmpbuf,
		  "pnm filter: invalid yres while loading\n");

  if (pnminfo->np != 0)		/* pbm's don't have a maxval field */
    {
      pnmscanner_gettoken(scan, buf, BUFLEN);
      CHECK_FOR_ERROR(pnmscanner_eof(scan), pnminfo->jmpbuf,
		      "pnm filter: premature end of file\n");
      
      pnminfo->maxval = isdigit(*buf)?atoi(buf):0;
      CHECK_FOR_ERROR(((pnminfo->maxval<=0)
		       || (pnminfo->maxval>255 && !pnminfo->asciibody)),
		      pnminfo->jmpbuf, 
		      "pnm filter: invalid maxval while loading\n");
    }

  /* Create a new image of the proper size and associate the filename with it.
   */
  image = gimp_new_image (filename,
			  pnminfo->xres,
			  pnminfo->yres,
			  (pnminfo->np >= 3) ? RGB_IMAGE : GRAY_IMAGE);

  pnminfo->loader(scan, pnminfo, image);
  
  /* Destroy the scanner */
  pnmscanner_destroy(scan);

  /* free the structures */
  free (pnminfo);
  
  /* close the file */
  close (fd);

  gimp_do_progress (1, 1);

  /* Tell the GIMP to display the image.
   */
  gimp_display_image (image);

  /* Tell the GIMP to update the image. (ie Redraw it).
   */
  gimp_update_image (image);
  
  /* Free the image. (This involves detaching the shared memory segment,
   *  which is a good thing.)
   */
  gimp_free_image (image);

  /* Quit.
   */
  gimp_quit ();
}


void
pnm_load_ascii(PNMScanner *scan, PNMInfo *info, Image image)
{
  unsigned char *data;
  int            datapos;
  int            maxpos, maxval, np;
  char           buf[BUFLEN];

  data = gimp_image_data (image);
  np = (info->np)?(info->np):1;
  maxpos = info->xres*info->yres*np;
  maxval = info->maxval;

  /* Buffer reads to increase performance */
  pnmscanner_createbuffer(scan, 4096);

  for (datapos = 0; datapos < maxpos; datapos++, data++)
    {
      /* Truncated files will just have all 0's at the end of the images */
      CHECK_FOR_ERROR(pnmscanner_eof(scan), info->jmpbuf,
		      "pnm filter: premature end of file\n");
      pnmscanner_gettoken(scan, buf, BUFLEN);
      switch (maxval)
	{
	case 255:
	  *data = isdigit(*buf)?atoi(buf):0;
	  break;
	case 1:
	  *data = (*buf=='0')?0xff:0x00;
	  break;
	default:
	  *data = (unsigned char)(255.0*(((double)(isdigit(*buf)?atoi(buf):0))
					 / (double)(maxval)));
	}
      if (0 == datapos % (info->xres*20))
        gimp_do_progress(datapos, maxpos);
    }
}

void 
pnm_load_raw(PNMScanner *scan, PNMInfo *info, Image image)
{
  unsigned char *data;
  int            fd;
  int            row;
  
  data = gimp_image_data (image);
  fd = pnmscanner_fd(scan);

  for (row = 0; row < info->yres; row++)
    {
      if (!(row % 30))
        gimp_do_progress(row, info->xres);
      
      CHECK_FOR_ERROR((info->xres*info->np 
		       != read(fd, data, info->xres*info->np)),
		      info->jmpbuf,
		      "pnm filter: premature end of file\n");
      
      if (info->maxval != 255)	/* Normalize if needed */
	{
	  int i;
	  for (i=0; i < info->xres*info->np; i++)
	    data[i] = (unsigned char)(255.0*(double)(data[i])
				      / (double)(info->maxval));
	}
      
      data += info->xres * info->np;
    }
}

void 
pnm_load_rawpbm(PNMScanner *scan, PNMInfo *info, Image image)
{
  unsigned char *buf;
  unsigned char *data, curbyte;
  int            fd;
  int            rowlen, xpos, bufpos, datapos, maxdatapos;

  data = gimp_image_data (image);
  fd = pnmscanner_fd(scan);
  rowlen = (int)ceil((double)(info->xres)/8.0);

  maxdatapos = info->xres*info->yres;

  CHECK_FOR_ERROR((NULL == (buf = malloc(rowlen*sizeof(unsigned char)))),
		  info->jmpbuf, "pnm filter: malloc failed\n");
  for (datapos = 0; datapos < maxdatapos; datapos++)
    {
      if (0 == datapos % info->xres)
	{
	  if (0 == datapos % (info->xres*20))
	    gimp_do_progress(datapos, maxdatapos);
	  if (rowlen != read(fd, buf, rowlen))
	    {
	      gimp_message("pnm filter: error reading file\n");
	      free(buf);
	      longjmp(info->jmpbuf,1);
	    }
	  bufpos = 0; xpos = 0;
	}
      if (0 == (xpos++) % 8)
	{
	  curbyte = buf[bufpos++];
	}
      data[datapos] = (curbyte&0x80) ? 0x00 : 0xff;
      curbyte <<= 1;
    }
  free(buf);
}

/* Writes out RGB and greyscale raw rows */
static void
pnmsaverow_raw(PNMRowInfo *ri, unsigned char *data)
{
  write(ri->fd, data, ri->xres*ri->np);
}

/* Writes out indexed raw rows */
static void
pnmsaverow_raw_indexed(PNMRowInfo *ri, unsigned char *data)
{
  int i;
  unsigned char *rbcur = ri->rowbuf;
  
  for (i = 0; i < ri->xres; i++)
    {
      *(rbcur++) = ri->red[*data];
      *(rbcur++) = ri->grn[*data];
      *(rbcur++) = ri->blu[*(data++)];
    }
  write(ri->fd, ri->rowbuf, ri->xres*3);
}

/* Writes out RGB and greyscale ascii rows */
static void
pnmsaverow_ascii(PNMRowInfo *ri, unsigned char *data)
{
  int i;
  unsigned char *rbcur = ri->rowbuf;
  
  for (i = 0; i < ri->xres*ri->np; i++)
    {
      rbcur += sprintf(rbcur,"%d\n", 0xff & *(data++));
    }
  write(ri->fd, ri->rowbuf, strlen(ri->rowbuf));
}

/* Writes out RGB and greyscale ascii rows */
static void
pnmsaverow_ascii_indexed(PNMRowInfo *ri, unsigned char *data)
{
  int i;
  unsigned char *rbcur = ri->rowbuf;
  
  for (i = 0; i < ri->xres; i++)
    {
      rbcur += sprintf(rbcur,"%d\n", 0xff & ri->red[*(data)]);
      rbcur += sprintf(rbcur,"%d\n", 0xff & ri->grn[*(data)]);
      rbcur += sprintf(rbcur,"%d\n", 0xff & ri->blu[*(data++)]);
    }
  write(ri->fd, ri->rowbuf, strlen(ri->rowbuf));
}

static void
save_image (filename)
     char *filename;
{
  int fd;
  Image image;
  char *temp;
  unsigned char *data, *rowbuf;
  int group_ID, frame_ID, saveascii_ID, saveraw_ID, saveformat_ID;
  int opt_saveascii = 0;
  int opt_saveraw = 1;
  char buf[BUFLEN];
  int np, xres, yres, rowbufsize;
  int imagetype;
  PNMRowInfo rowinfo;
  int ypos;
  unsigned char red[256];
  unsigned char grn[256];
  unsigned char blu[256];
  void (*saverow)(PNMRowInfo *, unsigned char *);

  /* Get the input image.
   */
  image = gimp_get_input_image (0);
   
  dialog_ID = gimp_new_dialog ("PNM Save Options");
  gimp_new_label (dialog_ID, DEFAULT, "Save Options");
  group_ID = gimp_new_row_group (dialog_ID, DEFAULT, NORMAL, "");

  frame_ID = gimp_new_frame (dialog_ID, group_ID, "Output Format");
  saveformat_ID = gimp_new_row_group (dialog_ID, frame_ID, RADIO, "");
  saveraw_ID = gimp_new_radio_button (dialog_ID, saveformat_ID, "RAW");
  gimp_change_item (dialog_ID, saveraw_ID, 
		    sizeof (opt_saveraw), &opt_saveraw);
  saveascii_ID = gimp_new_radio_button (dialog_ID, saveformat_ID, "ASCII");
  gimp_change_item (dialog_ID, saveascii_ID, 
		    sizeof (opt_saveascii), &opt_saveascii);

  gimp_add_callback (dialog_ID, saveascii_ID, item_callback, &opt_saveascii);
  gimp_add_callback (dialog_ID, saveraw_ID, item_callback, &opt_saveraw);
  gimp_add_callback (dialog_ID, gimp_ok_item_id (dialog_ID), ok_callback, 0);
  gimp_add_callback (dialog_ID, gimp_cancel_item_id (dialog_ID),
		     cancel_callback, 0);
  
  if (!gimp_show_dialog (dialog_ID))
    gimp_quit ();

  temp = malloc (strlen (filename) + 11);
  if (!temp)
    gimp_quit ();


  sprintf (temp, "Saving %s:", filename);
  gimp_init_progress (temp);
  free (temp);
  
  /* open the file */
  fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0644);
  if (fd == -1)
    {
      fprintf (stderr, "%s: can't open \"%s\"\n", prog_name, filename);
      gimp_quit ();
    }

  imagetype = gimp_image_type(image);
  data = gimp_image_data (image);
  xres = gimp_image_width (image);
  yres = gimp_image_height (image);

  /* write out magic number */
  if (opt_saveascii)
    {
      if (imagetype == GRAY_IMAGE)
	{
	  write(fd, "P2\n", 3);
	  np = 1;
	  rowbufsize = xres * 4;
	  saverow = pnmsaverow_ascii;
	}
      else if (imagetype == RGB_IMAGE)
	{
	  write(fd, "P3\n", 3);	  
	  np = 3;
	  rowbufsize = xres * 12;
	  saverow = pnmsaverow_ascii;
	}
      else			/* Indexed */
	{
	  write(fd, "P3\n", 3);	  
	  np = 1;
	  rowbufsize = xres * 12;
	  saverow = pnmsaverow_ascii_indexed;	
	}
    }
  else
    {
      if (imagetype == GRAY_IMAGE)
	{
	  write(fd, "P5\n", 3);
	  np = 1;
	  rowbufsize = xres;
	  saverow = pnmsaverow_raw;
	}
      else if (imagetype == RGB_IMAGE)
	{
	  write(fd, "P6\n", 3);	  
	  np = 3;
	  rowbufsize = xres * 3;
	  saverow = pnmsaverow_raw;
	}
      else			/* Indexed */
	{
	  write(fd, "P6\n", 3);	  
	  np = 1;
	  rowbufsize = xres * 3;
	  saverow = pnmsaverow_raw_indexed;
	}
    }

  if (imagetype == INDEXED_IMAGE)
    {
      int i;
      unsigned char *cmap = gimp_image_cmap (image);
      long colors = gimp_image_colors (image);

      for (i = 0; i < colors; i++)
	{
	  red[i] = *cmap++;
	  grn[i] = *cmap++;
	  blu[i] = *cmap++;
	  rowinfo.red = red;
	  rowinfo.grn = grn;
	  rowinfo.blu = blu;
	}
    }

  /* write out comment string */
  write(fd, SAVE_COMMENT_STRING, strlen(SAVE_COMMENT_STRING));

  /* write out resolution and maxval */
  sprintf(buf, "%d %d\n255\n", xres, yres);
  write(fd, buf, strlen(buf));

  rowbuf = malloc(rowbufsize+1);
  rowinfo.fd = fd;
  rowinfo.rowbuf = rowbuf;
  rowinfo.xres = xres;
  rowinfo.np = np;

  /* Write the body out */
  for (ypos = 0; ypos < yres; ypos++)
    {
      (*saverow)(&rowinfo, data);
      data += xres*np;
      if (0 == ypos % 20)
	gimp_do_progress(ypos, yres);
    }
  
  /* close the file */
  close (fd);

  gimp_do_progress (1, 1);
  
  free(rowbuf);

  /* Free the image. (This involves detaching the shared memory segment,
   *  which is a good thing.)
   */
  gimp_free_image (image);

  /* Quit.
   */
  gimp_quit ();  
}


static void
item_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  *((long*) client_data) = *((long*) call_data);
}

static void
ok_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 1);
}

static void
cancel_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 0);
}


/**************** FILE SCANNER UTILITIES **************/

/* pnmscanner_create ---
 *    Creates a new scanner based on a file descriptor.  The
 *    look ahead buffer is one character initially.
 */
PNMScanner *
pnmscanner_create(int fd)
{
  PNMScanner *s;

  s = (PNMScanner *)malloc(sizeof(PNMScanner));
  if (!s) return(NULL);
  s->fd = fd;
  s->inbuf = 0;
  s->eof = !read(s->fd, &(s->cur), 1);
  return(s);
}

/* pnmscanner_destroy ---
 *    Destroys a scanner and its resources.  Doesn't close the fd.
 */
void
pnmscanner_destroy(PNMScanner *s)
{
  if (s->inbuf) free(s->inbuf);
  free(s);
}

/* pnmscanner_createbuffer ---
 *    Creates a buffer so we can do buffered reads.
 */
void
pnmscanner_createbuffer(PNMScanner *s, int bufsize)
{
  s->inbuf = malloc(sizeof(char)*bufsize);
  s->inbufsize = bufsize;
  s->inbufpos = 0;
  s->inbufvalidsize = read(s->fd, s->inbuf, bufsize);
}

/* pnmscanner_gettoken ---
 *    Gets the next token, eating any leading whitespace.
 */
void
pnmscanner_gettoken(PNMScanner *s, char *buf, int bufsize)
{
  int ctr=0;

  pnmscanner_eatwhitespace(s);
  while (!(s->eof) && !isspace(s->cur) && (s->cur != '#') && (ctr<bufsize))
    {
      buf[ctr++] = s->cur;
      pnmscanner_getchar(s);
    }
  buf[ctr] = '\0';
}

/* pnmscanner_getchar ---
 *    Reads a character from the input stream
 */
void
pnmscanner_getchar(PNMScanner *s)
{
  if (s->inbuf)
    {
      s->cur = s->inbuf[s->inbufpos++];
      if (s->inbufpos >= s->inbufvalidsize)
	{
	  if (s->inbufsize > s->inbufvalidsize)
	    s->eof = 1;
	  else
	    s->inbufvalidsize = read(s->fd, s->inbuf, s->inbufsize);
	  s->inbufpos = 0;
	}
    }
  else
    s->eof = !read(s->fd, &(s->cur), 1);
}

/* pnmscanner_eatwhitespace ---
 *    Eats up whitespace from the input and returns when done or eof.
 *    Also deals with comments.  
 */
void
pnmscanner_eatwhitespace(PNMScanner *s)
{
  int state = 0;

  while (!(s->eof) && (state != -1))
    {
      switch (state)
	{
	case 0:  /* in whitespace */
	  if (s->cur == '#')
	    {
	      state = 1;  /* goto comment */
	      pnmscanner_getchar(s);
	    }
	  else if (!isspace(s->cur))
	    state = -1;
	  else
	    pnmscanner_getchar(s);
	  break;

	case 1:  /* in comment */
	  if (s->cur == '\n')
	    state = 0;  /* goto whitespace */
	  pnmscanner_getchar(s);
	  break;
	}
    }
}


