/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*  saves and loads gimp brush files...
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "gimp.h"
#include "brush_header.h"

#define MAX_NAME 256

/* Some variables... */
static int dialog_ID;

/* Declare some local functions.
 */
static void load_image (char *);
static void save_image (char *);

static void text_callback (int, void *, void *);
static void scale_callback (int, void *, void *);
static void ok_callback (int, void *, void *);
static void cancel_callback (int, void *, void *);

char *prog_name;

void
main (argc, argv)
     int argc;
     char **argv;
{
  /* Save the program name so we can use it later in reporting errors
   */
  prog_name = argv[0];

  /* Call 'gimp_init' to initialize this filter.
   * 'gimp_init' makes sure that the filter was properly called and
   *  it opens pipes for reading and writing.
   */
  if (gimp_init (argc, argv))
    {
      /* This is a file filter so all it needs to know about is loading
       *  and saving images. So we'll install handlers for those two
       *  messages.
       */
      gimp_install_load_save_handlers (load_image, save_image);

      /* Run until something happens. That something could be getting
       *  a 'QUIT' message or getting a load or save message.
       */
      gimp_main_loop ();
    }
}


static void
load_image (filename)
     char *filename;
{
  Image image;
  FILE * fp;
  unsigned char *dest;
  char * brush_name;
  int bn_size;
  unsigned char buf [sz_BrushHeader];
  BrushHeader header;
  unsigned int * hp;
  int i;

  /*  Open the requested file  */
  if (! (fp = fopen (filename, "r")))
    {
      printf ("%s: can't open \"%s\"\n", prog_name, filename);
      gimp_quit ();
    }

  /*  Read in the header size  */
  if ((fread (buf, 1, sz_BrushHeader, fp)) < sz_BrushHeader)
    {
      printf ("%s: error reading GIMP brush \"%s\"\n", prog_name, filename);
      gimp_quit ();
    }
  
  /*  rearrange the bytes in each unsigned int  */
  hp = (unsigned int *) &header;
  for (i = 0; i < (sz_BrushHeader / 4); i++)
    hp [i] = (buf [i * 4] << 24) + (buf [i * 4 + 1] << 16) +
             (buf [i * 4 + 2] << 8) + (buf [i * 4 + 3]);

  /*  Check for correct file format */
  if (header.magic_number != GBRUSH_MAGIC)
    {
      /*  One thing that can save this error is if the brush is version 1  */
      if (header.version != 1)
	{
	  gimp_message ("File not in GIMP brush format!\n");
	  fclose (fp);
	  gimp_quit ();
	}
    }
  /*  Check for correct version  */
  if (header.version != FILE_VERSION)
    {
      /*  If this is a version 1 brush, set the fp back 8 bytes  */
      if (header.version == 1)
	{
	  fseek (fp, -8, SEEK_CUR);
	  header.header_size += 8;
	}
      else
	{
	  gimp_message ("Unknown GIMP brush version #\n");
	  fclose (fp);
	  gimp_quit ();
	}
    }
  /*  Check that the bytes are 1--only format supported currently  */
  if (header.bytes != 1)
    {
      gimp_message ("GIMP brushes with more than 1 byte/pixel are not supported.");
      fclose (fp);
      gimp_quit ();
    }

  /*  Get a new image structure  */
  image = gimp_new_image (filename, header.width, header.height, GRAY_IMAGE);
  dest = gimp_image_data (image);

  /*  Read in the brush name  */
  if ((bn_size = (header.header_size - sz_BrushHeader)) > 0)
    {
      brush_name = (char *) malloc (sizeof (char) * bn_size);
      if ((fread (brush_name, 1, bn_size, fp)) < bn_size)
	{
	  gimp_message ("Error in GIMP brush file...aborting.");
	  fclose (fp);
	  gimp_free_image (image);
	  free (brush_name);
	  gimp_quit ();
	}
    }

  /*  Read the image data  */
  if ((fread (dest, 1, header.width * header.height, fp)) <
      header.width * header.height)
    gimp_message ("GIMP brush file appears to be truncated.");

  /*  Clean up  */
  fclose (fp);
  gimp_display_image (image);
  gimp_update_image (image);
  gimp_free_image (image);

  free (brush_name);
  gimp_quit ();
}


static void
save_image (filename)
     char *filename;
{
  Image image;
  FILE * fp;
  BrushHeader header;
  unsigned char buf [sz_BrushHeader];
  char * brush_name;
  long spacing;
  int has_alpha;
  unsigned int * hp;
  int bn_size;
  int i;
  unsigned char *src;
  int rowgroup_ID;
  int colgroup_ID;
  int name_ID;
  int frame_ID;
  int spacing_ID;

  image = gimp_get_input_image (0);
  switch (gimp_image_type (image))
    {
    case GRAY_IMAGE:
    case GRAYA_IMAGE:
      break;
    default:
      gimp_message ("gbrush: can only operate on gray images");
      gimp_free_image (image);
      gimp_quit ();
      break;
    }

  dialog_ID = gimp_new_dialog ("GIMP brush");

  rowgroup_ID = gimp_new_row_group (dialog_ID, DEFAULT, NORMAL, "");
  colgroup_ID = gimp_new_column_group (dialog_ID, rowgroup_ID, NORMAL, "");
  brush_name = strdup ("User Defined");
  gimp_new_label (dialog_ID, colgroup_ID, "Brush Name:");
  name_ID = gimp_new_text (dialog_ID, colgroup_ID, brush_name);
  frame_ID = gimp_new_frame (dialog_ID, rowgroup_ID, "Spacing");
  spacing_ID = gimp_new_scale (dialog_ID, frame_ID, 0, 1000, 25, 0);

  gimp_add_callback (dialog_ID, gimp_ok_item_id (dialog_ID), ok_callback, 0);
  gimp_add_callback (dialog_ID, gimp_cancel_item_id (dialog_ID), cancel_callback, 0);
  gimp_add_callback (dialog_ID, name_ID, text_callback, &brush_name);
  gimp_add_callback (dialog_ID, spacing_ID, scale_callback, &spacing);

  if (!gimp_show_dialog (dialog_ID))
    {
      gimp_quit ();
      return;
    }

  bn_size = strlen (brush_name) + 1;
  header.header_size = sz_BrushHeader + bn_size;
  header.version = FILE_VERSION;
  header.width = gimp_image_width (image);
  header.height = gimp_image_height (image);
  header.bytes = 1;
  header.magic_number = GBRUSH_MAGIC;
  header.spacing = spacing;

  has_alpha = gimp_image_alpha (image);

  /*  rearrange the bytes in each unsigned int  */
  hp = (unsigned int *) &header;
  for (i = 0; i < (sz_BrushHeader / 4); i++)
    {
      buf [i * 4 + 0] = (unsigned char) ((hp [i] >> 24) & 0xff);
      buf [i * 4 + 1] = (unsigned char) ((hp [i] >> 16) & 0xff);
      buf [i * 4 + 2] = (unsigned char) ((hp [i] >> 8) & 0xff);
      buf [i * 4 + 3] = (unsigned char) ((hp [i] >> 0) & 0xff);
    }

  /*  open the file for writing  */
  if ((fp = fopen (filename, "w")))
    {
      /*  write the header to the open file  */
      fwrite (buf, 1, sz_BrushHeader, fp);

      /*  write the brush name to the open file  */
      fwrite (brush_name, 1, bn_size, fp);

      /*  write the brush data to the file  */
      src = gimp_image_data (image);
      for (i = 0; i < header.width * header.height; i++)
	{
	  fputc (*src++, fp);
	  if (has_alpha)
	    src++;  /* ignore the alpha channel */
	}
      
      fclose (fp);
    }

  /*  Clean up  */
  free (brush_name);
  gimp_free_image (image);
  gimp_quit ();
}


static void
text_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  char ** brush_name;

  brush_name = (char **) client_data;

  *brush_name = (char *) realloc (*brush_name, strlen (call_data) + 1);

  strcpy (*brush_name, call_data);
}


static void
scale_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  *((long*) client_data) = *((long*) call_data);
}
 

static void
ok_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 1);
}


static void
cancel_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 0);
}

