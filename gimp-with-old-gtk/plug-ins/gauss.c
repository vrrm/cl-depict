/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "gimp.h"

typedef struct {
  double radius;
  long horizontal;
  long vertical;
} BlurValues;

#define MAXIMUM(a,b) (((a) > (b)) ? (a) : (b))

/* Declare a local function.
 */
static void text_callback (int, void *, void *);
static void toggle_callback (int, void *, void *);
static void ok_callback (int, void *, void *);
static void cancel_callback (int, void *, void *);
static void gaussian_blur (Image, Image, int, int, double);
static int *make_curve (double, int *);
static void run_length_encode (unsigned char *, int *, int, int);

static char *prog_name;
static int dialog_ID;
static BlurValues vals;

int
main (argc, argv)
     int argc;
     char **argv;
{
  Image input, output;
  int group_ID;
  int text_ID;
  int horz_ID;
  int vert_ID;
  int frame_ID;
  int temp_ID;
  char buf[16];
  void *data;
  double std_dev;
  double radius;

  /* Save the program name so we can use it later in reporting errors
   */
  prog_name = argv[0];

  /* Call 'gimp_init' to initialize this filter.
   * 'gimp_init' makes sure that the filter was properly called and
   *  it opens pipes for reading and writing.
   */
  if (gimp_init (argc, argv))
    {
      /* This is a regular filter. What that means is that it operates
       *  on the input image. Output is put into the ouput image. The
       *  filter should not worry, or even care where these images come
       *  from. The only guarantee is that they are the same size and
       *  depth.
       */
      input = gimp_get_input_image (0);
      output = gimp_get_output_image (0);

      /* If both an input and output image were available, then do some
       *  work. (Blur). Then update the output image.
       */
      if (input && output)
	{
	  if ((gimp_image_type (input) == RGB_IMAGE) ||
	      (gimp_image_type (input) == GRAY_IMAGE) ||
	      (gimp_image_type (input) == RGBA_IMAGE) ||
	      (gimp_image_type (input) == GRAYA_IMAGE))
	    {
	      data = gimp_get_params ();
	      if (data)
		vals = *((BlurValues*) data);
	      else
		{
		  vals.radius = 5.0;
		  vals.horizontal = 1;
		  vals.vertical = 1;
		}

	      dialog_ID = gimp_new_dialog ("Gaussian Blur");
	      gimp_new_label (dialog_ID, DEFAULT, "Options");
	      group_ID = gimp_new_row_group (dialog_ID, DEFAULT, NORMAL, "");
	      
	      temp_ID = gimp_new_column_group (dialog_ID, group_ID, NORMAL, "");
	      gimp_new_label (dialog_ID, temp_ID, "Pixel Radius:");
	      sprintf (buf, "%0.2f", vals.radius);
	      text_ID = gimp_new_text (dialog_ID, temp_ID, buf);

	      frame_ID = gimp_new_frame (dialog_ID, group_ID, "Direction");
	      temp_ID = gimp_new_row_group (dialog_ID, frame_ID, NORMAL, "");
	      horz_ID = gimp_new_check_button (dialog_ID, temp_ID, "Horizontal");
	      vert_ID = gimp_new_check_button (dialog_ID, temp_ID, "Vertical");
	      gimp_change_item (dialog_ID, horz_ID, sizeof (vals.horizontal), &vals.horizontal);
	      gimp_change_item (dialog_ID, vert_ID, sizeof (vals.vertical), &vals.vertical);
	      
	      gimp_add_callback (dialog_ID, text_ID, text_callback, &vals.radius);
	      gimp_add_callback (dialog_ID, horz_ID, toggle_callback, &vals.horizontal);
	      gimp_add_callback (dialog_ID, vert_ID, toggle_callback, &vals.vertical);
	      gimp_add_callback (dialog_ID, gimp_ok_item_id (dialog_ID), ok_callback, 0);
	      gimp_add_callback (dialog_ID, gimp_cancel_item_id (dialog_ID), cancel_callback, 0);

	      if (gimp_show_dialog (dialog_ID))
		{
		  gimp_set_params (sizeof (BlurValues), &vals);
		  gimp_init_progress ("Gaussian Blur");
		  
		  radius = fabs (vals.radius) + 1.0;
		  if (radius < 2.0)
		    {
		      gimp_message ("Sub-pixel radii yield spurious results.");
		    }

		  std_dev = sqrt (-(radius * radius) / (2 * log (1.0 / 255.0)));

		  gaussian_blur (input, output, vals.horizontal,
				 vals.vertical, std_dev);
		    
		  gimp_update_image (output);
		}
	    }
	  else
	    gimp_message ("blur: cannot operate on indexed color images");
	}
      
      /* Free both images.
       */
      gimp_free_image (input);
      gimp_free_image (output);

      /* Quit
       */
      gimp_quit ();
    }

  return 0;
}

static void
text_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  *((double*) client_data) = atof (call_data);
}

static void
toggle_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  *((long*) client_data) = *((long*) call_data);
}

static void
ok_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 1);
}

static void
cancel_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 0);
}

static void
gaussian_blur (input, output, horz, vert, radius)
     Image input, output;
     int horz, vert;
     double radius;
{
  long width, height;
  long channels, rowstride;
  unsigned char *dest, *dp;
  unsigned char *src, *sp;
  int *buf, *b;
  int pixels;
  int total;
  int x1, y1, x2, y2;
  int i, row, col, chan;
  int start, end;
  int progress, max_progress;
  int *curve;
  int *sum;
  int val;
  int length;
  int initial_p, initial_m;
  
  curve = make_curve (radius, &length);
  sum = malloc (sizeof (int) * (2 * length + 1));
  if (!sum)
    {
      fprintf (stderr, "%s: unable to allocate memory\n", prog_name);
      gimp_quit ();
    }

  sum[0] = 0;
  
  for (i = 1; i <= length*2; i++)
    sum[i] = curve[i-length] + sum[i-1];
  sum += length;

  gimp_image_area (input, &x1, &y1, &x2, &y2);
  
  width = (x2 - x1);
  height = (y2 - y1);
  channels = gimp_image_channels (input);
  rowstride = gimp_image_width (input) * channels;

  buf = malloc (sizeof (int) * MAXIMUM (width, height) * 2);
  if (!buf)
    {
      fprintf (stderr, "%s: unable to allocate memory\n", prog_name);
      gimp_quit ();
    }

  progress = 0;
  max_progress = (horz) ? height : 0;
  max_progress += (vert) ? width : 0;
  
  total = sum[length] - sum[-length];
		  
  src = (unsigned char *)
    gimp_image_data (input) + x1 * channels + y1 * rowstride;

  if (vert)
    {
      dest = (unsigned char *)
	gimp_image_data (output) + x1 * channels + y1 * rowstride;
      for (col = 0; col < width; col++)
	{
	  sp = src;
	  dp = dest;
	  
	  src += channels;
	  dest += channels;
	  
	  for (chan = 0; chan < channels; chan++)
	    {
	      initial_p = sp[chan];
	      initial_m = sp[(height-1) * rowstride + chan];

	      /*  Determine a run-length encoded version of the row  */
	      run_length_encode (sp + chan, buf, height, rowstride);

	      for (row = 0; row < height; row++)
		{
		  start = (row < length) ? -row : -length;
		  end = (height <= (row + length)) ? (height - row - 1) : length;
		  
		  val = 0;
		  i = start;
		  b = buf + (row + i) * 2;

		  if (start != -length)
		    val += initial_p * (sum[start] - sum[-length]);

		  while (i < end)
		    {
		      pixels = b[0];
		      i += pixels;
		      if (i > end)
			i = end;
		      val += b[1] * (sum[i] - sum[start]);
		      b += (pixels * 2);
		      start = i;
		    }

		  if (end != length)
		    val += initial_m * (sum[length] - sum[end]);

		  dp[row * rowstride + chan] = val / total;
		}
	    }
	  
	  progress++;
	  if ((progress % 5) == 0)
	    gimp_do_progress (progress, max_progress);
	}

      /*  prepare for the horizontal pass  */
      src = (unsigned char *) 
	gimp_image_data (output) + (x1 * channels) + y1 * rowstride;
    }

  if (horz)
    {
      dest = src;

      for (row = 0; row < height; row++)
	{
	  sp = src;
	  dp = dest;

	  src += rowstride;
	  dest += rowstride;
	  
	  for (chan = 0; chan < channels; chan++)
	    {
	      initial_p = sp[chan];
	      initial_m = sp[(width-1) * channels + chan];

	      /*  Determine a run-length encoded version of the row  */
	      run_length_encode (sp + chan, buf, width, channels);
	      
	      for (col = 0; col < width; col++)
		{
		  start = (col < length) ? -col : -length;
		  end = (width <= (col + length)) ? (width - col - 1) : length;
		  
		  val = 0;
		  i = start;
		  b = buf + (col + i) * 2;
		  
		  if (start != -length)
		    val += initial_p * (sum[start] - sum[-length]);

		  while (i < end)
		    {
		      pixels = b[0];
		      i += pixels;
		      if (i > end)
			i = end;
		      val += b[1] * (sum[i] - sum[start]);
		      b += (pixels * 2);
		      start = i;
		    }
		  
		  if (end != length)
		    val += initial_m * (sum[length] - sum[end]);

		  dp[col * channels + chan] = val / total;
		}
	    }
	  
	  progress++;
	  if ((progress % 5) == 0)
	    gimp_do_progress (progress, max_progress);
	}
    }

  free (buf);
}

/*
 * The equations: g(r) = exp (- r^2 / (2 * sigma^2))
 *                   r = sqrt (x^2 + y ^2)
 */

static int *
make_curve (sigma, length)
     double sigma;
     int *length;
{
  int *curve;
  double sigma2;
  double l;
  int temp;
  int i, n;

  sigma2 = 2 * sigma * sigma;
  l = sqrt (-sigma2 * log (1.0 / 255.0));

  n = ceil (l) * 2;
  if ((n % 2) == 0)
    n += 1;

  curve = malloc (sizeof (int) * n);
  if (!curve)
    {
      fprintf (stderr, "%s: could not allocate memory\n", prog_name);
      gimp_quit ();
    }

  *length = n / 2;
  curve += *length;
  curve[0] = 255;

  for (i = 1; i <= *length; i++)
    {
      temp = (int) (exp (- (i * i) / sigma2) * 255);
      curve[-i] = temp;
      curve[i] = temp;
    }

    /*
    fprintf (stdout, "\nlength: %d\n", *length * 2);
    for (i = -(*length); i <= *length; i++) 
    fprintf (stdout, "%d ", curve[i]); 
    fprintf (stdout, "\n"); 
    */

  return curve;
}


static void
run_length_encode (src, dest, w, bytes)
     unsigned char * src;
     int * dest;
     int w, bytes;
{
  int start;
  int i;
  int j;
  unsigned char last;

  last = *src;
  src += bytes;
  start = 0;

  for (i = 1; i < w; i++)
    {
      if (*src != last)
	{
	  for (j = start; j < i; j++)
	    {
	      *dest++ = (i - j);
	      *dest++ = last;
	    }
	  start = i;
	  last = *src;
	}
      src += bytes;
    }

  for (j = start; j < i; j++)
    {
      *dest++ = (i - j);
      *dest++ = last;
    }
}
