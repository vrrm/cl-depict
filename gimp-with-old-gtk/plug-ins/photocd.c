/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * PhotoCD loading file filter for the GIMP
 *   (c) 1996 Gerd Knorr <kraxel@cs.tu-berlin.de>
 *
 * It loads the image with one of the reolutions 192x128, 384x256
 * and 768x512 as RGB_IMAGE
 *
 * Some of the code is just cut-and pasted from the JPEG plug-in
 *  (from Peter Mattis)
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "gimp.h"

static unsigned char gray1[768];
static unsigned char gray2[768];
static unsigned char red[384];
static unsigned char blue[384];

/*--------------------------------------------------------*/

static char *prog_name;

/*--------------------------------------------------------*/

static int gray_LUT[256] = {
    0,   1,   2,   4,   5,   7,   8,   9,  11,  12,  14,  15,  16,  18,  19,  21,
   22,  24,  25,  26,  28,  29,  31,  32,  33,  35,  36,  38,  39,  41,  42,  43,
   45,  46,  48,  49,  50,  52,  53,  55,  56,  57,  59,  60,  62,  63,  65,  66,
   67,  69,  70,  72,  73,  74,  76,  77,  79,  80,  82,  83,  84,  86,  87,  89,
   90,  91,  93,  94,  96,  97,  99, 100, 101, 103, 104, 106, 107, 108, 110, 111,
  113, 114, 115, 117, 118, 120, 121, 123, 124, 125, 127, 128, 130, 131, 132, 134,
  135, 137, 138, 140, 141, 142, 144, 145, 147, 148, 149, 151, 152, 154, 155, 156,
  158, 159, 161, 162, 164, 165, 166, 168, 169, 171, 172, 173, 175, 176, 178, 179,
  181, 182, 183, 185, 186, 188, 189, 190, 192, 193, 195, 196, 198, 199, 200, 202,
  203, 205, 206, 207, 209, 210, 212, 213, 214, 216, 217, 219, 220, 222, 223, 224,
  226, 227, 229, 230, 231, 233, 234, 236, 237, 239, 240, 241, 243, 244, 246, 247,
  248, 250, 251, 253, 254, 256, 257, 258, 260, 261, 263, 264, 265, 267, 268, 270,
  271, 272, 274, 275, 277, 278, 280, 281, 282, 284, 285, 287, 288, 289, 291, 292,
  294, 295, 297, 298, 299, 301, 302, 304, 305, 306, 308, 309, 311, 312, 313, 315,
  316, 318, 319, 321, 322, 323, 325, 326, 328, 329, 330, 332, 333, 335, 336, 338,
  339, 340, 342, 343, 345, 346, 347, 349, 350, 352, 353, 355, 356, 357, 359, 360  
};

static int blue_LUT[256] = {
  -318, -316, -314, -312, -310, -308, -306, -304, -302, -300, -298, -296, -294, -292, -290, -288,
  -286, -284, -282, -280, -278, -276, -274, -272, -270, -268, -266, -264, -262, -260, -258, -256,
  -254, -252, -250, -248, -246, -244, -242, -240, -238, -236, -234, -232, -230, -228, -226, -224,
  -222, -220, -218, -216, -214, -212, -210, -208, -206, -204, -202, -200, -198, -196, -194, -192,
  -190, -188, -186, -184, -182, -180, -178, -176, -174, -172, -170, -168, -166, -164, -162, -160,
  -158, -156, -154, -152, -150, -148, -146, -144, -142, -140, -138, -136, -134, -132, -130, -128,
  -126, -124, -122, -120, -118, -116, -114, -112, -110, -108, -106, -104, -102, -100,  -98,  -96,
   -94,  -92,  -90,  -88,  -86,  -84,  -82,  -80,  -78,  -76,  -74,  -72,  -70,  -68,  -66,  -64,
   -62,  -60,  -58,  -56,  -54,  -52,  -50,  -48,  -46,  -44,  -42,  -40,  -38,  -36,  -34,  -32,
   -30,  -28,  -26,  -24,  -22,  -20,  -18,  -16,  -14,  -12,  -10,   -8,   -6,   -4,   -2,    0,
     2,    4,    6,    8,   10,   12,   14,   16,   18,   20,   22,   24,   26,   28,   30,   32,
    34,   36,   38,   40,   42,   44,   46,   48,   50,   52,   54,   56,   58,   60,   62,   64,
    66,   68,   70,   72,   74,   76,   78,   80,   82,   84,   86,   88,   90,   92,   94,   96,
    98,  100,  102,  104,  106,  108,  110,  112,  114,  116,  118,  120,  122,  124,  126,  128,
   130,  132,  134,  136,  138,  140,  142,  144,  146,  148,  150,  152,  154,  156,  158,  160,
   162,  164,  166,  168,  170,  172,  174,  176,  178,  180,  182,  184,  186,  188,  190,  192,
};

static int red_LUT[256] = {
  -274, -272, -270, -268, -266, -264, -262, -260, -258, -256, -254, -252, -250, -248, -246, -244,
  -242, -240, -238, -236, -234, -232, -230, -228, -226, -224, -222, -220, -218, -216, -214, -212,
  -210, -208, -206, -204, -202, -200, -198, -196, -194, -192, -190, -188, -186, -184, -182, -180,
  -178, -176, -174, -172, -170, -168, -166, -164, -162, -160, -158, -156, -154, -152, -150, -148,
  -146, -144, -142, -140, -138, -136, -134, -132, -130, -128, -126, -124, -122, -120, -118, -116,
  -114, -112, -110, -108, -106, -104, -102, -100,  -98,  -96,  -94,  -92,  -90,  -88,  -86,  -84,
   -82,  -80,  -78,  -76,  -74,  -72,  -70,  -68,  -66,  -64,  -62,  -60,  -58,  -56,  -54,  -52,
   -50,  -48,  -46,  -44,  -42,  -40,  -38,  -36,  -34,  -32,  -30,  -28,  -26,  -24,  -22,  -20,
   -18,  -16,  -14,  -12,  -10,   -8,   -6,   -4,   -2,    0,    2,    4,    6,    8,   10,   12,
    14,   16,   18,   20,   22,   24,   26,   28,   30,   32,   34,   36,   38,   40,   42,   44,
    46,   48,   50,   52,   54,   56,   58,   60,   62,   64,   66,   68,   70,   72,   74,   76,
    78,   80,   82,   84,   86,   88,   90,   92,   94,   96,   98,  100,  102,  104,  106,  108,
   110,  112,  114,  116,  118,  120,  122,  124,  126,  128,  130,  132,  134,  136,  138,  140,
   142,  144,  146,  148,  150,  152,  154,  156,  158,  160,  162,  164,  166,  168,  170,  172,
   174,  176,  178,  180,  182,  184,  186,  188,  190,  192,  194,  196,  198,  200,  202,  204,
   206,  208,  210,  212,  214,  216,  218,  220,  222,  224,  226,  228,  230,  232,  234,  236,
};

/*--------------------------------------------------------*/

static int dialog_ID;
static int dialog_result = 3;

static void
item_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
    if (*(int*)call_data == 1)
	dialog_result = (int)client_data;
}

static void
ok_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 1);
}

static void
cancel_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 0);
}

int size_dialog()
{
    static int one = 1;
    int group_ID,s,m,l;
	
    dialog_ID = gimp_new_dialog ("PhotoCD Load Options");
    group_ID  = gimp_new_row_group (dialog_ID, 0, RADIO, "foo");
    s         = gimp_new_radio_button (dialog_ID, group_ID, "196x128");
    m         = gimp_new_radio_button (dialog_ID, group_ID, "384x256");
    l         = gimp_new_radio_button (dialog_ID, group_ID, "768x512");
    gimp_change_item (dialog_ID, l, sizeof(int), &one);
    gimp_add_callback (dialog_ID, s, item_callback, (void*)1);
    gimp_add_callback (dialog_ID, m, item_callback, (void*)2);
    gimp_add_callback (dialog_ID, l, item_callback, (void*)3);
    gimp_add_callback (dialog_ID, gimp_ok_item_id (dialog_ID), ok_callback, 0);
    gimp_add_callback (dialog_ID, gimp_cancel_item_id (dialog_ID), cancel_callback, 0);

    if (gimp_show_dialog (dialog_ID))
	return dialog_result;
    else
	return -1;
}

/*--------------------------------------------------------*/

void load_image(char *filename)
{
    FILE           *fp;
    int            x,y,i,r,b,g,gray,width,height,seek,size;
    unsigned char  *temp;
    Image          image;

    switch (size = size_dialog()) {
    case -1: gimp_quit ();
    case  1: width = 192; height = 128; seek = 8192;   break;
    case  2: width = 384; height = 256; seek = 47104;  break;
    case  3: width = 768; height = 512; seek = 196608; break;
    }

    /* no progress bar for the tiny 192x128 image */
    if (size > 1) {
	temp = malloc (strlen (filename) + 11);
	if (!temp)
	    gimp_quit ();
    
	sprintf (temp, "Loading %s:", filename);
	gimp_init_progress (temp);
	free (temp);
    }

    if ((fp = fopen (filename, "rb")) == NULL) {
	printf ("%s: can't open \"%s\"\n", prog_name, filename);
	gimp_quit ();
    }
    fseek(fp,seek,SEEK_SET);

    image = gimp_new_image (filename,width,height, RGB_IMAGE);
    temp  = gimp_image_data (image);

    for (y = 0, i = 0; y < height; y += 2) {
	if (size > 1)
	    gimp_do_progress (y, height);
	fread(gray1,width,1,fp);
	fread(gray2,width,1,fp);
	fread(blue,width/2,1,fp);
	if (1 != fread(red,width/2,1,fp)) {
	    gimp_quit ();
	}

	for (x = 0; x < width; x++) {
	    gray = gray_LUT[gray1[x]];
	    b    = gray + blue_LUT[blue[x >> 1]];
	    r    = gray + red_LUT [red [x >> 1]];
	    g    = (10*gray - b - 3*r) / 6;
	    temp[i++] = (r & ~0xff) ? ((r < 0) ? 0 : 255 ) : r;
	    temp[i++] = (g & ~0xff) ? ((g < 0) ? 0 : 255 ) : g;
	    temp[i++] = (b & ~0xff) ? ((b < 0) ? 0 : 255 ) : b;
	}
    
	for (x = 0; x < width; x++) {
	    gray = gray_LUT[gray2[x]];
	    b    = gray + blue_LUT[blue[x >> 1]];
	    r    = gray + red_LUT [red [x >> 1]];
	    g    = (10*gray - b - 3*r) / 6;
	    temp[i++] = (r & ~0xff) ? ((r < 0) ? 0 : 255 ) : r;
	    temp[i++] = (g & ~0xff) ? ((g < 0) ? 0 : 255 ) : g;
	    temp[i++] = (b & ~0xff) ? ((b < 0) ? 0 : 255 ) : b;
	}
    }
    if (size > 1)
	gimp_do_progress (1, 1);
    gimp_display_image (image);
    gimp_update_image (image);
    gimp_free_image (image);
    gimp_quit ();

    return;
}


/*--------------------------------------------------------*/

int
main (int argc, char *argv[])
{
    /*
     * Save the program name so we can use it later in reporting errors
     */
    prog_name = argv[0];

    /*
     * Call 'gimp_init' to initialize this filter.
     * 'gimp_init' makes sure that the filter was properly called and
     *  it opens pipes for reading and writing.
     */
    if (gimp_init (argc, argv))
	{
	    /*
	     * This is a file filter so all it needs to know about is loading
	     *  and saving images. So we'll install handlers for those two
	     *  messages.
	     */
	    gimp_install_load_save_handlers (load_image, NULL);

	    /*
	     * Run until something happens. That something could be getting
	     *  a 'QUIT' message or getting a load or save message.
	     */
	    gimp_main_loop ();
	}
    
    /* keep compiler happy */
    return 0;
}

