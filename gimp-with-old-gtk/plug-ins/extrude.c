/* extrude.c (C) 1996 Karl LaRocca larocca@cobite.com
 * ========= ======== ============ ==================
 * Extrude plug-in filter for The Gimp                           
 *
 *
 * The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* extrude.c version 0.99
 * ======================
 * works with grayscale and 24bit images
 * anti-alaising
 * can use another image as extrusion map (but no point really until texture mapping is completed)
 *
 * TO DO:
 * ======
 * texture map original image onto triangles
 * blocks
 * no mask incomplete (right now it can only do mask incomplete)
 * perhaps let the pyramids get out of their initial little cells (height scalar > 1.0) UUUUUGH....
 *
 */

/*#define _DEBUG */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "gimp.h"

enum radio_buttons {BLOCKS,
		    PYRAMIDS,
		    RANDOM, 
		    LEVELS,
		    MAP,
		    SOLID,
		    MASK,
		    TEXTURE,
		    MAX_RADIO};

enum quadrants {TOP_LEFT,
		TOP_RIGHT,
		BOTTOM_LEFT,
		BOTTOM_RIGHT};

typedef struct {
  long x, y;
} Point;

typedef struct {
  double x,y;
} FloatPoint;

typedef struct {
  FloatPoint apex;
  double color[3];
} Cell;

typedef struct {
  long size;
  long radio[8];
} ExtrudeVals;

#ifndef FALSE
#define FALSE 0
#define TRUE  1
#endif

/* absolute distance between two points */
#define DELTA(x,x1) (fabs(fabs(x) - fabs(x1)) < 1.0) ? (fabs(fabs(x) - fabs(x1))) : 1.0;

/* scalar for anti-alaising */
#define PIXEL_RATIO(x,y) ((x < 1.0) || (y < 1.0)) ? (((x * y) / 2.0)) : 1.0;

/* luminance of a r,g,b triplet */
#define SCALETO1(x) = (float)x / 255.0;
#define LUM(r,g,b) = ((MIN(MIN(r,g),b)) + MAX(MAX((r,g)b)) / 2.0);

#ifdef _DEBUG
#define TOP_COLOR 250
#define RIGHT_COLOR 200
#define LEFT_COLOR 150
#define BOTTOM_COLOR 100
#define NO_COLOR 0
#endif

static void text_callback (int, void *, void *);
static void radio_callback (int, void *, void *);
static void ok_callback (int, void *, void *);
static void cancel_callback (int, void *, void *);

static long width;
static long height;
static double size_2;
static ExtrudeVals v;
static int radio_ID[7];
static int image_menu_ID; 
static int dialog_ID;
static int map_ID;

static Image input;
static Image output;
static Image map;

static Cell *cells;
static Point grid;
static long top, left, right, bottom;

static void getvals(void);
static void extrude();
static int do_dialog(void);
static int init(void);
static void debug(void);

int main(int argc, char **argv) {
  if (!gimp_init(argc, argv))
    return 0;

  input = gimp_get_input_image(0);
  output = gimp_get_output_image(0);

  if (!input || !output) return 0;

  getvals();

  if (do_dialog()){
    if (!init()) {
      gimp_message("Extrude: Unable to open map image.");
    } else {
      switch (gimp_image_type(input)) {
      case RGB_IMAGE:
      case GRAY_IMAGE:
	extrude();
	gimp_update_image(output);
	break;
      case RGBA_IMAGE:
      case GRAYA_IMAGE:
	gimp_message("Extrude: Can't operator on transparent image");
	break;
      case INDEXED_IMAGE: case INDEXEDA_IMAGE:
	gimp_message("Extrude: Can't operate on indexed image");
	break;
      case UNKNOWN_IMAGE:
	gimp_message("Extrude: Can't operate on unknown image");
	break;
	break;
      }
    }
  }

  free(cells);
  gimp_free_image(input);
  gimp_free_image(output);

  gimp_quit();
  return 0;
}

static void getvals() {
  void *data;

  width = gimp_image_width(input);
  height = gimp_image_height(input);

  data = gimp_get_params();
  if (data) {
    v = *((ExtrudeVals *) data);
  } else {
    v.size = 10;
    v.radio[BLOCKS]   = FALSE;
    v.radio[PYRAMIDS] = TRUE;
    v.radio[RANDOM]   = FALSE;
    v.radio[LEVELS]   = TRUE;
    v.radio[MAP]      = FALSE;
    v.radio[SOLID]    = FALSE;
    v.radio[MASK]     = TRUE;
    v.radio[TEXTURE]  = FALSE;
    }
}

static int init() {
  gimp_set_params (sizeof (ExtrudeVals), &v);
  gimp_init_progress ("Extrude");

  grid.x = (int)(width / v.size);
  grid.y = (int)(height / v.size);

  if (v.radio[MAP]) {
    map = gimp_get_input_image(map_ID);
    if (!map)
      return FALSE;
  } else map = 0;

  /* gimp assures us that map is the same width and height as src */
  /* there has to be a better way to say this */
  top = width % v.size;
  bottom = top / 2;
  if (top % 2) top = bottom + 1;
  else top = bottom;

  left = width % v.size;
  right = left / 2;
  if (left % 2) left = right + 1;
  else left = right;

  size_2 = v.size / 2;

  /* allocate an array to hold the heights of the cells */
  cells = (Cell *)malloc((sizeof(Cell) * grid.x * grid.y));

  cells = memset(cells, 0, (sizeof(Cell) * grid.x * grid.y));

  return TRUE;
}

/* perform the filter on a single channel image */
static void extrude() {
  unsigned char *src, *dest;
  int i;
  FloatPoint origin, center, scale, delta, check;
  Cell *this_cell;
  Point this_point;
  double slope[4];
  double height_scalar = 0.0;
  double pixel_ratio;
  long row, col;
  int has_alpha;

  short src_channels = gimp_image_channels (input);
  short map_channels;
  short this_channels;

  Point area;
  short quad;

#ifndef _DEBUG
  unsigned char TOP_COLOR[3], RIGHT_COLOR[3], BOTTOM_COLOR[3], LEFT_COLOR[3];
#endif
  unsigned char MEDIAN_COLOR[3];

  area.x = width - left - right;
  area.y = height - top - bottom;

  origin.x = area.x / 2;
  origin.y = area.y / 2;

  /* now I don't remember what scale exactly means, why don't you comment stuff fuckhead!!! */
  scale.x = origin.x - size_2;
  scale.y = origin.y - size_2;

  has_alpha = gimp_image_alpha (input);

  /* populate cells from src */
  if (v.radio[MAP]) {
    src = gimp_image_data(map);
    this_channels = map_channels = gimp_image_channels(map);
  }  else {
    src = gimp_image_data(input);
    this_channels = src_channels;
  }

  /* note that we don't have to bother about "bottom", by the end of the loop, we don't care where src is */
  src+= top * width * this_channels;
  
  for (row = 0; row < area.y; row++){
    src+= left * this_channels;
    for (col = 0; col < area.x; col++) {
      for (i = 0; i < this_channels; i++) 
	cells[((row / v.size) * grid.x) + (col / v.size)].color[i] += ((float)*src++ / 255.0);
    }
    src+= right * this_channels;
  }

  /* find the average color and position the apex for each cell */
  for (row = 0; row < grid.y; row++) { 
     for (col = 0; col < grid.x; col++) { 
       this_cell = &cells[(row * grid.x) + col];
 
       for (i = 0; i < this_channels; i++)
	 this_cell->color[i] /= (float)(v.size * v.size); 

       center.x = ((col * v.size) + size_2);
       center.y = ((row * v.size) + size_2);
       
       if (v.radio[LEVELS] || v.radio[MAP]) {
	 height_scalar = 0;
	 for (i = 0; i < this_channels; i++)
	   height_scalar += this_cell->color[i];
	 height_scalar /= (float)this_channels;
       } else 
	 height_scalar = ((float)rand() / RAND_MAX);

       this_cell->apex.x = size_2 + ((((center.x - origin.x) / scale.x) * 
					     size_2) 
					    * height_scalar);
       if (this_cell->apex.x >= v.size) this_cell->apex.x = v.size - .00001;
 
       this_cell->apex.y = size_2 + ((((center.y - origin.y) / scale.y) * 
					     size_2) 
					    * height_scalar);
       if (this_cell->apex.y >= v.size) this_cell->apex.y = v.size - .00001;
      } 
   } 

  /* write the average values back into dest*/
  dest = gimp_image_data(output);
  src = gimp_image_data(input);
  this_channels = src_channels;

  memcpy(dest, src, top * width * src_channels);
  src+= top * width * src_channels;
  dest+= top * width * src_channels;

  for (row = 0; row < area.y; row++){
    this_point.y = (row % v.size);

    for (col = 0; col < left; col++) {
      for (i = 0; i < this_channels; i++)
	*dest++ = *src++;
    }

    src += area.x * this_channels;

    /* process a size width chunk of a row*/
    for (col = 0; col < area.x; col+= v.size) {
      /* calculations that effect the entire row */
      this_cell = &cells[((row / v.size) * grid.x) + (col / v.size)];

#ifndef _DEBUG
      for (i = 0; i < this_channels; i++) {
	TOP_COLOR[i]    = this_cell->color[i] * 255;
	LEFT_COLOR[i]   = TOP_COLOR[i] * .75;
	RIGHT_COLOR[i]  = LEFT_COLOR[i] * .75;
	BOTTOM_COLOR[i] = RIGHT_COLOR[i] * .75;
      }
#endif

      if (this_cell->apex.x != 0)
	slope[TOP_LEFT] = ((float)((float)0 - this_cell->apex.y) / 
			   (float)((float)0 - this_cell->apex.x));
      
      if (this_cell->apex.y != v.size)
	slope[TOP_RIGHT] = ((float)(this_cell->apex.y - 0) / 
			    (float)(this_cell->apex.x - v.size));

      if (this_cell->apex.x != v.size) {
	slope[BOTTOM_LEFT]  = ((float)((float)v.size - this_cell->apex.y) / 
			       (float)((float)0 - this_cell->apex.x));

	slope[BOTTOM_RIGHT] = (((float)0 - ((float)v.size - this_cell->apex.y)) /
			       ((float)0 - ((float)v.size - this_cell->apex.x)));
      }

      /* process the row */
      for (this_point.x = 0; this_point.x < v.size; this_point.x++) {

	/* figure out what quadrant the pixel falls into */
	if (this_point.y < this_cell->apex.y) {
	  if (this_point.x < this_cell->apex.x) 
	    { quad = TOP_LEFT; } 
	  else 
	    { quad = TOP_RIGHT; }
	} else if (this_point.x <= this_cell->apex.x)
	  { quad = BOTTOM_LEFT; }
	else { quad = BOTTOM_RIGHT; }

	switch (quad) {
	case TOP_LEFT:
	  check.x = this_cell->apex.x + (((float)this_point.y - this_cell->apex.y) / 
					 slope[TOP_LEFT]);
	  check.y = this_cell->apex.y + (((float)this_point.x - this_cell->apex.x) *
					 slope[TOP_LEFT]); 

	  delta.x = DELTA(check.x, this_point.x);
	  delta.y = DELTA(check.y, this_point.y);
	  pixel_ratio = PIXEL_RATIO(delta.x, delta.y);
	  for (i = 0; i < this_channels; i++)
	    MEDIAN_COLOR[i] = (TOP_COLOR[i] + LEFT_COLOR[i]) / 2;

	  if (this_point.y < check.y) {
	    for (i = 0; i < this_channels; i++)
	      *dest++ = (TOP_COLOR[i] * pixel_ratio) + (MEDIAN_COLOR[i] * (1 - pixel_ratio));
	  } else {
	    for (i = 0; i < this_channels; i++)
	      *dest++ = (LEFT_COLOR[i] * pixel_ratio) + (MEDIAN_COLOR[i] * (1 - pixel_ratio));
	  }
	  break;

	case TOP_RIGHT:
	  check.x = v.size + ((this_point.y - 0) / slope[TOP_RIGHT]);
	  check.y = slope[TOP_RIGHT] * (this_point.x - v.size); 

	  delta.x = DELTA(check.x, this_point.x);
	  delta.y = DELTA(check.y, this_point.y);
	  pixel_ratio = PIXEL_RATIO(delta.x, delta.y);
	  for (i = 0; i < this_channels; i++)
	    MEDIAN_COLOR[i] = (TOP_COLOR[i] + RIGHT_COLOR[i]) / 2;

	  if (this_point.y < check.y)
	    for (i = 0; i < this_channels; i++)
	      *dest++ = (TOP_COLOR[i] * pixel_ratio) + (MEDIAN_COLOR[i] * (1 - pixel_ratio));
	  else 
	    for (i = 0; i < this_channels; i++)
	      *dest++ = (RIGHT_COLOR[i] * pixel_ratio) + (MEDIAN_COLOR[i] * (1 - pixel_ratio));
	  break;

	case BOTTOM_LEFT:
	  check.x = this_cell->apex.x + ((this_point.y - this_cell->apex.y) / 
					 slope[BOTTOM_LEFT]);
	  check.y = this_cell->apex.y + ((this_point.x - this_cell->apex.x) *
					 slope[BOTTOM_LEFT]);

	  delta.x = DELTA(check.x, this_point.x);
	  delta.y = DELTA(check.y, this_point.y);
	  pixel_ratio = PIXEL_RATIO(delta.x, delta.y);
	  for (i = 0; i < this_channels; i++)
	    MEDIAN_COLOR[i] = (BOTTOM_COLOR[i] + LEFT_COLOR[i]) / 2;

	  if (this_point.y > check.y)
	    for (i = 0; i < this_channels; i++)
	      *dest++ = (BOTTOM_COLOR[i] * pixel_ratio) + (MEDIAN_COLOR[i] * (1 - pixel_ratio));
	  else 
	    for (i = 0; i < this_channels; i++)
	      *dest++ = (LEFT_COLOR[i] * pixel_ratio) + (MEDIAN_COLOR[i] * (1 - pixel_ratio));
	  break;

	case BOTTOM_RIGHT:
	  check.y = this_cell->apex.y + ((this_point.x - this_cell->apex.x) * slope[BOTTOM_RIGHT]);
	  check.x = this_cell->apex.x + ((this_point.y - this_cell->apex.y) / slope[BOTTOM_RIGHT]);

	  delta.x = DELTA(check.x, this_point.x);
	  delta.y = DELTA(check.y, this_point.y);

	  pixel_ratio = PIXEL_RATIO(delta.x, delta.y);
	  for (i = 0; i < this_channels; i++)
	    MEDIAN_COLOR[i] = (BOTTOM_COLOR[i] + RIGHT_COLOR[i]) / 2;

	  if (this_point.y > check.y)
	    for (i = 0; i < this_channels; i++)
	      *dest++ = (BOTTOM_COLOR[i] * pixel_ratio) + (MEDIAN_COLOR[i] * (1 - pixel_ratio));
	  else 
	    for (i = 0; i < this_channels; i++)
	      *dest++ = (RIGHT_COLOR[i] * pixel_ratio) + (MEDIAN_COLOR[i] * (1 - pixel_ratio));
	  break;
	}
      }
    }

    for (col = 0; col < right; col++) {
      for (i = 0; i < this_channels; i++)
	*dest++ = *src++;
    }
    if ((row % 5) == 0)
      gimp_do_progress(row, area.y);
  }
  memcpy(dest, src, bottom * width * this_channels);
}

static void debug() {
  printf("x_grid: %ld, y_grid %ld\n", grid.x, grid.y);
  printf("top: %ld, bottom: %ld, left %ld, right: %ld\n", top, bottom, left, right);
}

/* callback for the text input boxes */
static void text_callback (int item_ID, void *client_data, void *call_data) {
  *((long*) client_data) = abs(atoi(call_data));
}

/* callback function for the radio buttons */
static void radio_callback (int item_ID, void *client_data, void *call_data) {
  *((long*) client_data) = *((long*) call_data);

  /* don't let SOLID be set unless in BLOCK mode */
  if ((item_ID == radio_ID[SOLID]) && !v.radio[BLOCKS]) {
    v.radio[SOLID] = FALSE;
    gimp_change_item (dialog_ID, radio_ID[SOLID], sizeof (v.radio[SOLID]), &v.radio[SOLID]);
  }

  /* if switching to PYRAMID mode, unset SOLID if it is set */
  if ((item_ID == radio_ID[PYRAMIDS]) && v.radio[SOLID]) {
    v.radio[SOLID] = FALSE;
    gimp_change_item (dialog_ID, radio_ID[SOLID], sizeof (v.radio[SOLID]), &v.radio[SOLID]);
  }

  if (item_ID == radio_ID[MASK]) {
    v.radio[MASK] = TRUE;
    gimp_change_item (dialog_ID, radio_ID[MASK], sizeof (v.radio[MASK]), &v.radio[MASK]);
  }
}

static void image_menu_callback(int item_ID, void *client_data, void *call_data) {
  *((long*) client_data) = *((long*) call_data);
}

static void ok_callback (int item_ID, void *client_data, void *call_data) {
  gimp_close_dialog (dialog_ID, 1);
}

static void cancel_callback (int item_ID, void *client_data, void *call_data) {
  gimp_close_dialog (dialog_ID, 0);
}

/* generate the dialog window */
static int do_dialog(void) {
  int group_ID;
  int size_ID;
  int row_ID, temp_ID;
  int frame_ID;
  char buf[16];

  dialog_ID = gimp_new_dialog ("Extrude");
  group_ID = gimp_new_row_group (dialog_ID, DEFAULT, NORMAL, "");

  /* the "SIZE" frame contains size, blocks, and pyramids */
  sprintf(buf, "%ld", v.size);
  frame_ID = gimp_new_frame(dialog_ID, group_ID, "Size");

  row_ID = gimp_new_row_group(dialog_ID, frame_ID, DEFAULT, "");
  size_ID = gimp_new_text(dialog_ID, row_ID, buf);
  gimp_add_callback (dialog_ID, size_ID, text_callback, &v.size);

  row_ID = gimp_new_column_group(dialog_ID, row_ID, RADIO, "");

  radio_ID[PYRAMIDS] = gimp_new_radio_button (dialog_ID, row_ID, "Pyramids");
  gimp_change_item (dialog_ID, radio_ID[PYRAMIDS], sizeof (v.radio[PYRAMIDS]), &v.radio[PYRAMIDS]);
  gimp_add_callback (dialog_ID, radio_ID[PYRAMIDS], radio_callback, &v.radio[PYRAMIDS]);

  radio_ID[BLOCKS] = gimp_new_radio_button (dialog_ID, row_ID, "Blocks");
  gimp_change_item (dialog_ID, radio_ID[BLOCKS], sizeof (v.radio[BLOCKS]), &v.radio[BLOCKS]);
  gimp_add_callback (dialog_ID, radio_ID[BLOCKS], radio_callback, &v.radio[BLOCKS]);

  /* the "DEPTH" frame contains depth, random & levels */
  frame_ID = gimp_new_frame(dialog_ID, group_ID, "Depth");

  temp_ID = gimp_new_row_group(dialog_ID, frame_ID, DEFAULT, "");  
  row_ID = gimp_new_column_group(dialog_ID, temp_ID, RADIO, "");
  radio_ID[LEVELS] = gimp_new_radio_button (dialog_ID, row_ID, "Image");
  gimp_change_item (dialog_ID, radio_ID[LEVELS], sizeof (v.radio[LEVELS]), &v.radio[LEVELS]);
  gimp_add_callback (dialog_ID, radio_ID[LEVELS], radio_callback, &v.radio[LEVELS]);

  radio_ID[RANDOM] = gimp_new_radio_button (dialog_ID, row_ID, "Rand");
  gimp_change_item (dialog_ID, radio_ID[RANDOM], sizeof (v.radio[RANDOM]), &v.radio[RANDOM]);
  gimp_add_callback (dialog_ID, radio_ID[RANDOM], radio_callback, &v.radio[RANDOM]);

  radio_ID[MAP] = gimp_new_radio_button (dialog_ID, row_ID, "Map");
  gimp_change_item (dialog_ID, radio_ID[MAP], sizeof (v.radio[MAP]), &v.radio[MAP]);
  gimp_add_callback (dialog_ID, radio_ID[MAP], radio_callback, &v.radio[MAP]);

  /* image to be used as the depth map */
  row_ID = gimp_new_column_group(dialog_ID, temp_ID, DEFAULT, "");
  image_menu_ID = gimp_new_image_menu (dialog_ID, row_ID, 
				       IMAGE_CONSTRAIN_RGB | IMAGE_CONSTRAIN_GRAY,
				       "Depth Map");
  gimp_add_callback (dialog_ID, image_menu_ID, image_menu_callback, &map_ID); 

  /* the last frame contains "solid front" and "mask incomplete" */
  frame_ID = gimp_new_frame(dialog_ID, group_ID, "");
  row_ID = gimp_new_row_group(dialog_ID, frame_ID, DEFAULT, "");
  radio_ID[SOLID] = gimp_new_check_button (dialog_ID, row_ID, "Solid Front (blocks only)");
  gimp_change_item (dialog_ID, radio_ID[SOLID], sizeof (v.radio[SOLID]), &v.radio[SOLID]);
  gimp_add_callback (dialog_ID, radio_ID[SOLID], radio_callback, &v.radio[SOLID]);

  radio_ID[MASK] = gimp_new_check_button (dialog_ID, row_ID, "Mask Incomplete");
  gimp_change_item (dialog_ID, radio_ID[MASK], sizeof (v.radio[MASK]), &v.radio[MASK]);
  gimp_add_callback (dialog_ID, radio_ID[MASK], radio_callback, &v.radio[MASK]);

  radio_ID[TEXTURE] = gimp_new_check_button (dialog_ID, row_ID, "Use Texture Mapping");
  gimp_change_item (dialog_ID, radio_ID[TEXTURE], sizeof (v.radio[TEXTURE]), &v.radio[TEXTURE]);
  gimp_add_callback (dialog_ID, radio_ID[TEXTURE], radio_callback, &v.radio[TEXTURE]);

  /* callbacks for ok and cancel */
  gimp_add_callback (dialog_ID, gimp_ok_item_id (dialog_ID), ok_callback, 0);
  gimp_add_callback (dialog_ID, gimp_cancel_item_id (dialog_ID), cancel_callback, 0);

  return(gimp_show_dialog (dialog_ID));
}









