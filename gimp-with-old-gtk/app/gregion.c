/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <string.h>
#include <values.h>
#include <math.h>
#include "appenv.h"
#include "errors.h"
#include "gregion.h"
#include "paint_funcs.h"
#include "undo.h"

#define ROUND(x) ((int) (x + 0.5))

/* id's for gregions... */
static int gregion_id = 0;

GRegion *
gregion_new (width, height)
     int width;
     int height;
{
  GRegion *new;

  new = (GRegion *) xmalloc (sizeof (GRegion));
  new->width = width;
  new->height = height;
  new->empty = TRUE;
  new->boundary = NULL;
  new->bounds_known = TRUE;
  new->boundary_known = TRUE;
  new->num_segments = 0;
  new->x1 = new->y1 = 0;
  new->x2 = width;
  new->y2 = height;
  new->id = gregion_id++;

  new->mask = mask_buf_new (width, height);

  return new;
}


GRegion *
gregion_copy (src, dest)
     GRegion * src;
     GRegion * dest;
{
  GRegion * new;

  /*  If the dest is NULL, create a new structure to return  */
  if (!dest)
    new = gregion_new (src->width, src->height);
  /*  Otherwise, add the src region to the dest region  */
  else
    {
      new = dest;
      if ((dest->width != src->width) || (dest->height != src->height))
	warning ("Copying may fail due to unequal extents between src and dest.");
    }

  /* copy the contents */
  memcpy (mask_buf_data (new->mask), mask_buf_data (src->mask),
	  src->width * src->height);

  return new;
}


void
gregion_free (region)
     GRegion * region;
{
  mask_buf_free (region->mask);
  if (region->boundary)
    xfree (region->boundary);
  xfree (region);
}


BoundSeg *
gregion_boundary (region, num_segs)
     GRegion * region;
     int * num_segs;
{
  PixelRegion bPR;
  int x1, y1, x2, y2;

  if (region->boundary_known)
    {
      *num_segs = region->num_segments;
      return region->boundary;
    }

  /* free the out of date boundary segments */
  if (region->boundary)
    xfree (region->boundary);

  if (gregion_bounds (region, &x1, &y1, &x2, &y2))
    {
      bPR.bytes = 1;
      bPR.w = region->width;
      bPR.h = region->height;
      bPR.rowstride = region->width * bPR.bytes;
      bPR.data = mask_buf_data (region->mask);

      region->boundary = find_mask_boundary (&bPR, &region->num_segments,
					     x1, y1, x2, y2);
      *num_segs = region->num_segments;
    }
  else
    {
      region->num_segments = 0;
      *num_segs = region->num_segments;
      region->boundary = NULL;
    }

  region->boundary_known = TRUE;
  return region->boundary;
}


int
gregion_value (region, x, y)
     GRegion * region;
     int x, y;
{
  unsigned char * data;

  if (x < 0 || x >= region->width || y < 0 || y >= region->height)
    return 0;
  
  data = mask_buf_data (region->mask) + 
    y * region->width + x;
  
  return *data;
}


int
gregion_bounds (region, x1, y1, x2, y2)
     GRegion * region;
     int * x1, * y1;
     int * x2, * y2;
{
  unsigned char * data;
  int x, y;

  /*  if the region's bounds have already been reliably calculated...  */
  if (region->bounds_known)
    {
      *x1 = region->x1;
      *y1 = region->y1;
      *x2 = region->x2;
      *y2 = region->y2;

      return (region->empty) ? FALSE : TRUE;
    }

  /*  if the region is empty, return FALSE  */
  if (gregion_is_empty (region))
    {
      region->empty = TRUE;
      *x1 = *y1 = 0;
      *x2 = region->width;
      *y2 = region->height;
    }
  /*  otherwise, go through and calculate the bounds  */
  else
    {
      region->empty = FALSE;
      *x1 = region->width;
      *y1 = region->height;
      *x2 = 0;
      *y2 = 0;
      
      data = mask_buf_data (region->mask);
      
      for (y = 0; y < region->height; y++)
	for (x = 0; x < region->width; x++, data++)
	  if (*data)
	    {
	      if (x < *x1)
		*x1 = x;
	      if (x > *x2)
		*x2 = x;
	      if (y < *y1)
		*y1 = y;
	      if (y > *y2)
		*y2 = y;
	    }

      *x2 = BOUNDS (*x2 + 1, 0, region->width);
      *y2 = BOUNDS (*y2 + 1, 0, region->height);
    }

  region->x1 = *x1;
  region->y1 = *y1;
  region->x2 = *x2;
  region->y2 = *y2;
  region->bounds_known = TRUE;

  return (region->empty) ? FALSE : TRUE;
}


int
gregion_is_empty (region)
     GRegion * region;
{
  unsigned char * data = mask_buf_data (region->mask);
  int size = region->width * region->height;

  if (region->bounds_known)
    return region->empty;

  /*  check if any pixel in the mask is non-zero  */
  while (size--)
    {
      if (*data++)
	return 0;
    }

  return 1;
}


void
gregion_add_segment (region, x, y, width, value)
     GRegion * region;
     int x, y;
     int width;
     int value;
{
  unsigned char * data;
  int val;
  int x2;

  /*  check horizontal extents...  */
  x2 = x + width;
  if (x2 < 0) x2 = 0;
  if (x2 > region->width) x2 = region->width;
  if (x < 0) x = 0;
  if (x > region->width) x = region->width;
  width = x2 - x;
  if (!width) return;

  if (y < 0 || y > region->height)
    return;

  region->bounds_known = FALSE;
  region->boundary_known = FALSE;

  data = mask_buf_data (region->mask) + y * region->width + x;

  while (width--)
    {
      val = *data + value;
      if (val > 255)
	val = 255;
      *data++ = val;
    }
}


void
gregion_sub_segment (region, x, y, width, value)
     GRegion * region;
     int x, y;
     int width;
     int value;
{
  unsigned char * data;
  int val;
  int x2;

  /*  check horizontal extents...  */
  x2 = x + width;
  if (x2 < 0) x2 = 0;
  if (x2 > region->width) x2 = region->width;
  if (x < 0) x = 0;
  if (x > region->width) x = region->width;
  width = x2 - x;
  if (!width) return;

  if (y < 0 || y > region->height)
    return;

  region->bounds_known = FALSE;
  region->boundary_known = FALSE;

  data = mask_buf_data (region->mask) + y * region->width + x;

  while (width--)
    {
      val = *data - value;
      if (val < 0)
	val = 0;
      *data++ = val;
    }
}


void
gregion_combine_rect (region, op, x, y, w, h)
     GRegion * region;
     int op;
     int x, y;
     int w, h;
{
  int i;

  for (i = y; i < y + h; i++)
    {
      if (i >= 0 && i < region->height)
	switch (op)
	  {
	  case ADD :
	    gregion_add_segment (region, x, i, w, 255);
	    break;
	  case SUB :
	    gregion_sub_segment (region, x, i, w, 255);
	    break;
	  }
    }
}


void
gregion_combine_ellipse (region, op, x, y, w, h, aa)
     GRegion * region;
     int op;
     int x, y;
     int w, h;
     int aa;    /*  antialias selection?  */
{
  int i, j;
  int x0, x1, x2;
  int val, last;
  float a_sqr, b_sqr, aob_sqr;
  float w_sqr, h_sqr;
  float y_sqr;
  float t0, t1;
  float r;
  float cx, cy;
  float rad;
  float dist;
  

  if (!w || !h)
    return;

  a_sqr = (w * w / 4.0);
  b_sqr = (h * h / 4.0);
  aob_sqr = a_sqr / b_sqr;

  cx = x + w / 2.0;
  cy = y + h / 2.0;

  for (i = y; i < (y + h); i++)
    {
      if (i >= 0 && i < region->height)
	{
	  /*  Non-antialiased code  */
	  if (!aa)
	    {
	      y_sqr = (i + 0.5 - cy) * (i + 0.5 - cy);
	      rad = sqrt (a_sqr - a_sqr * y_sqr / (double) b_sqr);
	      x1 = ROUND (cx - rad);
	      x2 = ROUND (cx + rad);
	  
	      switch (op)
		{
		case ADD :
		  gregion_add_segment (region, x1, i, (x2 - x1), 255);
		  break;
		case SUB :
		  gregion_sub_segment (region, x1, i, (x2 - x1), 255);
		  break;
		}
	    }
	  /*  antialiasing  */
	  else
	    {
	      x0 = x;
	      last = 0;
	      h_sqr = (i + 0.5 - cy) * (i + 0.5 - cy);
	      for (j = x; j < (x + w); j++)
		{
		  w_sqr = (j + 0.5 - cx) * (j + 0.5 - cx);

		  if (h_sqr != 0)
		    {
		      t0 = w_sqr / h_sqr;
		      t1 = a_sqr / (t0 + aob_sqr);
		      r = sqrt (t1 + t0 * t1);
		      rad = sqrt (w_sqr + h_sqr);
		      dist = rad - r;
		    }
		  else 
		    dist = -1.0;

		  if (dist < -0.5)
		    val = 255;
		  else if (dist < 0.5)
		    val = (int) (255 * (1 - (dist + 0.5)));
		  else
		    val = 0;

		  if (last != val && last)
		    {
		      switch (op)
			{
			case ADD :
			  gregion_add_segment (region, x0, i, j - x0, last);
			  break;
			case SUB :
			  gregion_sub_segment (region, x0, i, j - x0, last);
			  break;
			}
		    }

		  if (last != val)
		    {
		      x0 = j;
		      last = val;
		    }
		}

	      if (last)
		{
		  if (op == ADD)
		    gregion_add_segment (region, x0, i, j - x0, last);
		  else if (op == SUB)
		    gregion_sub_segment (region, x0, i, j - x0, last);
		}
	    }

	}
    }      
}


void
gregion_combine_region (region, op, add_on)
     GRegion * region;
     int op;
     GRegion * add_on;
{
  unsigned char * src, * dest;
  int val;
  int x, y;

  for (y = 0; y < add_on->height; y++)
    {
      src = mask_buf_data (add_on->mask) + y * add_on->width;
      dest = mask_buf_data (region->mask) + y * region->width;

      for (x = 0; x < add_on->width; x++)
	{
	  switch (op)
	    {
	    case ADD:
	      val = *dest + *src;
	      if (val > 255) val = 255;
	      break;
	    case SUB:
	      val = *dest - *src;
	      if (val < 0) val = 0;
	      break;
	    default:
	      break;
	    }
	  *dest++ = val;
	  src++;
	}
    }

  region->bounds_known = FALSE;
  region->boundary_known = FALSE;
}


void
gregion_feather (input, output, radius, op)
     GRegion *input, *output;
     double radius;
     int op;
{
  PixelRegion srcPR, destPR;

  srcPR.bytes = 1;
  srcPR.w = input->width;
  srcPR.h = input->height;
  srcPR.rowstride = srcPR.w;
  srcPR.data = mask_buf_data (input->mask);

  destPR.bytes = 1;
  destPR.rowstride = output->width;
  destPR.data = mask_buf_data (output->mask);

  gaussian_blur_region (&srcPR, &destPR, radius, op);

  output->bounds_known = FALSE;
  output->boundary_known = FALSE;
}


void
gregion_push_undo (gimage_ID, region)
     int gimage_ID;
     GRegion * region;
{
  int x1, y1, x2, y2;
  MaskBuf * new;
  PixelRegion srcPR, destPR;

  if (gregion_bounds (region, &x1, &y1, &x2, &y2))
    {
      srcPR.bytes = 1;
      srcPR.w = (x2 - x1);
      srcPR.h = (y2 - y1);
      srcPR.rowstride = region->width;
      srcPR.data = mask_buf_data (region->mask) + y1 * region->width + x1;

      new = mask_buf_new (srcPR.w, srcPR.h);
      new->x = x1;  new->y = y1;
      destPR.rowstride = new->width;
      destPR.data = mask_buf_data (new);

      copy_region (&srcPR, &destPR);

    }
  else
    new = NULL;

  /* push the undo buffer onto the undo stack */
  undo_push_mask (gimage_ID, new);
}


void
gregion_clear (gimage_ID, region)
     int gimage_ID;
     GRegion * region;
{
  unsigned char * data = mask_buf_data (region->mask);
  int size = region->width * region->height;

  /*  push the current gregion onto the undo stack  */
  gregion_push_undo (gimage_ID, region);

  /*  clear the region  */
  memset (data, 0, size);

  region->empty = TRUE;
  region->x1 = 0; region->y1 = 0;
  region->x2 = region->width;
  region->y2 = region->height;
  region->bounds_known = TRUE;
  region->boundary_known = FALSE;
}


void
gregion_invert (gimage_ID, region)
     int gimage_ID;
     GRegion * region;
{
  unsigned char * data = mask_buf_data (region->mask);
  int size = region->width * region->height;

  /*  push the current gregion onto the undo stack  */
  gregion_push_undo (gimage_ID, region);

  /*  subtract each pixel in the mask from 255  */
  while (size--)
    {
      *data = 255 - *data;
      data++;
    }

  region->bounds_known = FALSE;
  region->boundary_known = FALSE;
}


void
gregion_sharpen (gimage_ID, region)
     int gimage_ID;
     GRegion * region;
{
  unsigned char * data = mask_buf_data (region->mask);
  int size = region->width * region->height;

  /*  push the current gregion onto the undo stack  */
  gregion_push_undo (gimage_ID, region);

  /*  if a pixel in the mask has a non-zero value, make it 255  */
  while (size--)
    {
      if (*data > HALF_WAY)
	*data++ = 255;
      else
	*data++ = 0;
    }

  region->bounds_known = FALSE;
}


void
gregion_all (gimage_ID, region)
     int gimage_ID;
     GRegion * region;
{
  unsigned char * data = mask_buf_data (region->mask);
  int size = region->width * region->height;

  /*  push the current gregion onto the undo stack  */
  gregion_push_undo (gimage_ID, region);

  /*  if a pixel in the mask has a non-zero value, make it 255  */
  while (size--)
    *data++ = 255;

  region->x1 = 0;
  region->y1 = 0;
  region->x2 = region->width;
  region->y2 = region->height;

  region->empty = FALSE;
  region->bounds_known = TRUE;
  region->boundary_known = FALSE;
}


void
gregion_border (gimage_ID, region, radius)
     int gimage_ID;
     GRegion * region;
     int radius;
{
  PixelRegion destPR;
  BoundSeg * bs;
  int num_segs;

  /*  push the current gregion onto the undo stack  */
  gregion_push_undo (gimage_ID, region);

  bs = gregion_boundary (region, &num_segs);

  destPR.bytes = 1;
  destPR.w = region->width;
  destPR.h = region->height;
  destPR.rowstride = region->width * destPR.bytes;
  destPR.data = mask_buf_data (region->mask);

  border_region (&destPR, bs, num_segs, radius);

  region->bounds_known = FALSE;
  region->boundary_known = FALSE;
}



