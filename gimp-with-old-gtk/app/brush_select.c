/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "appenv.h"
#include "actionarea.h"
#include "brushes.h"
#include "brush_select.h"
#include "buildmenu.h"
#include "colormaps.h"
#include "disp_callbacks.h"
#include "errors.h"
#include "gconvert.h"
#include "paint_funcs.h"
#include "visual.h"


#define STD_CELL_WIDTH    36
#define STD_CELL_HEIGHT   36
#define NUM_BRUSH_COLUMNS 5
#define NUM_BRUSH_ROWS    5
#define MAX_WIN_WIDTH     (STD_CELL_WIDTH * NUM_BRUSH_COLUMNS)
#define MAX_WIN_HEIGHT    (STD_CELL_HEIGHT * NUM_BRUSH_ROWS)
#define MARGIN_WIDTH      3
#define MARGIN_HEIGHT     3

#define BRUSH_EVENT_MASK  GDK_EXPOSURE_MASK | \
                          GDK_BUTTON_PRESS_MASK | \
			  GDK_BUTTON_RELEASE_MASK | \
                          GDK_BUTTON1_MOTION_MASK | \
			  GDK_ENTER_NOTIFY_MASK

/*  local function prototypes  */
static void create_preview               (BrushSelectP);
static void display_brush                (BrushSelectP, GBrushP, int, int, int);
static void display_brushes              (BrushSelectP);
static void display_setup                (BrushSelectP);
static void draw_preview                 (BrushSelectP);
static void preview_calc_scrollbar       (BrushSelectP);
static void brush_select_show_selected   (BrushSelectP, int, int);
static void update_active_brush_field    (BrushSelectP);
static void brush_select_close_callback  (GtkWidget *, gpointer, gpointer);
static void brush_select_refresh_callback(GtkWidget *, gpointer, gpointer);
static void brush_select_cancel_callback (GtkWidget *, gpointer, gpointer);
static void paint_mode_menu_callback     (GtkWidget *, gpointer, gpointer);
static gint brush_select_events          (GtkWidget *, GdkEvent *);
static void brush_select_disconnect      (GtkObserver *, GtkData *);
static gint preview_scroll_update        (GtkObserver *, GtkData *);
static gint opacity_scale_update         (GtkObserver *, GtkData *);
static gint spacing_scale_update         (GtkObserver *, GtkData *);

/*  the option menu items -- the paint modes  */
static MenuItem option_items[] =
{
  { "Normal", 0, 0, paint_mode_menu_callback, (gpointer) NORMAL_MODE, NULL, NULL },
  { "Dissolve", 0, 0, paint_mode_menu_callback, (gpointer) DISSOLVE_MODE, NULL, NULL },
  { "Multiply", 0, 0, paint_mode_menu_callback, (gpointer) MULTIPLY_MODE, NULL, NULL },
  { "Screen", 0, 0, paint_mode_menu_callback, (gpointer) SCREEN_MODE, NULL, NULL },
  { "Difference", 0, 0, paint_mode_menu_callback, (gpointer) DIFFERENCE_MODE, NULL, NULL },
  { "Addition", 0, 0, paint_mode_menu_callback, (gpointer) ADDITION_MODE, NULL, NULL },
  { "Subtract", 0, 0, paint_mode_menu_callback, (gpointer) SUBTRACT_MODE, NULL, NULL },
  { "Darken Only", 0, 0, paint_mode_menu_callback, (gpointer) DARKEN_ONLY_MODE, NULL, NULL },
  { "Lighten Only", 0, 0, paint_mode_menu_callback, (gpointer) LIGHTEN_ONLY_MODE, NULL, NULL },
  { "Hue", 0, 0, paint_mode_menu_callback, (gpointer) HUE_MODE, NULL, NULL },
  { "Saturation", 0, 0, paint_mode_menu_callback, (gpointer) SATURATION_MODE, NULL, NULL },
  { "Color", 0, 0, paint_mode_menu_callback, (gpointer) COLOR_MODE, NULL, NULL },
  { "Value", 0, 0, paint_mode_menu_callback, (gpointer) VALUE_MODE, NULL, NULL },
  { NULL, 0, 0, NULL, NULL, NULL, NULL }
};

/*  the action area structure  */
static ActionAreaItem action_items[] =
{
  { "Close", brush_select_close_callback, NULL, NULL },
  { "Refresh", brush_select_refresh_callback, NULL, NULL },
  { "Cancel", brush_select_cancel_callback, NULL, NULL },
};

static double old_opacity;
static int old_spacing;
static int old_paint_mode;
static int last_x, last_y;
static int scroll_row, scroll_col, scroll_index;

static void
create_preview (bsp)
     BrushSelectP bsp;
{
  /*  Get the maximum brush extents  */
  bsp->cell_width = STD_CELL_WIDTH;
  bsp->cell_height = STD_CELL_HEIGHT;

  bsp->width = MAX_WIN_WIDTH;
  bsp->height = MAX_WIN_HEIGHT;

  bsp->preview = gtk_drawing_area_new (bsp->width, bsp->height,
				       brush_select_events,
				       BRUSH_EVENT_MASK);
  gtk_widget_set_user_data (bsp->preview, (gpointer) bsp);
  gtk_container_add (bsp->frame, bsp->preview);
  gtk_widget_show (bsp->preview);

  if (bsp->image)
    image_buf_destroy (bsp->image);
  bsp->image = image_buf_create (GRAY_BUF, bsp->width, bsp->height);
  if (!bsp->image)
    fatal_error ("Could not allocate image for brush select dialog.");
}


GtkWidget *
create_paint_mode_menu (callback)
     GtkCallback callback;
{
  GtkWidget *menu;
  int i;

  for (i = 0; i <= VALUE_MODE; i++)
    option_items[i].callback = callback;

  menu = build_menu (option_items, NULL);

  return menu;
}


BrushSelectP
brush_select_new ()
{
  BrushSelectP bsp;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *options_box;
  GtkWidget *sbar;
  GtkWidget *label;
  GtkWidget *util_box;
  GtkWidget *menu;
  GtkWidget *option_menu;
  GtkWidget *action_area;
  GtkWidget *slider;

  gtk_push_visual (gray_visual);
  gtk_push_colormap (graycmap);

  bsp = xmalloc (sizeof (_BrushSelect));
  bsp->preview = NULL;
  bsp->image = NULL;
  bsp->gc = NULL;

  /*  The shell and main vbox  */
  bsp->shell = gtk_window_new ("Brush Selection", GTK_WINDOW_TOPLEVEL);
  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_add (bsp->shell, vbox);

  /*  The horizontal box containing preview & scrollbar & options box */
  hbox = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  bsp->frame = gtk_frame_new (NULL);
  gtk_frame_set_type (bsp->frame, GTK_SHADOW_IN);
  gtk_box_pack (hbox, bsp->frame, FALSE, FALSE, 0, GTK_PACK_START);
  bsp->sbar_data = (GtkDataAdjustment *)
    gtk_data_adjustment_new (0, 0, MAX_WIN_HEIGHT, 1, 1, MAX_WIN_HEIGHT);
  sbar = gtk_vscrollbar_new (bsp->sbar_data);
  gtk_box_pack (hbox, sbar, FALSE, FALSE, 0, GTK_PACK_START);

  /*  Create the brush preview window and the underlying image  */
  create_preview (bsp);

  gtk_widget_show (sbar);
  gtk_widget_show (bsp->frame);

  /*  options box  */
  options_box = gtk_vbox_new (FALSE, 10);
  gtk_container_set_border_width (options_box, 5);
  gtk_box_pack (hbox, options_box, TRUE, TRUE, 0, GTK_PACK_START);

  /*  Create the active brush label  */
  util_box = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (util_box, 0);
  gtk_box_pack (options_box, util_box, FALSE, FALSE, 0, GTK_PACK_START);
  bsp->brush_name = gtk_label_new ("Active");
  gtk_box_pack (util_box, bsp->brush_name, FALSE, FALSE, 2, GTK_PACK_START);

  gtk_widget_show (bsp->brush_name);
  gtk_widget_show (util_box);

  /*  Brush size label  */
  util_box = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (util_box, 0);
  gtk_box_pack (options_box, util_box, FALSE, FALSE, 0, GTK_PACK_START);
  label = gtk_label_new ("Size:");
  gtk_box_pack (util_box, label, FALSE, FALSE, 2, GTK_PACK_START);
  bsp->brush_size = gtk_label_new ("(0x0)");
  gtk_box_pack (util_box, bsp->brush_size, FALSE, FALSE, 2, GTK_PACK_START);

  gtk_widget_show (label);
  gtk_widget_show (bsp->brush_size);
  gtk_widget_show (util_box);

  /*  Create the paint mode option menu  */
  old_paint_mode = get_brush_paint_mode ();

  util_box = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (util_box, 0);
  gtk_box_pack (options_box, util_box, FALSE, FALSE, 0, GTK_PACK_START);
  label = gtk_label_new ("Mode:");
  gtk_box_pack (util_box, label, FALSE, FALSE, 2, GTK_PACK_START);
  menu = create_paint_mode_menu (paint_mode_menu_callback);
  option_menu = gtk_option_menu_new ();
  gtk_box_pack (util_box, option_menu, FALSE, FALSE, 2, GTK_PACK_START);

  gtk_widget_show (label);
  gtk_widget_show (option_menu);
  gtk_widget_show (util_box);
  gtk_option_menu_set_menu (option_menu, menu);

  /*  Create the opacity scale widget  */
  old_opacity = get_brush_opacity ();

  util_box = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (util_box, 0);
  gtk_box_pack (options_box, util_box, FALSE, FALSE, 0, GTK_PACK_START);
  label = gtk_label_new ("Opacity:");
  gtk_box_pack (util_box, label, FALSE, FALSE, 2, GTK_PACK_START);
  bsp->opacity_data = (GtkDataAdjustment *)
    gtk_data_adjustment_new (100.0, 0.0, 100.0, 1.0, 1.0, 1.0);
  slider = gtk_hscale_new (bsp->opacity_data);
  gtk_box_pack (util_box, slider, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_scale_set_value_pos (slider, GTK_POS_TOP);
  bsp->opacity_observer.update = opacity_scale_update;
  bsp->opacity_observer.disconnect = brush_select_disconnect;
  bsp->opacity_observer.user_data = bsp;
  gtk_data_attach ((GtkData *) bsp->opacity_data, &bsp->opacity_observer);

  gtk_widget_show (label);
  gtk_widget_show (slider);
  gtk_widget_show (util_box);

  /*  Create the brush spacing scale widget  */
  old_spacing = get_brush_spacing ();

  util_box = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (util_box, 0);
  gtk_box_pack (options_box, util_box, FALSE, FALSE, 0, GTK_PACK_START);
  label = gtk_label_new ("Spacing:");
  gtk_box_pack (util_box, label, FALSE, FALSE, 2, GTK_PACK_START);
  bsp->spacing_data = (GtkDataAdjustment *)
    gtk_data_adjustment_new (0.0, 0.0, 1000.0, 1.0, 1.0, 1.0);
  slider = gtk_hscale_new (bsp->spacing_data);
  gtk_box_pack (util_box, slider, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_scale_set_value_pos (slider, GTK_POS_TOP);
  bsp->spacing_observer.update = spacing_scale_update;
  bsp->spacing_observer.disconnect = brush_select_disconnect;
  bsp->spacing_observer.user_data = bsp;
  gtk_data_attach ((GtkData *) bsp->spacing_data, &bsp->spacing_observer);

  gtk_widget_show (label);
  gtk_widget_show (slider);
  gtk_widget_show (util_box);

  /*  The action area  */
  action_items[0].user_data = bsp;
  action_items[1].user_data = bsp;
  action_items[2].user_data = bsp;
  action_area = build_action_area (action_items, 3);
  gtk_box_pack (vbox, action_area, TRUE, TRUE, 2, GTK_PACK_START);

  gtk_widget_show (action_area);
  gtk_widget_show (options_box);
  gtk_widget_show (hbox);
  gtk_widget_show (vbox);
  gtk_widget_show (bsp->shell);

  gtk_pop_visual ();
  gtk_pop_colormap ();

  return bsp;
}


void
brush_select_select (bsp, index)
     BrushSelectP bsp;
     int index;
{
  int row, col;

  update_active_brush_field (bsp);
  row = index / NUM_BRUSH_COLUMNS;
  col = index - row * NUM_BRUSH_COLUMNS;

  brush_select_show_selected (bsp, row, col);
}


void
brush_select_free (bsp)
     BrushSelectP bsp;
{
  if (bsp)
    {
      gdk_gc_destroy (bsp->gc);
      if (bsp->image)
	image_buf_destroy (bsp->image);
      xfree (bsp);
    }
}


static void
clear_brush (bsp, col, row)
     BrushSelectP bsp;
     int col, row;
{
  int i;
  unsigned char * buf;
  int offset_x, offset_y;
  int yend;
  int ystart;

  buf = (unsigned char *) xmalloc (sizeof (char) * bsp->cell_width);
  memset (buf, 255, sizeof (char) * bsp->cell_width);

  offset_x = col * bsp->cell_width;
  offset_y = row * bsp->cell_height - bsp->scroll_offset;

  ystart = BOUNDS (offset_y, 0, image_buf_height (bsp->image));
  yend = BOUNDS (offset_y + bsp->cell_height, 0, image_buf_height (bsp->image));

  for (i = ystart; i < yend; i++)
    image_buf_draw_row (bsp->image, buf, offset_x, i, bsp->cell_width);

  free (buf);
}


static void
display_brush (bsp, brush, col, row, scaling)
     BrushSelectP bsp;
     GBrushP brush;
     int col, row;
     int scaling;
{
  TempBuf * brush_buf;
  PixelRegion srcPR, destPR;
  unsigned char * src, *s;
  unsigned char * buf, *b;
  int width, height;
  int offset_x, offset_y;
  int yend;
  int ystart;
  int i, j;
  int new_buf;

  buf = (unsigned char *) xmalloc (sizeof (char) * bsp->cell_width);

  if ((brush->mask->width > bsp->cell_width ||
       brush->mask->height > bsp->cell_height) &&
      scaling)
    {
      srcPR.bytes = brush->mask->bytes;
      srcPR.w = brush->mask->width;
      srcPR.h = brush->mask->height;
      srcPR.x = srcPR.y = 0;
      srcPR.rowstride = srcPR.bytes * brush->mask->width;
      srcPR.data = temp_buf_data (brush->mask);

      if (((float) brush->mask->width / (float) bsp->cell_width) >
	  ((float) brush->mask->height / (float) bsp->cell_height))
	{
	  width = bsp->cell_width;
	  height = (brush->mask->height * bsp->cell_width) / brush->mask->width;
	}
      else
	{
	  width = (brush->mask->width * bsp->cell_height) / brush->mask->height;
	  height = bsp->cell_height;
	}

      brush_buf = mask_buf_new (width, height);
      new_buf = 1;
      destPR.x = 0;
      destPR.y = 0;
      destPR.w = width;
      destPR.h = height;
      destPR.rowstride = srcPR.bytes * width;
      destPR.data = mask_buf_data (brush_buf);

      scale_region (&srcPR, &destPR);
    }
  else
    {
      brush_buf = brush->mask;
      new_buf = 0;
    }

  /*  calculate the offset into the image  */
  width = (brush_buf->width > bsp->cell_width) ? bsp->cell_width :
    brush_buf->width;
  height = (brush_buf->height > bsp->cell_height) ? bsp->cell_height :
    brush_buf->height;

  offset_x = col * bsp->cell_width + ((bsp->cell_width - width) >> 1);
  offset_y = row * bsp->cell_height + ((bsp->cell_height - height) >> 1)
    - bsp->scroll_offset;

  ystart = BOUNDS (offset_y, 0, image_buf_height (bsp->image));
  yend = BOUNDS (offset_y + height, 0, image_buf_height (bsp->image));

  /*  Get the pointer into the brush mask data  */
  src = mask_buf_data (brush_buf) +
    (brush->off_y + ystart - offset_y) * brush_buf->width +
    brush->off_x * brush_buf->bytes;

  for (i = ystart; i < yend; i++)
    {
      /*  Invert the mask for display.  We're doing this because
       *  a value of 255 in the  mask means it is full intensity.
       *  However, it makes more sense for full intensity to show
       *  up as black in this brush preview window...
       */
      s = src;
      b = buf;
      for (j = 0; j < width; j++)
	*b++ = 255 - *s++;

      image_buf_draw_row (bsp->image, buf, offset_x, i, width);

      src += brush_buf->width;
    }

  if (new_buf)
    mask_buf_free (brush_buf);
  xfree (buf);
}


static void
display_setup (bsp)
     BrushSelectP bsp;
{
  unsigned char * buf;
  int i;

  buf = (unsigned char *) xmalloc (sizeof (char) * image_buf_width (bsp->image));

  /*  Set the buffer to white  */
  memset (buf, 255, image_buf_width (bsp->image));

  /*  Set the image buffer to white  */
  for (i = 0; i < image_buf_height (bsp->image); i++)
    image_buf_draw_row (bsp->image, buf, 0, i, image_buf_width (bsp->image));

  xfree (buf);
}


static void
display_brushes (bsp)
     BrushSelectP bsp;
{
  link_ptr list = brush_list;    /*  the global brush list  */
  int row, col;
  GBrushP brush;

  /*  setup the display area  */
  display_setup (bsp);

  row = col = 0;
  while (list)
    {
      brush = (GBrushP) list->data;

      /*  Display the brush  */
      display_brush (bsp, brush, col, row, 1);

      /*  increment the counts  */
      if (++col == NUM_BRUSH_COLUMNS)
	{
	  row ++;
	  col = 0;
	}

      list = next_item (list);
    }
}


static void
brush_select_show_selected (bsp, row, col)
     BrushSelectP bsp;
     int row, col;
{
  static int old_row = 0;
  static int old_col = 0;
  unsigned char * buf;
  int yend;
  int ystart;
  int offset_x, offset_y;
  int i;

  buf = (unsigned char *) xmalloc (sizeof (char) * bsp->cell_width);

  if (old_col != col || old_row != row)
    {
      /*  remove the old selection  */
      offset_x = old_col * bsp->cell_width;
      offset_y = old_row * bsp->cell_height - bsp->scroll_offset;

      ystart = BOUNDS (offset_y , 0, image_buf_height (bsp->image));
      yend = BOUNDS (offset_y + bsp->cell_height, 0, image_buf_height (bsp->image));

      /*  set the buf to white  */
      memset (buf, 255, bsp->cell_width);

      for (i = ystart; i < yend; i++)
	{
	  if (i == offset_y || i == (offset_y + bsp->cell_height - 1))
	    image_buf_draw_row (bsp->image, buf, offset_x, i, bsp->cell_width);
	  else
	    {
	      image_buf_draw_row (bsp->image, buf, offset_x, i, 1);
	      image_buf_draw_row (bsp->image, buf, offset_x + bsp->cell_width - 1, i, 1);
	    }
	}
      image_buf_put_area (bsp->image, bsp->preview->window, bsp->gc, offset_x, ystart,
			  bsp->cell_width, (yend - ystart));
    }

  /*  make the new selection  */
  offset_x = col * bsp->cell_width;
  offset_y = row * bsp->cell_height - bsp->scroll_offset;

  ystart = BOUNDS (offset_y , 0, image_buf_height (bsp->image));
  yend = BOUNDS (offset_y + bsp->cell_height, 0, image_buf_height (bsp->image));

  /*  set the buf to black  */
  memset (buf, 0, bsp->cell_width);

  for (i = ystart; i < yend; i++)
    {
      if (i == offset_y || i == (offset_y + bsp->cell_height - 1))
	image_buf_draw_row (bsp->image, buf, offset_x, i, bsp->cell_width);
      else
	{
	  image_buf_draw_row (bsp->image, buf, offset_x, i, 1);
	  image_buf_draw_row (bsp->image, buf, offset_x + bsp->cell_width - 1, i, 1);
	}
    }
  image_buf_put_area (bsp->image, bsp->preview->window, bsp->gc, offset_x, ystart,
		      bsp->cell_width, (yend - ystart));

  old_row = row;
  old_col = col;

  xfree (buf);
}


static void
draw_preview (bsp)
     BrushSelectP bsp;
{
  /*  Draw the image buf to the preview window  */
  if (bsp->gc)
    image_buf_put (bsp->image, bsp->preview->window, bsp->gc);
}


static void
preview_calc_scrollbar (bsp)
     BrushSelectP bsp;
{
  int num_rows;
  int page_size;
  int max;

  bsp->scroll_offset = 0;
  num_rows = (num_brushes + NUM_BRUSH_COLUMNS - 1) / NUM_BRUSH_COLUMNS;
  max = num_rows * STD_CELL_HEIGHT;
  if (!num_rows) num_rows = 1;
  page_size = STD_CELL_HEIGHT * NUM_BRUSH_ROWS;

  bsp->sbar_data->value = bsp->scroll_offset;
  bsp->sbar_data->upper = max;
  bsp->sbar_data->page_size = (page_size < max) ? page_size : max;
  bsp->sbar_data->page_increment = (page_size >> 1);
  bsp->sbar_data->step_increment = STD_CELL_HEIGHT;

  gtk_data_notify ((GtkData *) bsp->sbar_data);
}


static void
update_active_brush_field (bsp)
     BrushSelectP bsp;
{
  GBrushP brush;
  char buf[32];

  brush = get_active_brush ();

  if (!brush)
    return;

  /*  Set brush name  */
  gtk_label_set (bsp->brush_name, brush->name);

  /*  Set brush size  */
  sprintf (buf, "(%d X %d)", brush->mask->width, brush->mask->height);
  gtk_label_set (bsp->brush_size, buf);

  /*  Set brush spacing  */
  bsp->spacing_data->value = get_brush_spacing ();
  gtk_data_notify ((GtkData *) bsp->spacing_data);
}


static gint
brush_select_events (widget, event)
     GtkWidget *widget;
     GdkEvent *event;
{
  GBrushP active;
  GdkEventButton *bevent;
  GdkEventMotion *mevent;
  BrushSelectP bsp;
  GBrushP brush;
  int row, col, index;
  int x_extent, y_extent;
  int tx, ty;

  bsp = (BrushSelectP) gtk_widget_get_user_data (widget);
  if (!bsp)
    return FALSE;

  switch (event->type)
    {
    case GDK_EXPOSE:
      /*  If this is the first exposure  */
      if (!bsp->gc)
	{
	  bsp->gc = gdk_gc_new (bsp->preview->window);

	  /*  Setup the scrollbar observer  */
	  bsp->sbar_observer.update = preview_scroll_update;
	  bsp->sbar_observer.disconnect = brush_select_disconnect;
	  bsp->sbar_observer.user_data = (gpointer) bsp;
	  gtk_data_attach ((GtkData *) bsp->sbar_data, &bsp->sbar_observer);

	  preview_calc_scrollbar (bsp);

	  /*  render the brushes into the newly created image structure  */
	  display_brushes (bsp);

	  /*  update the active selection  */
	  active = get_active_brush ();
	  if (active)
	    brush_select_select (bsp, active->index);
	}

      draw_preview (bsp);
      break;

    case GDK_BUTTON_PRESS:
      bevent = (GdkEventButton *) event;

      if (bevent->button == 1)
	{
	  col = bevent->x / bsp->cell_width;
	  row = (bevent->y + bsp->scroll_offset) / bsp->cell_height;
	  index = row * NUM_BRUSH_COLUMNS + col;

	  /*  Update these variables for scrolling in the brush subwindows  */
	  last_x = bevent->x;
	  last_y = bevent->y;
	  scroll_row = row;
	  scroll_col = col;
	  scroll_index = index;

	  brush = get_brush_by_index (index);

	  gdk_pointer_grab (bsp->preview->window, FALSE,
			    GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
			    NULL, NULL, bevent->time);
	  if (brush)
	    {
	      /*  Make this brush the active brush  */
	      select_brush (brush);

	      brush->off_x = BOUNDS (brush->mask->width - bsp->cell_width,
				     0, brush->mask->width) >> 2;
	      brush->off_y = BOUNDS (brush->mask->height - bsp->cell_height,
				     0, brush->mask->height) >> 2;
	    }
	}
      break;

    case GDK_BUTTON_RELEASE:
      bevent = (GdkEventButton *) event;

      brush = get_brush_by_index (scroll_index);

      brush->off_x = brush->off_y = 0;
      clear_brush (bsp, scroll_col, scroll_row);
      display_brush (bsp, brush, scroll_col, scroll_row, 1);
      brush_select_show_selected (bsp, scroll_row, scroll_col);

      gdk_pointer_ungrab (bevent->time);
      break;

    case GDK_MOTION_NOTIFY:
      mevent = (GdkEventMotion *) event;
      if (mevent->is_hint)
	{
	  gdk_window_get_pointer (widget->window, &tx, &ty, NULL);
	  mevent->x = tx;
	  mevent->y = ty;
	}

      brush = get_brush_by_index (scroll_index);

      /*  What are the brush subwindow scrolling extents?  */
      x_extent = brush->mask->width - bsp->cell_width;
      if (x_extent < 0) x_extent = 0;
      y_extent = brush->mask->height - bsp->cell_height;
      if (y_extent < 0) y_extent = 0;

      /*  Update the subwindow offsets for the brush  */
      brush->off_x = BOUNDS (brush->off_x - (mevent->x - last_x), 0, x_extent);
      brush->off_y = BOUNDS (brush->off_y - (mevent->y - last_y), 0, y_extent);

      last_x = mevent->x;
      last_y = mevent->y;

      display_brush (bsp, brush, scroll_col, scroll_row, 0);
      brush_select_show_selected (bsp, scroll_row, scroll_col);
      break;

    case GDK_ENTER_NOTIFY:
      install_colormap (GRAY);
      break;

    default:
      break;
    }

  return FALSE;
}

static void
brush_select_close_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  BrushSelectP bsp;

  bsp = (BrushSelectP) client_data;

  old_paint_mode = get_brush_paint_mode ();
  old_opacity = get_brush_opacity ();
  old_spacing = get_brush_spacing ();

  if (GTK_WIDGET_VISIBLE (bsp->shell))
    gtk_widget_hide (bsp->shell);
}


static void
brush_select_refresh_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  BrushSelectP bsp;
  GBrushP active;

  bsp = (BrushSelectP) client_data;

  /*  re-init the brush list  */
  brushes_init ();

  /*  recalculate scrollbar extents  */
  preview_calc_scrollbar (bsp);

  /*  render the brushes into the newly created image structure  */
  display_brushes (bsp);

  /*  update the active selection  */
  active = get_active_brush ();
  if (active)
    brush_select_select (bsp, active->index);

  /*  update the display  */
  draw_preview (bsp);
}


static void
brush_select_cancel_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  BrushSelectP bsp;

  bsp = (BrushSelectP) client_data;

  set_brush_paint_mode (old_paint_mode);
  set_brush_opacity (old_opacity);
  set_brush_spacing (old_spacing);

  if (GTK_WIDGET_VISIBLE (bsp->shell))
    gtk_widget_hide (bsp->shell);
}


static gint
preview_scroll_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataAdjustment *adj_data;
  BrushSelectP bsp;
  GBrushP active;
  int row, col;

  adj_data = (GtkDataAdjustment *) data;
  bsp = observer->user_data;

  if (bsp)
    {
      bsp->scroll_offset = adj_data->value;
      display_brushes (bsp);

      active = get_active_brush ();
      if (active)
	{
	  row = active->index / NUM_BRUSH_COLUMNS;
	  col = active->index - row * NUM_BRUSH_COLUMNS;
	  brush_select_show_selected (bsp, row, col);
	}

      draw_preview (bsp);
    }

  return FALSE;
}

static void
paint_mode_menu_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  set_brush_paint_mode ((int) client_data);
}


static gint
opacity_scale_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataAdjustment *adj_data;

  adj_data = (GtkDataAdjustment *) data;
  set_brush_opacity (adj_data->value / 100.0);

  return FALSE;
}


static gint
spacing_scale_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataAdjustment *adj_data;

  adj_data = (GtkDataAdjustment *) data;
  set_brush_spacing ((int) adj_data->value);

  return FALSE;
}


static void
brush_select_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}
