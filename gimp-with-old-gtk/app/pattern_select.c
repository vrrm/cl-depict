/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "appenv.h"
#include "actionarea.h"
#include "patterns.h"
#include "pattern_select.h"
#include "buildmenu.h"
#include "colormaps.h"
#include "disp_callbacks.h"
#include "errors.h"
#include "gconvert.h"
#include "paint_funcs.h"
#include "visual.h"


#define STD_CELL_WIDTH    36
#define STD_CELL_HEIGHT   36
#define NUM_PATTERN_COLUMNS 6
#define NUM_PATTERN_ROWS    5
#define MAX_WIN_WIDTH     (STD_CELL_WIDTH * NUM_PATTERN_COLUMNS)
#define MAX_WIN_HEIGHT    (STD_CELL_HEIGHT * NUM_PATTERN_ROWS)
#define MARGIN_WIDTH      1
#define MARGIN_HEIGHT     1
#define PATTERN_EVENT_MASK GDK_BUTTON1_MOTION_MASK | \
                           GDK_EXPOSURE_MASK | \
                           GDK_BUTTON_PRESS_MASK | \
			   GDK_ENTER_NOTIFY_MASK

/*  local function prototypes  */
static void create_preview                   (PatternSelectP);
static void display_pattern                  (PatternSelectP, GPatternP, int, int, int);
static void display_patterns                 (PatternSelectP);
static void display_setup                    (PatternSelectP);
static void draw_preview                     (PatternSelectP);
static void preview_calc_scrollbar           (PatternSelectP);
static void pattern_select_show_selected     (PatternSelectP, int, int);
static void update_active_pattern_field      (PatternSelectP);
static void pattern_select_close_callback    (GtkWidget *, gpointer, gpointer);
static void pattern_select_refresh_callback  (GtkWidget *, gpointer, gpointer);
static gint pattern_select_events            (GtkWidget *, GdkEvent *);
static void pattern_select_scroll_disconnect (GtkObserver *, GtkData *);
static gint pattern_select_scroll_update     (GtkObserver *, GtkData *);

/*  the action area structure  */
static ActionAreaItem action_items[2] =
{
  { "Close", pattern_select_close_callback, NULL, NULL },
  { "Refresh", pattern_select_refresh_callback, NULL, NULL },
};

static int last_x, last_y;
static int scroll_row, scroll_col, scroll_index;


static void
create_preview (psp)
     PatternSelectP psp;
{
  /*  Get the maximum pattern extents  */
  psp->cell_width = STD_CELL_WIDTH;
  psp->cell_height = STD_CELL_HEIGHT;

  psp->width = MAX_WIN_WIDTH;
  psp->height = MAX_WIN_HEIGHT;

  psp->preview = gtk_drawing_area_new (psp->width, psp->height,
				       pattern_select_events,
				       PATTERN_EVENT_MASK);
  gtk_widget_set_user_data (psp->preview, (gpointer) psp);
  gtk_container_add (psp->frame, psp->preview);
  gtk_widget_show (psp->preview);

  if (psp->image)
    image_buf_destroy (psp->image);
  psp->image = image_buf_create (COLOR_BUF, psp->width, psp->height);
  if (!psp->image)
    fatal_error ("Could not allocate image for pattern select dialog.");
}

PatternSelectP
pattern_select_new ()
{
  PatternSelectP psp;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *sbar;
  GtkWidget *label_box;
  GtkWidget *label;
  GtkWidget *action_area;

  psp = xmalloc (sizeof (_PatternSelect));
  psp->preview = NULL;
  psp->image = NULL;
  psp->gc = NULL;

  /*  The shell and main vbox  */
  psp->shell = gtk_window_new ("Pattern Selection", GTK_WINDOW_TOPLEVEL);
  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_add (psp->shell, vbox);

  /*  Create the active pattern label  */
  label_box = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (label_box, 2);
  gtk_box_pack (vbox, label_box, FALSE, FALSE, 0, GTK_PACK_START);
  label = gtk_label_new ("Pattern:");
  gtk_box_pack (label_box, label, FALSE, FALSE, 2, GTK_PACK_START);
  psp->pattern_name = gtk_label_new ("Active");
  gtk_box_pack (label_box, psp->pattern_name, FALSE, FALSE, 2, GTK_PACK_START);

  gtk_widget_show (label);
  gtk_widget_show (psp->pattern_name);
  gtk_widget_show (label_box);

  /*  Pattern size label  */
  label_box = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (label_box, 2);
  gtk_box_pack (vbox, label_box, FALSE, FALSE, 0, GTK_PACK_START);
  label = gtk_label_new ("Size:");
  gtk_box_pack (label_box, label, FALSE, FALSE, 2, GTK_PACK_START);
  psp->pattern_size = gtk_label_new ("(0x0)");
  gtk_box_pack (label_box, psp->pattern_size, FALSE, FALSE, 2, GTK_PACK_START);

  gtk_widget_show (label);
  gtk_widget_show (psp->pattern_size);
  gtk_widget_show (label_box);

  /*  The horizontal box containing preview & scrollbar  */
  hbox = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  psp->frame = gtk_frame_new (NULL);
  gtk_frame_set_type (psp->frame, GTK_SHADOW_IN);
  gtk_box_pack (hbox, psp->frame, FALSE, FALSE, 0, GTK_PACK_START);
  psp->sbar_data = (GtkDataAdjustment *)
    gtk_data_adjustment_new (0, 0, MAX_WIN_HEIGHT, 1, 1, MAX_WIN_HEIGHT);
  sbar = gtk_vscrollbar_new (psp->sbar_data);
  gtk_box_pack (hbox, sbar, FALSE, FALSE, 0, GTK_PACK_START);

  /*  Create the pattern preview window and the underlying image  */
  create_preview (psp);

  gtk_widget_show (sbar);
  gtk_widget_show (hbox);
  gtk_widget_show (psp->frame);

  /*  The action area  */
  action_items[0].user_data = psp;
  action_items[1].user_data = psp;
  action_area = build_action_area (action_items, 2);
  gtk_box_pack (vbox, action_area, FALSE, FALSE, 0, GTK_PACK_START);

  gtk_widget_show (action_area);
  gtk_widget_show (vbox);
  gtk_widget_show (psp->shell);

  return psp;
}

void
pattern_select_select (psp, index)
     PatternSelectP psp;
     int index;
{
  int row, col;

  update_active_pattern_field (psp);
  row = index / NUM_PATTERN_COLUMNS;
  col = index - row * NUM_PATTERN_COLUMNS;

  pattern_select_show_selected (psp, row, col);
}

void
pattern_select_free (psp)
     PatternSelectP psp;
{
  if (psp)
    {
      gdk_gc_destroy (psp->gc);
      if (psp->image)
	image_buf_destroy (psp->image);
      xfree (psp);
    }
}

static void
display_pattern (psp, pattern, col, row, scaling)
     PatternSelectP psp;
     GPatternP pattern;
     int col, row;
     int scaling;
{
  TempBuf * pattern_buf;
  unsigned char * src, *s;
  unsigned char * buf, *b;
  int cell_width, cell_height;
  int width, height;
  int rowstride;
  int offset_x, offset_y;
  int yend;
  int ystart;
  int i, j;

  buf = (unsigned char *) xmalloc (sizeof (char) * psp->cell_width * 3);

  pattern_buf = pattern->mask;

  /*  calculate the offset into the image  */
  cell_width = psp->cell_width - 2*MARGIN_WIDTH;
  cell_height = psp->cell_height - 2*MARGIN_HEIGHT;
  width = (pattern_buf->width > cell_width) ? cell_width :
    pattern_buf->width;
  height = (pattern_buf->height > cell_height) ? cell_height :
    pattern_buf->height;

  offset_x = col * psp->cell_width + ((cell_width - width) >> 1) + MARGIN_WIDTH;
  offset_y = row * psp->cell_height + ((cell_height - height) >> 1)
    - psp->scroll_offset + MARGIN_HEIGHT;

  ystart = BOUNDS (offset_y, 0, image_buf_height (psp->image));
  yend = BOUNDS (offset_y + height, 0, image_buf_height (psp->image));

  /*  Get the pointer into the pattern mask data  */
  rowstride = pattern_buf->width * pattern_buf->bytes;
  src = temp_buf_data (pattern_buf) +
    (pattern->off_y + ystart - offset_y) * rowstride +
    pattern->off_x * pattern_buf->bytes;

  for (i = ystart; i < yend; i++)
    {
      s = src;
      b = buf;

      if (pattern_buf->bytes == 1)
	for (j = 0; j < width; j++)
	  {
	    *b++ = *s;
	    *b++ = *s;
	    *b++ = *s++;
	  }
      else
	for (j = 0; j < width; j++)
	  {
	    *b++ = *s++;
	    *b++ = *s++;
	    *b++ = *s++;
	  }

      image_buf_draw_row (psp->image, buf, offset_x, i, width);

      src += rowstride;
    }

  xfree (buf);
}

static void
display_setup (psp)
     PatternSelectP psp;
{
  unsigned char * buf;
  int i;

  buf = (unsigned char *) xmalloc (sizeof (char) * image_buf_width (psp->image) * 3);

  /*  Set the buffer to white  */
  memset (buf, 255, image_buf_width (psp->image) * 3);

  /*  Set the image buffer to white  */
  for (i = 0; i < image_buf_height (psp->image); i++)
    image_buf_draw_row (psp->image, buf, 0, i, image_buf_width (psp->image));

  xfree (buf);
}

static void
display_patterns (psp)
     PatternSelectP psp;
{
  link_ptr list = pattern_list;    /*  the global pattern list  */
  int row, col;
  GPatternP pattern;

  /*  setup the display area  */
  display_setup (psp);

  row = col = 0;
  while (list)
    {
      pattern = (GPatternP) list->data;

      /*  Display the pattern  */
      display_pattern (psp, pattern, col, row, 1);

      /*  increment the counts  */
      if (++col == NUM_PATTERN_COLUMNS)
	{
	  row ++;
	  col = 0;
	}

      list = next_item (list);
    }
}

static void
pattern_select_show_selected (psp, row, col)
     PatternSelectP psp;
     int row, col;
{
  static int old_row = 0;
  static int old_col = 0;
  unsigned char * buf;
  int yend;
  int ystart;
  int offset_x, offset_y;
  int i;

  buf = (unsigned char *) xmalloc (sizeof (char) * psp->cell_width * 3);

  if (old_col != col || old_row != row)
    {
      /*  remove the old selection  */
      offset_x = old_col * psp->cell_width;
      offset_y = old_row * psp->cell_height - psp->scroll_offset;

      ystart = BOUNDS (offset_y , 0, image_buf_height (psp->image));
      yend = BOUNDS (offset_y + psp->cell_height, 0, image_buf_height (psp->image));

      /*  set the buf to white  */
      memset (buf, 255, psp->cell_width * 3);

      for (i = ystart; i < yend; i++)
	{
	  if (i == offset_y || i == (offset_y + psp->cell_height - 1))
	    image_buf_draw_row (psp->image, buf, offset_x, i, psp->cell_width);
	  else
	    {
	      image_buf_draw_row (psp->image, buf, offset_x, i, 1);
	      image_buf_draw_row (psp->image, buf, offset_x + psp->cell_width - 1, i, 1);
	    }
	}
      image_buf_put_area (psp->image, psp->preview->window, psp->gc, offset_x, ystart,
			  psp->cell_width, (yend - ystart));
    }

  /*  make the new selection  */
  offset_x = col * psp->cell_width;
  offset_y = row * psp->cell_height - psp->scroll_offset;

  ystart = BOUNDS (offset_y , 0, image_buf_height (psp->image));
  yend = BOUNDS (offset_y + psp->cell_height, 0, image_buf_height (psp->image));

  /*  set the buf to black  */
  memset (buf, 0, psp->cell_width * 3);

  for (i = ystart; i < yend; i++)
    {
      if (i == offset_y || i == (offset_y + psp->cell_height - 1))
	image_buf_draw_row (psp->image, buf, offset_x, i, psp->cell_width);
      else
	{
	  image_buf_draw_row (psp->image, buf, offset_x, i, 1);
	  image_buf_draw_row (psp->image, buf, offset_x + psp->cell_width - 1, i, 1);
	}
    }
  image_buf_put_area (psp->image, psp->preview->window, psp->gc, offset_x, ystart,
		      psp->cell_width, (yend - ystart));

  old_row = row;
  old_col = col;

  xfree (buf);
}

static void
draw_preview (psp)
     PatternSelectP psp;
{
  /*  Draw the image buf to the preview window  */
  if (psp->gc)
    image_buf_put (psp->image, psp->preview->window, psp->gc);
}

static void
preview_calc_scrollbar (psp)
     PatternSelectP psp;
{
  int num_rows;
  int page_size;
  int max;

  psp->scroll_offset = 0;
  num_rows = (num_patterns + NUM_PATTERN_COLUMNS - 1) / NUM_PATTERN_COLUMNS;
  max = num_rows * STD_CELL_HEIGHT;
  if (!num_rows) num_rows = 1;
  page_size = STD_CELL_HEIGHT * NUM_PATTERN_ROWS;

  psp->sbar_data->value = psp->scroll_offset;
  psp->sbar_data->upper = max;
  psp->sbar_data->page_size = (page_size < max) ? page_size : max;
  psp->sbar_data->page_increment = (page_size >> 1);
  psp->sbar_data->step_increment = STD_CELL_HEIGHT;

  gtk_data_notify ((GtkData *) psp->sbar_data);
}

static void
update_active_pattern_field (psp)
     PatternSelectP psp;
{
  GPatternP pattern;
  char buf[32];

  pattern = get_active_pattern ();

  if (!pattern)
    return;

  /*  Set pattern name  */
  gtk_label_set (psp->pattern_name, pattern->name);

  /*  Set pattern size  */
  sprintf (buf, "(%d X %d)", pattern->mask->width, pattern->mask->height);
  gtk_label_set (psp->pattern_size, buf);
}

static gint
pattern_select_events (widget, event)
     GtkWidget *widget;
     GdkEvent *event;
{
  GPatternP active;
  GdkEventButton *bevent;
  GdkEventMotion *mevent;
  PatternSelectP psp;
  GPatternP pattern;
  int row, col, index;
  int x_extent, y_extent;
  int tx, ty;

  psp = (PatternSelectP) gtk_widget_get_user_data (widget);
  if (!psp)
    return FALSE;

  switch (event->type)
    {
    case GDK_EXPOSE:
      /*  If this is the first exposure  */
      if (!psp->gc)
	{
	  psp->gc = gdk_gc_new (psp->preview->window);

	  /*  Setup the scrollbar observer  */
	  psp->sbar_observer.update = pattern_select_scroll_update;
	  psp->sbar_observer.disconnect = pattern_select_scroll_disconnect;
	  psp->sbar_observer.user_data = (gpointer) psp;
	  gtk_data_attach ((GtkData *) psp->sbar_data, &psp->sbar_observer);

	  /*  render the patterns into the newly created image structure  */
	  preview_calc_scrollbar (psp);
	  display_patterns (psp);

	  /*  update the active selection  */
	  active = get_active_pattern ();
	  if (active)
	    pattern_select_select (psp, active->index);
	}

      draw_preview (psp);
      break;

    case GDK_BUTTON_PRESS:
      bevent = (GdkEventButton *) event;

      if (bevent->button == 1)
	{
	  col = bevent->x / psp->cell_width;
	  row = (bevent->y + psp->scroll_offset) / psp->cell_height;
	  index = row * NUM_PATTERN_COLUMNS + col;

	  /*  Update these variables for scrolling in the pattern subwindows  */
	  last_x = bevent->x;
	  last_y = bevent->y;
	  scroll_row = row;
	  scroll_col = col;
	  scroll_index = index;

	  pattern = get_pattern_by_index (index);

	  gdk_pointer_grab (psp->preview->window, FALSE,
			    GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
			    NULL, NULL, bevent->time);
	  if (pattern)
	    /*  Make this brush the active brush  */
	    select_pattern (pattern);
	}
      break;

    case GDK_BUTTON_RELEASE:
      bevent = (GdkEventButton *) event;
      gdk_pointer_ungrab (bevent->time);
      break;

    case GDK_MOTION_NOTIFY:
      mevent = (GdkEventMotion *) event;
      if (mevent->is_hint)
	{
	  gdk_window_get_pointer (widget->window, &tx, &ty, NULL);
	  mevent->x = tx;
	  mevent->y = ty;
	}

      pattern = get_pattern_by_index (scroll_index);

      /*  What are the pattern subwindow scrolling extents?  */
      x_extent = pattern->mask->width - psp->cell_width;
      if (x_extent < 0) x_extent = 0;
      y_extent = pattern->mask->height - psp->cell_height;
      if (y_extent < 0) y_extent = 0;

      /*  Update the subwindow offsets for the pattern  */
      pattern->off_x = BOUNDS (pattern->off_x - (mevent->x - last_x), 0, x_extent);
      pattern->off_y = BOUNDS (pattern->off_y - (mevent->y - last_y), 0, y_extent);

      last_x = mevent->x;
      last_y = mevent->y;

      display_pattern (psp, pattern, scroll_col, scroll_row, 0);
      pattern_select_show_selected (psp, scroll_row, scroll_col);
      break;

    case GDK_ENTER_NOTIFY:
      install_colormap (RGB);
      break;

    default:
      break;
    }

  return FALSE;
}

static void
pattern_select_close_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  PatternSelectP psp;
  psp = (PatternSelectP) client_data;
  if (GTK_WIDGET_VISIBLE (psp->shell))
    gtk_widget_hide (psp->shell);
}

static void
pattern_select_refresh_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  PatternSelectP psp;
  GPatternP active;

  psp = (PatternSelectP) client_data;

  /*  re-init the pattern list  */
  patterns_init ();

  /*  recalculate scrollbar extents  */
  preview_calc_scrollbar (psp);

  /*  render the patterns into the newly created image structure  */
  display_patterns (psp);

  /*  update the active selection  */
  active = get_active_pattern ();
  if (active)
    pattern_select_select (psp, active->index);

  /*  update the display  */
  draw_preview (psp);
}

static void
pattern_select_scroll_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}

static gint
pattern_select_scroll_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataAdjustment *adj_data;
  PatternSelectP psp;
  GPatternP active;
  int row, col;

  adj_data = (GtkDataAdjustment *) data;
  psp = observer->user_data;

  if (psp)
    {
      psp->scroll_offset = adj_data->value;
      display_patterns (psp);

      active = get_active_pattern ();
      if (active)
	{
	  row = active->index / NUM_PATTERN_COLUMNS;
	  col = active->index - row * NUM_PATTERN_COLUMNS;
	  pattern_select_show_selected (psp, row, col);
	}

      draw_preview (psp);
    }

  return FALSE;
}
