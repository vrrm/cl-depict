/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "draw_core.h"
#include "edit_selection.h"
#include "errors.h"
#include "move.h"
#include "gimage_mask.h"
#include "gdisplay.h"
#include "linked.h"

typedef struct _MoveTool MoveTool;

struct _MoveTool
{
  DrawCore *      core;       /*  Core select object                      */
};

/*  move tool action functions  */

static void   move_tool_button_press      (Tool *, GdkEventButton *, gpointer);
static void   move_tool_button_release    (Tool *, GdkEventButton *, gpointer);
static void   move_tool_motion            (Tool *, GdkEventMotion *, gpointer);

static void *move_options = NULL;


/*  move action functions  */

static void
move_tool_button_press (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  GDisplay * gdisp;
  MoveTool * move;

  gdisp = (GDisplay *) gdisp_ptr;
  move = (MoveTool *) tool->private;

  gdk_pointer_grab (gdisp->canvas->window, FALSE,
		    GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
		    NULL, NULL, bevent->time);
      
  tool->state = ACTIVE;
  tool->gdisp_ptr = gdisp_ptr;

  if (gdisplay_mask_value (gdisp, bevent->x, bevent->y) > HALF_WAY)
    init_edit_selection (tool, gdisp_ptr, bevent);
}

void
move_tool_button_release (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  MoveTool * move;
  GDisplay * gdisp;

  gdisp = (GDisplay *) gdisp_ptr;
  move = (MoveTool *) tool->private;

  gdk_pointer_ungrab (bevent->time);
  gdk_flush ();

  /*  draw_core_stop (free_sel->core, tool);*/

  tool->state = INACTIVE;

  /*  First take care of the case where the user "cancels" the action  */
  if (! (bevent->state & GDK_BUTTON3_MASK))
    {
    }
}

void
move_tool_motion (tool, mevent, gdisp_ptr)
     Tool *tool;
     GdkEventMotion *mevent;
     gpointer gdisp_ptr;
{
  MoveTool * move;
  GDisplay * gdisp;

  if (tool->state != ACTIVE)
    return;

  gdisp = (GDisplay *) gdisp_ptr;
  move = (MoveTool *) tool->private;
}


void
move_tool_control (tool, action, gdisp_ptr)
     Tool *tool;
     int action;
     gpointer gdisp_ptr;
{
  MoveTool * move;

  move = (MoveTool *) tool->private;

  switch (action)
    {
    case PAUSE : 
      /*  draw_core_pause (free_sel->core, tool);*/
      break;
    case RESUME :
      /*  draw_core_resume (free_sel->core, tool);*/
      break;
    case HALT :
      /*  draw_core_stop (free_sel->core, tool);*/
      break;
    }
}


void
move_tool_draw (tool)
     Tool * tool;
{
  MoveTool * move;

  move = (MoveTool *) tool->private;
}


Tool *
tools_new_move_tool ()
{
  Tool * tool;
  MoveTool * private;

  if (! move_options)
    move_options = tools_register_no_options (MOVE, "Move Tool Options");

  tool = (Tool *) xmalloc (sizeof (Tool));
  private = (MoveTool *) xmalloc (sizeof (MoveTool));

  private->core = draw_core_new (move_tool_draw);

  tool->type = MOVE;
  tool->state = INACTIVE;
  tool->scroll_lock = 0;   /*  Allow scrolling  */
  tool->private = (void *) private;
  tool->button_press_func = move_tool_button_press;
  tool->button_release_func = move_tool_button_release;
  tool->motion_func = move_tool_motion;
  tool->arrow_keys_func = edit_sel_arrow_keys_func;
  tool->control_func = move_tool_control;

  return tool;
}


void
tools_free_move_tool (tool)
     Tool * tool;
{
  MoveTool * move;

  move = (MoveTool *) tool->private;

  draw_core_free (move->core);
  xfree (move);
}




