/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "appenv.h"
#include "colormaps.h"
#include "patterns.h"
#include "pattern_header.h"
#include "pattern_select.h"
#include "buildmenu.h"
#include "colormaps.h"
#include "errors.h"
#include "general.h"
#include "gimprc.h"
#include "linked.h"
#include "menus.h"


/*  global variables  */
GPatternP           active_pattern = NULL;
link_ptr            pattern_list = NULL;
int                 num_patterns = 0;

PatternSelectP      pattern_select_dialog = NULL;

/*  static variables  */
static int          have_default_pattern = 0;

/*  static function prototypes  */
static link_ptr     insert_pattern_in_list   (link_ptr, GPatternP);
static GPatternP    load_pattern             (char *);
static void         free_pattern             (GPatternP);

/*  function declarations  */
void
patterns_init ()
{
  DIR *dir;
  GPatternP pattern;
  char path[256];
  char filename[256];
  char *home = NULL;
  char *local_path, *token;
  struct stat buf;
  int err;
  struct dirent * dir_ent;

  if (pattern_list)
    patterns_free ();

  pattern_list = NULL;
  num_patterns = 0;

  if (!pattern_path)
    return;

  home = getenv ("HOME");

  local_path = xstrdup (pattern_path);
  token = strtok (local_path, ":");

  while (token)
    {
      if (*token == '~')
	sprintf (path, "%s%s", home, token + 1);
      else
	sprintf (path, "%s", token);

      /*  see if the directory exists and if it has any items in it  */
      err = stat (path, &buf);
      if (!err && S_ISDIR (buf.st_mode))
	{
	  if (token[strlen (token) - 1] != '/')
	    strcat (path, "/");

	  /*  open the pattern directory  */
	  if (! (dir= opendir (path)))
	    warning ("error reading patterns from directory \"%s\"", path);
	  else
	    {
	      while ( (dir_ent = readdir (dir)) )
		{
		  sprintf (filename, "%s%s", path, dir_ent->d_name);
		  /*  double check the filename--especially that it is not a sub-dir  */
		  err = stat (filename, &buf);
		  if (!err && S_ISREG (buf.st_mode))
		    {
		      pattern = load_pattern (filename);
		      if (pattern)
			/*  insert pattern alphabetically  */
			pattern_list = insert_pattern_in_list (pattern_list, pattern);
		      /*  Check if the current pattern is the default one  */
		      if (default_pattern && dir_ent->d_name)
			if (strcmp (default_pattern, dir_ent->d_name) == 0)
			  {
			    active_pattern = pattern;
			    have_default_pattern = 1;
			  }
		    }
		}
	      closedir (dir);
	    }
	}

      token = strtok (NULL, ":");
    }

  /*  assign indexes to the loaded patterns  */
  {
    link_ptr list = pattern_list;

    while (list)
      {
	/*  Set the pattern index  */
	((GPattern *) list->data)->index = num_patterns++;
	list = next_item (list);
      }
  }

  xfree (local_path);
}


void
patterns_free ()
{
  link_ptr list;
  GPatternP pattern;

  list = pattern_list;

  while (list)
    {
      pattern = (GPatternP) list->data;
      free_pattern (pattern);
      list = next_item (list);
    }

  free_list (list);

  have_default_pattern = 0;
  active_pattern = NULL;
  num_patterns = 0;
  pattern_list = NULL;
}


void
pattern_select_dialog_free ()
{
  if (pattern_select_dialog)
    {
      pattern_select_free (pattern_select_dialog);
      pattern_select_dialog = NULL;
    }
}


GPatternP
get_active_pattern ()
{
  if (have_default_pattern)
    {
      have_default_pattern = 0;
      if (!active_pattern)
	fatal_error ("Specified default pattern not found!");

    }
  else if (! active_pattern && pattern_list)
    active_pattern = (GPatternP) pattern_list->data;

  return active_pattern;
}


static link_ptr
insert_pattern_in_list (list, pattern)
     link_ptr list;
     GPatternP pattern;
{
  link_ptr tmp;
  link_ptr prev;
  link_ptr new_link;
  GPatternP b;
  int val;

  /* Insert the item in the list */
  if (list)
    {
      prev = NULL;
      tmp = list;
      do {
	  if (tmp)
	    {
	      b = (GPatternP) tmp->data;

	      /* do the comparison needed for the insertion sort */
	      val = strcmp (pattern->name, b->name);
	    }
	  else
	    val = -1;

          if (val <= 0)
            {
	      /* this is the place the item goes */
	      /* Insert the item into the list. We'll have to create
	       *  a new link and then do a little insertion.
	       */
              new_link = alloc_list ();
	      if (!new_link)
		fatal_error ("Unable to allocate memory");

              new_link->data = pattern;
              new_link->next = tmp;

              if (prev)
                prev->next = new_link;
              if (tmp == list)
                list = new_link;

	      return list;
            }

	  /* Advance to the next item in the list.
	   */
          prev = tmp;
          tmp = next_item (tmp);
        } while (prev);
    }
  else
    /* There are no items in the pattern list, so we'll just start
     *  one right now.
     */
    list = add_to_list (list, pattern);

  return list;
}


static GPatternP
load_pattern (filename)
     char * filename;
{
  GPatternP pattern;
  FILE * fp;
  int bn_size;
  unsigned char buf [sz_PatternHeader];
  PatternHeader header;
  unsigned int * hp;
  int i;

  pattern = (GPatternP) xmalloc (sizeof (GPattern));

  pattern->filename = xstrdup (filename);
  pattern->name = NULL;
  pattern->mask = NULL;
  pattern->off_x = 0;
  pattern->off_y = 0;

  /*  Open the requested file  */
  if (! (fp = fopen (filename, "r")))
    {
      warning ("can't load pattern \"%s\"\n", filename);
      free_pattern (pattern);
      return NULL;
    }

  /*  Read in the header size  */
  if ((fread (buf, 1, sz_PatternHeader, fp)) < sz_PatternHeader)
    {
      warning ("GIMP: error reading GIMP pattern \"%s\"\n", filename);
      free_pattern (pattern);
      return NULL;
    }

  /*  rearrange the bytes in each unsigned int  */
  hp = (unsigned int *) &header;
  for (i = 0; i < (sz_PatternHeader / 4); i++)
    hp [i] = (buf [i * 4] << 24) + (buf [i * 4 + 1] << 16) +
             (buf [i * 4 + 2] << 8) + (buf [i * 4 + 3]);

  /*  Check for correct file format */
  if (header.magic_number != GPATTERN_MAGIC)
    {
      /*  One thing that can save this error is if the pattern is version 1  */
      if (header.version != 1)
	{
	  fclose (fp);
	  free_pattern (pattern);
	  return NULL;
	}
    }
  /*  Check for correct version  */
  if (header.version != FILE_VERSION)
    {
      warning ("Unknown GIMP version #%d in \"%s\"\n", header.version,
	       filename);
      fclose (fp);
      free_pattern (pattern);
      return NULL;
    }

  /*  Get a new pattern mask  */
  pattern->mask = temp_buf_new (header.width, header.height, header.bytes, 0, 0, NULL);

  /*  Read in the pattern name  */
  if ((bn_size = (header.header_size - sz_PatternHeader)))
    {
      pattern->name = (char *) xmalloc (sizeof (char) * bn_size);
      if ((fread (pattern->name, 1, bn_size, fp)) < bn_size)
	{
	  warning ("Error in GIMP pattern file...aborting.");
	  fclose (fp);
	  free_pattern (pattern);
	  return NULL;
	}
    }
  else
    pattern->name = xstrdup ("Unnamed");

  /*  Read the pattern mask data  */
  /*  Read the image data  */
  if ((fread (temp_buf_data (pattern->mask), 1,
	      header.width * header.height * header.bytes, fp)) <
      header.width * header.height * header.bytes)
    warning ("GIMP pattern file appears to be truncated.");

  /*  Clean up  */
  fclose (fp);

  /*temp_buf_swap (pattern->mask);*/

  return pattern;
}


GPatternP
get_pattern_by_index (index)
     int index;
{
  link_ptr list;
  GPatternP pattern;

  list = pattern_list;

  while (list)
    {
      pattern = (GPatternP) list->data;
      if (pattern->index == index)
	return pattern;
      list = next_item (list);
    }

  return NULL;
}


void
select_pattern (pattern)
     GPatternP pattern;
{
  /*  Set the active pattern  */
  active_pattern = pattern;

  /*  Make sure the active pattern is unswapped... */
  /*temp_buf_unswap (pattern->mask);*/

  /*  Keep up appearances in the pattern dialog  */
  if (pattern_select_dialog)
    pattern_select_select (pattern_select_dialog, pattern->index);
}


void
create_pattern_dialog ()
{
  if (!pattern_select_dialog)
    {
      /*  Create the dialog...  */
      pattern_select_dialog = pattern_select_new ();

      install_colormap (RGB);
    }
  else
    {
      /*  Popup the dialog  */
      if (!GTK_WIDGET_VISIBLE (pattern_select_dialog->shell))
	gtk_widget_show (pattern_select_dialog->shell);

      install_colormap (RGB);
    }
}


static void
free_pattern (pattern)
     GPatternP pattern;
{
  if (pattern->mask)
    temp_buf_free (pattern->mask);
  if (pattern->filename)
    xfree (pattern->filename);
  if (pattern->name)
    xfree (pattern->name);

  xfree (pattern);
}
