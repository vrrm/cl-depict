/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "appenv.h"
#include "actionarea.h"

#define TIGHTNESS 5

GtkWidget *
build_action_area (actions, num_actions)
     ActionAreaItem *actions;
     int num_actions;
{
  GtkWidget *action_area;
  GtkWidget *table;
  GtkWidget *button;
  GtkWidget *label;
  GtkData *data;
  int i;

  action_area = gtk_frame_new (NULL);
  gtk_frame_set_type (action_area, GTK_SHADOW_NONE);
  table = gtk_table_new (1, num_actions, TRUE);
  gtk_container_set_border_width (table, 0);
  gtk_container_add (action_area, table);
  
  for (i = 0; i < num_actions; i++)
    {
      button = gtk_push_button_new ();
      gtk_table_attach (table, button, 
			i, i + 1, 0, 1,
			TRUE, TRUE, TIGHTNESS, 
			FALSE, FALSE, 0);
      label = gtk_label_new (actions[i].label);
      gtk_container_add (button, label);

      if (actions[i].callback)
	{
	  data = gtk_button_get_state (button);
	  gtk_callback_add (data, actions[i].callback, actions[i].user_data);
	}

      gtk_widget_show (label);
      gtk_widget_show (button);
      actions[i].widget = button;
    }

  gtk_widget_show (table);
  return action_area;
}
