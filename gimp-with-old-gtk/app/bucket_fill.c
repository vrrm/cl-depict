/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "autodialog.h"
#include "brush_select.h"
#include "bucket_fill.h"
#include "fuzzy_select.h"
#include "gdisplay.h"
#include "gimage_mask.h"
#include "interface.h"
#include "paint_funcs.h"
#include "palette.h"
#include "patterns.h"
#include "selection.h"
#include "tools.h"
#include "undo.h"

/*  the Bucket Fill structures  */
typedef enum
{
  ColorFill,
  PatternFill
} FillType;

typedef struct _BucketTool BucketTool;

struct _BucketTool
{
  int     target_x;   /*  starting x coord          */
  int     target_y;   /*  starting y coord          */
};

typedef struct _BucketOptions BucketOptions;

struct _BucketOptions
{
  double    opacity;
  double    threshold;
  FillType  fill_type;
  int       paint_mode;

  GtkObserver opacity_observer;
  GtkObserver threshold_observer;
};

/*  local function prototypes  */
static void  bucket_fill_button_press    (Tool *, GdkEventButton *, gpointer);
static void  bucket_fill_button_release  (Tool *, GdkEventButton *, gpointer);
static void  bucket_fill_motion          (Tool *, GdkEventMotion *, gpointer);
static void  bucket_fill_control         (Tool *, int, gpointer);
static void  bucket_fill_region          (PixelRegion *, PixelRegion *,
					  unsigned char *, TempBuf *, int);
static void  bucket_fill_line_color      (unsigned char *, unsigned char *,
					  unsigned char *, int, int, int);
static void  bucket_fill_line_pattern    (unsigned char *, unsigned char *,
					  TempBuf *, int, int, int, int, int);

/*  local variables  */
static BucketOptions *bucket_options = NULL;


static gint
bucket_fill_scale_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataAdjustment *adj_data;
  double *scale_val;

  scale_val = (double *) observer->user_data;
  adj_data = (GtkDataAdjustment *) data;
  *scale_val = adj_data->value;

  return FALSE;
}

static void
bucket_fill_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}

static void
bucket_fill_type_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  bucket_options->fill_type = (FillType) client_data;
}

static void
bucket_fill_mode_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  bucket_options->paint_mode = (int) client_data;
}

static BucketOptions *
create_bucket_options ()
{
  BucketOptions *options;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *option_menu;
  GtkWidget *menu;
  GtkWidget *radio_frame;
  GtkWidget *radio_box;
  GtkWidget *radio_button;
  GtkWidget *opacity_scale;
  GtkData *opacity_scale_data;
  GtkWidget *threshold_scale;
  GtkData *threshold_scale_data;
  GtkData *owner = NULL;
  int i;
  char *button_names[2] =
  {
    "Color Fill",
    "Pattern Fill"
  };

  /*  the new options structure  */
  options = (BucketOptions *) xmalloc (sizeof (BucketOptions));
  options->opacity = 100.0;
  options->threshold = 15.0;
  options->fill_type = ColorFill;
  options->paint_mode = NORMAL;

  /*  the main vbox  */
  vbox = gtk_vbox_new (FALSE, 5);
  
  /*  the main label  */
  label = gtk_label_new ("Bucket Fill Options");
  gtk_box_pack (vbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  /*  the opacity scale  */
  hbox = gtk_hbox_new (FALSE, 4);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  
  label = gtk_label_new ("Fill Opacity");
  gtk_box_pack (hbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  opacity_scale_data = gtk_data_adjustment_new (100.0, 0.0, 100.0, 1.0, 1.0, 1.0);
  opacity_scale = gtk_hscale_new ((GtkDataAdjustment *) opacity_scale_data);
  gtk_box_pack (hbox, opacity_scale, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_scale_set_value_pos (opacity_scale, GTK_POS_TOP);
  options->opacity_observer.update = bucket_fill_scale_update;
  options->opacity_observer.disconnect = bucket_fill_disconnect;
  options->opacity_observer.user_data = &options->opacity;
  gtk_data_attach (opacity_scale_data, &options->opacity_observer);
  gtk_widget_show (opacity_scale);
  gtk_widget_show (hbox);

  /*  the threshold scale  */
  hbox = gtk_hbox_new (FALSE, 4);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  
  label = gtk_label_new ("Fill Threshold");
  gtk_box_pack (hbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  threshold_scale_data = gtk_data_adjustment_new (15.0, 1.0, 255.0, 1.0, 1.0, 1.0);
  threshold_scale = gtk_hscale_new ((GtkDataAdjustment *) threshold_scale_data);
  gtk_box_pack (hbox, threshold_scale, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_scale_set_value_pos (threshold_scale, GTK_POS_TOP);
  options->threshold_observer.update = bucket_fill_scale_update;
  options->threshold_observer.disconnect = bucket_fill_disconnect;
  options->threshold_observer.user_data = &options->threshold;
  gtk_data_attach (threshold_scale_data, &options->threshold_observer);
  gtk_widget_show (threshold_scale);
  gtk_widget_show (hbox);

  /*  the paint mode menu  */
  hbox = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  label = gtk_label_new ("Mode:");
  gtk_box_pack (hbox, label, FALSE, FALSE, 2, GTK_PACK_START);
  menu = create_paint_mode_menu (bucket_fill_mode_callback);
  option_menu = gtk_option_menu_new ();
  gtk_box_pack (hbox, option_menu, FALSE, FALSE, 2, GTK_PACK_START);
  
  gtk_widget_show (label);
  gtk_widget_show (option_menu);
  gtk_widget_show (hbox);

  /*  the radio frame and box  */
  radio_frame = gtk_frame_new (NULL);
  gtk_box_pack (vbox, radio_frame, FALSE, FALSE, 0, GTK_PACK_START);

  radio_box = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (radio_box, 0);
  gtk_container_add (radio_frame, radio_box);

  /*  the radio buttons  */
  for (i = 0; i < 2; i++)
    {
      radio_button = gtk_radio_button_new (owner);
      if (!i)
	owner = gtk_button_get_owner (radio_button);
      gtk_box_pack (radio_box, radio_button, FALSE, FALSE, 0, GTK_PACK_START);
      gtk_callback_add (gtk_button_get_state (radio_button),
			bucket_fill_type_callback,
			(void *) i);
      label = gtk_label_new (button_names[i]);
      gtk_label_set_alignment (label, 0.0, 0.5);
      gtk_container_add (radio_button, label);
      gtk_widget_show (label);
      gtk_widget_show (radio_button);
    }
  gtk_widget_show (radio_box);
  gtk_widget_show (radio_frame);


  /*  Register this selection options widget with the main tools options dialog  */
  tools_register_options (BUCKET_FILL, vbox);

  /*  Post initialization  */
  gtk_option_menu_set_menu (option_menu, menu);

  return options;
}


void
bucket_fill_button_press (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  GDisplay * gdisp;
  BucketTool * bucket_tool;

  gdisp = (GDisplay *) gdisp_ptr;
  bucket_tool = (BucketTool *) tool->private;

  gdisplay_untransform_coords (gdisp, bevent->x, bevent->y,
			       &bucket_tool->target_x,
			       &bucket_tool->target_y, FALSE, 1);

  /*  Make the tool active and set the gdisplay which owns it  */
  gdk_pointer_grab (gdisp->canvas->window, FALSE,
		    GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
		    NULL, NULL, bevent->time);

  /*  Make the tool active and set the gdisplay which owns it  */
  tool->gdisp_ptr = gdisp_ptr;
  tool->state = ACTIVE;
}


void
bucket_fill_button_release (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  GDisplay * gdisp;
  BucketTool * bucket_tool;
  TempBuf * buf;
  PixelRegion bufPR, maskPR;
  GRegion * region = NULL;
  int bytes, has_alpha;
  int x1, y1, x2, y2;
  unsigned char col [MAX_CHANNELS];
  unsigned char *d1, *d2;
  GPatternP pattern;
  TempBuf * pat_buf;
  int new_buf = 0;

  gdisp = (GDisplay *) gdisp_ptr;
  bucket_tool = (BucketTool *) tool->private;

  gdk_pointer_ungrab (bevent->time);
  gdk_flush ();

  /*  if the 3rd button isn't pressed, fill the selected region  */
  if (! (bevent->state & GDK_BUTTON3_MASK))
    {
      if (bevent->state & GDK_SHIFT_MASK)
	gimage_get_background (gdisp->gimage, col);
      else
	gimage_get_foreground (gdisp->gimage, col);

      if (bucket_options->fill_type == PatternFill)
	{
	  pattern = get_active_pattern ();
	  
	  if (!pattern)
	    {
	      message_box ("No available patterns for this operation.", NULL, NULL);
	      return;
	    }

	  /*  If the pattern doesn't match the image in terms of color type,
	   *  transform it.  (ie  pattern is RGB, image is indexed)
	   */
	  if (((pattern->mask->bytes == 3) && !gimage_color (gdisp->gimage)) ||
	      ((pattern->mask->bytes == 1) && !gimage_gray (gdisp->gimage)))
	    {
	      int size;

	      if ((pattern->mask->bytes == 1) && gimage_color (gdisp->gimage))
		pat_buf = temp_buf_new (pattern->mask->width, pattern->mask->height, 3, 0, 0, NULL);
	      else
		pat_buf = temp_buf_new (pattern->mask->width, pattern->mask->height, 1, 0, 0, NULL);
	      
	      d1 = temp_buf_data (pattern->mask);
	      d2 = temp_buf_data (pat_buf);

	      size = pattern->mask->width * pattern->mask->height;
	      while (size--)
		{
		  gimage_transform_color (gdisp->gimage, d1, d2, (pattern->mask->bytes == 3) ? RGB : GRAY);
		  d1 += pattern->mask->bytes;
		  d2 += pat_buf->bytes;
		}

	      new_buf = 1;
	    }
	  else
	    pat_buf = pattern->mask;
	}

      bytes = gimage_bytes (gdisp->gimage);
      has_alpha = gimage_has_alpha (gdisp->gimage);

      /*  If there is no selection mask, the do a seed bucket
       *  fill...To do this, calculate a new contiguous region
       */
      if (! gimage_mask_bounds (gdisp->gimage, &x1, &y1, &x2, &y2))
	{
	  region = gregion_new (gimage_width (gdisp->gimage),
				gimage_height (gdisp->gimage));
	      
	  find_contiguous_region (region, gdisp->gimage, 1,
				  (int) bucket_options->threshold,
				  bucket_tool->target_x,
				  bucket_tool->target_y,
				  NULL);

	  gregion_bounds (region, &x1, &y1, &x2, &y2);
	  maskPR.bytes = 1;
	  maskPR.rowstride = gimage_width (gdisp->gimage) * maskPR.bytes;
	  maskPR.data = mask_buf_data (region->mask) +
	    maskPR.rowstride * y1 + maskPR.bytes * x1;

	  /*  if the gimage doesn't have an alpha channel,
	   *  make sure that the temp buf does.  We need the
	   *  alpha channel to fill with the region calculate above
	   */
	  if (! has_alpha)
	    {
	      bytes ++;
	      has_alpha = 1;
	    }
	  buf = temp_buf_new ((x2 - x1), (y2 - y1),
			      bytes, 0, 0, NULL);
	}
      /*  Otherwise, fill the entire selection or floating selection  */
      else
	{
	  buf = temp_buf_new ((x2 - x1), (y2 - y1),
			      bytes, 0, 0, NULL);
	}

      bufPR.bytes = buf->bytes;
      bufPR.x = x1;
      bufPR.y = y1;
      bufPR.w = buf->width;
      bufPR.h = buf->height;
      bufPR.rowstride = buf->width * bufPR.bytes;
      bufPR.data = temp_buf_data (buf);

      if (region)
	bucket_fill_region (&bufPR, &maskPR, col, pat_buf, has_alpha);
      else
	bucket_fill_region (&bufPR, NULL, col, pat_buf, has_alpha);

      gimage_apply_image (gdisp->gimage, &bufPR, 1, (bucket_options->opacity * 255) / 100,
			  bucket_options->paint_mode);

      /*  update the image  */
      gimage_update (gdisp->gimage, bufPR.x, bufPR.y, bufPR.w, bufPR.h);
      gdisplays_flush ();

      /*  free the temporary buffer  */
      temp_buf_free (buf);
      /*  free the region  */
      if (region)
	gregion_free (region);

      if (new_buf)
	temp_buf_free (pat_buf);
    }

  tool->state = INACTIVE;
}


static void
bucket_fill_motion (tool, mevent, gdisp_ptr)
     Tool *tool;
     GdkEventMotion *mevent;
     gpointer gdisp_ptr;
{
}


static void
bucket_fill_control (tool, action, gdisp_ptr)
     Tool * tool;
     int action;
     gpointer gdisp_ptr;
{
}


static void
bucket_fill_line_color (buf, mask, col, has_alpha, bytes, width)
     unsigned char * buf, * mask;
     unsigned char * col;
     int has_alpha;
     int bytes;
     int width;
{
  int alpha, b;

  alpha = (has_alpha) ? bytes - 1 : bytes;
  while (width--)
    {
      for (b = 0; b < alpha; b++)
	buf[b] = col[b];
      
      if (has_alpha)
	{
	  if (mask)
	    buf[alpha] = *mask++;
	  else
	    buf[alpha] = OPAQUE;
	}
      
      buf += bytes;
    }
}


static void
bucket_fill_line_pattern (buf, mask, pattern, has_alpha, bytes, x, y, width)
     unsigned char * buf, * mask;
     TempBuf * pattern;
     int has_alpha;
     int bytes;
     int x, y;
     int width;
{
  unsigned char *pat, *p;
  int alpha, b;
  int i;

  /*  Get a pointer to the appropriate scanline of the pattern buffer  */
  pat = temp_buf_data (pattern) +
    (y % pattern->height) * pattern->width * pattern->bytes;

  alpha = (has_alpha) ? bytes - 1 : bytes;
  for (i = 0; i < width; i++)
    {
      p = pat + ((i + x) % pattern->width) * pattern->bytes;

      for (b = 0; b < alpha; b++)
	buf[b] = p[b];
      
      if (has_alpha)
	{
	  if (mask)
	    buf[alpha] = *mask++;
	  else
	    buf[alpha] = OPAQUE;
	}
      
      buf += bytes;
    }
}


static void
bucket_fill_region (bufPR, maskPR, col, pattern, has_alpha)
     PixelRegion * bufPR;
     PixelRegion * maskPR;
     unsigned char * col;
     TempBuf * pattern;
     int has_alpha;
{
  unsigned char *s, *m;
  int y;

  s = bufPR->data;
  if (maskPR)
    m = maskPR->data;
  else
    m = NULL;

  for (y = 0; y < bufPR->h; y++)
    {
      switch (bucket_options->fill_type)
	{
	case ColorFill:
	  bucket_fill_line_color (s, m, col, has_alpha, bufPR->bytes, bufPR->w);
	  break;
	case PatternFill:
	  bucket_fill_line_pattern (s, m, pattern, has_alpha, bufPR->bytes,
				    bufPR->x, bufPR->y + y, bufPR->w);
	  break;
	}

      s += bufPR->rowstride;
      
      if (maskPR)
	m += maskPR->rowstride;
    }
}

/*********************************/
/*  Global bucket fill functions */
/*********************************/


Tool *
tools_new_bucket_fill ()
{
  Tool * tool;
  BucketTool * private;

  if (! bucket_options)
    bucket_options = create_bucket_options ();

  tool = (Tool *) xmalloc (sizeof (Tool));
  private = (BucketTool *) xmalloc (sizeof (BucketTool));

  tool->type = BUCKET_FILL;
  tool->state = INACTIVE;
  tool->scroll_lock = 1;  /*  Disallow scrolling  */
  tool->private = private;
  tool->button_press_func = bucket_fill_button_press;
  tool->button_release_func = bucket_fill_button_release;
  tool->motion_func = bucket_fill_motion;
  tool->arrow_keys_func = standard_arrow_keys_func;
  tool->control_func = bucket_fill_control;

  return tool;
}


void
tools_free_bucket_fill (tool)
     Tool * tool;
{
  BucketTool * bucket_tool;

  bucket_tool = (BucketTool *) tool->private;

  xfree (bucket_tool);
}


