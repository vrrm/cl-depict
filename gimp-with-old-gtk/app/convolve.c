/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include "appenv.h"
#include "autodialog.h"
#include "brushes.h"
#include "errors.h"
#include "convolve.h"
#include "gdisplay.h"
#include "paint_funcs.h"
#include "paint_core.h"
#include "selection.h"
#include "tools.h"

#define FIELD_COLS    4

#define MIN_BLUR      64         /*  (8/9 original pixel)   */
#define MAX_BLUR      0.25       /*  (1/33 original pixel)  */
#define MIN_SHARPEN   -512
#define MAX_SHARPEN   -64

typedef enum
{
  Blur,
  Sharpen,
  Custom
} ConvolveType;

typedef struct _ConvolveOptions ConvolveOptions;
struct _ConvolveOptions
{
  ConvolveType  type;
  double        pressure;

  GtkObserver   pressure_observer;
};

/*  forward function declarations  */
static void         convolve_motion      (Tool *);
static void         calculate_matrix     ();
static void         integer_matrix       (float *, int *, int);
static void         copy_matrix          (float *, float *, int);
static int          sum_matrix           (int *, int);

/*  local variables  */
static int          matrix [25];
static int          matrix_size;
static int          matrix_divisor;
static ConvolveOptions *convolve_options = NULL;

static float        custom_matrix [25] =
{
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
  0, 0, 1, 0, 0,
  0, 0, 0, 0, 0,
  0, 0, 0, 0, 0,
};

static float        blur_matrix [25] = 
{
  0, 0, 0, 0, 0,
  0, 1, 1, 1, 0,
  0, 1, MIN_BLUR, 1, 0, 
  0, 1, 1, 1, 0,
  0, 0 ,0, 0, 0,
};

static float        sharpen_matrix [25] = 
{
  0, 0, 0, 0, 0,
  0, 1, 1, 1, 0,
  0, 1, MIN_SHARPEN, 1, 0,
  0, 1, 1, 1, 0,
  0, 0, 0, 0, 0,
};


static gint
convolve_scale_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataAdjustment *adj_data;
  double *scale_val;

  scale_val = (double *) observer->user_data;
  adj_data = (GtkDataAdjustment *) data;
  *scale_val = adj_data->value;

  /*  recalculate the matrix  */
  calculate_matrix ();

  return FALSE;
}

static void
convolve_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}

static void
convolve_type_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  convolve_options->type =(ConvolveType) client_data;
  /*  recalculate the matrix  */
  calculate_matrix ();
}

ConvolveOptions *
create_convolve_options ()
{
  ConvolveOptions *options;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *pressure_scale;
  GtkData *pressure_scale_data;
  GtkWidget *radio_frame;
  GtkWidget *radio_box;
  GtkWidget *radio_button;
  GtkData *owner = NULL;
  int i;
  char *button_names[3] =
  {
    "Blur",
    "Sharpen",
    "Custom"
  };

  /*  the new options structure  */
  options = (ConvolveOptions *) xmalloc (sizeof (ConvolveOptions));
  options->type = Blur;
  options->pressure = 50.0;

  /*  the main vbox  */
  vbox = gtk_vbox_new (FALSE, 5);
  
  /*  the main label  */
  label = gtk_label_new ("Convolver Options");
  gtk_box_pack (vbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  /*  the pressure scale  */
  hbox = gtk_hbox_new (FALSE, 4);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  
  label = gtk_label_new ("Pressure");
  gtk_box_pack (hbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  pressure_scale_data = gtk_data_adjustment_new (50.0, 0.0, 100.0, 1.0, 1.0, 1.0);
  pressure_scale = gtk_hscale_new ((GtkDataAdjustment *) pressure_scale_data);
  gtk_box_pack (hbox, pressure_scale, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_scale_set_value_pos (pressure_scale, GTK_POS_TOP);
  options->pressure_observer.update = convolve_scale_update;
  options->pressure_observer.disconnect = convolve_disconnect;
  options->pressure_observer.user_data = &options->pressure;
  gtk_data_attach (pressure_scale_data, &options->pressure_observer);
  gtk_widget_show (pressure_scale);
  gtk_widget_show (hbox);

  /*  the radio frame and box  */
  radio_frame = gtk_frame_new (NULL);
  gtk_box_pack (vbox, radio_frame, FALSE, FALSE, 0, GTK_PACK_START);

  radio_box = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (radio_box, 0);
  gtk_container_add (radio_frame, radio_box);

  /*  the radio buttons  */
  for (i = 0; i < 2; i++)
    {
      radio_button = gtk_radio_button_new (owner);
      if (!i)
	owner = gtk_button_get_owner (radio_button);
      gtk_box_pack (radio_box, radio_button, FALSE, FALSE, 0, GTK_PACK_START);
      gtk_callback_add (gtk_button_get_state (radio_button),
			convolve_type_callback,
			(void *) i);
      label = gtk_label_new (button_names[i]);
      gtk_label_set_alignment (label, 0.0, 0.5);
      gtk_container_add (radio_button, label);
      gtk_widget_show (label);
      gtk_widget_show (radio_button);
    }
  gtk_widget_show (radio_box);
  gtk_widget_show (radio_frame);

  /*  Register this selection options widget with the main tools options dialog  */
  tools_register_options (CONVOLVE, vbox);

  return options;
}

void *
convolve_paint_func (tool, state)
     Tool * tool;
     int state;
{
  PaintCore * paint_core;

  paint_core = (PaintCore *) tool->private;

  switch (state)
    {
    case INIT_PAINT:
      /*  calculate the matrix  */
      calculate_matrix ();
      break;

    case MOTION_PAINT:
      convolve_motion (tool);
      break;

    case FINISH_PAINT:
      break;
    }

  return NULL;
}

Tool *
tools_new_convolve ()
{
  Tool * tool;
  PaintCore * private;

  if (! convolve_options)
    convolve_options = create_convolve_options ();

  tool = paint_core_new (CONVOLVE);

  private = (PaintCore *) tool->private;
  private->paint_func = convolve_paint_func;

  return tool;
}

void
tools_free_convolve (tool)
     Tool * tool;
{
  paint_core_free (tool);
}

void
convolve_motion (tool)
     Tool * tool;
{
  GDisplay * gdisp;
  PaintCore * paint_core;
  TempBuf * area;
  PixelRegion srcPR, destPR, tempPR;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  /*  If the image type is indexed, don't convolve  */
  if (gimage_type (gdisp->gimage) == INDEXED_GIMAGE)
    return;

  /*  Get a region which can be used to paint to  */
  if (! (area = paint_core_get_paint_area (tool, 0)))
    return;

  /*  configure the pixel regions correctly  */
  srcPR.bytes = gimage_bytes (gdisp->gimage);
  srcPR.w = area->width;
  srcPR.h = area->height;
  srcPR.rowstride = srcPR.bytes * gimage_width (gdisp->gimage);
  srcPR.data = gimage_data (gdisp->gimage) + srcPR.rowstride * area->y +
    srcPR.bytes * area->x;

  destPR.bytes = area->bytes;
  destPR.rowstride = area->width * area->bytes;
  destPR.data = temp_buf_data (area);

  /*  convolve the source image with the convolve mask  */
  if (srcPR.w >= matrix_size && srcPR.h >= matrix_size)
    {
      /*  if the source has no alpha, then add alpha pixels  */
      if (!gimage_has_alpha (gdisp->gimage))
	{
	  tempPR.bytes = srcPR.bytes + 1;
	  tempPR.w = srcPR.w;
	  tempPR.h = srcPR.h;
	  tempPR.rowstride = tempPR.bytes * tempPR.w;
	  /*  allocate temp space  */
	  tempPR.data = xmalloc (tempPR.h * tempPR.rowstride);

	  add_alpha_region (&srcPR, &tempPR);
	  convolve_region (&tempPR, &destPR, matrix, matrix_size,
			   matrix_divisor, NORMAL);

	  /*  Free the allocated temp space  */
	  xfree (tempPR.data);
	}
      else
	convolve_region (&srcPR, &destPR, matrix, matrix_size,
			 matrix_divisor, NORMAL);
    }
  else
    {
      /*  if the source has no alpha, then add alpha pixels, otherwise copy  */
      if (!gimage_has_alpha (gdisp->gimage))
	add_alpha_region (&srcPR, &destPR);
      else
	copy_region (&srcPR, &destPR);
    }

  /*  paste the newly painted canvas to the gimage which is being worked on  */
  paint_core_paste_canvas (tool, OPAQUE, (int) (get_brush_opacity () * 255),
			   NORMAL_MODE, SOFT, INCREMENTAL);
}

static void
calculate_matrix ()
{
  float percent;

  /*  find percent of tool pressure  */
  percent = (float) convolve_options->pressure / 100.0;

  /*  get the appropriate convolution matrix and size and divisor  */
  switch (convolve_options->type)
    {
    case Blur:
      matrix_size = 5;
      blur_matrix [12] = MIN_BLUR + percent * (MAX_BLUR - MIN_BLUR);
      copy_matrix (blur_matrix, custom_matrix, matrix_size);
      break;

    case Sharpen:
      matrix_size = 5;
      sharpen_matrix [12] = MIN_SHARPEN + percent * (MAX_SHARPEN - MIN_SHARPEN);
      copy_matrix (sharpen_matrix, custom_matrix, matrix_size);
      break;
      
    case Custom:
      matrix_size = 5;
      break;
    }

  integer_matrix (custom_matrix, matrix, matrix_size);
  matrix_divisor = sum_matrix (matrix, matrix_size);

  if (!matrix_divisor)
    matrix_divisor = 1;
}

static void
integer_matrix (source, dest, size)
     float * source;
     int * dest;
     int size;
{
  int i;

#define PRECISION  10000

  for (i = 0; i < size*size; i++)
    *dest++ = (int) (*source ++ * PRECISION);
}

static void
copy_matrix (src, dest, size)
     float * src;
     float * dest;
     int size;
{
  int i;

  for (i = 0; i < size*size; i++)
    *dest++ = *src++;
}

static int
sum_matrix (matrix, size)
     int * matrix;
     int size;
{
  int sum = 0;

  size *= size;

  while (size --)
    sum += *matrix++;

  return sum;
}


