/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "draw_core.h"
#include "tools.h"
#include "edit_selection.h"
#include "floating_sel.h"
#include "gimage_mask.h"
#include "gdisplay.h"
#include "undo.h"

#define EDIT_SELECT_SCROLL_LOCK 0
#define ARROW_VELOCITY          25

typedef struct _edit_selection EditSelection;

struct _edit_selection
{
  int                 origx, origy;        /*  original x and y coords            */

  int                 sx, sy;              /*  starting x and y coordinates       */
  int                 ex, ey;              /*  ending x and y coordinates         */

  int                 x1, y1;              /*  bounding box coordinates           */
  int                 x2, y2;              /*  bounding box coordinates           */

  DrawCore *          core;                /*  selection core for drawing bounds  */
  
  ButtonReleaseFunc   old_button_release;  /*  old button press member func       */
  MotionFunc          old_motion;          /*  old motion member function         */
  ToolCtlFunc         old_control;         /*  old control member function        */
  int                 old_scroll_lock;     /*  old value of scroll lock           */
  
};


/*  static EditSelection structure--there is ever only one present  */
static EditSelection edit_select;


void
init_edit_selection (tool, gdisp_ptr, bevent)
     Tool *tool;
     gpointer gdisp_ptr;
     GdkEventButton *bevent;
{
  GDisplay * gdisp;
  int x, y;

  gdisp = (GDisplay *) gdisp_ptr;

  edit_select.sx        =  bevent->x;
  edit_select.sy        =  bevent->y;
  edit_select.ex        =  bevent->x;
  edit_select.ey        =  bevent->y;

  /*  Move the (x, y) point from screen to image space  */
  gdisplay_untransform_coords (gdisp, bevent->x, bevent->y, &x, &y, FALSE, 1);

  edit_select.origx    =  x;
  edit_select.origy    =  y;

  edit_select.old_button_release = tool->button_release_func;
  edit_select.old_motion         = tool->motion_func;
  edit_select.old_control        = tool->control_func;
  edit_select.old_scroll_lock    = tool->scroll_lock;

  /*  reset the function pointers on the selection tool  */
  tool->button_release_func = edit_selection_button_release;
  tool->motion_func = edit_selection_motion;
  tool->control_func = edit_selection_control;
  tool->scroll_lock = EDIT_SELECT_SCROLL_LOCK;

  /*  find the bounding box  */
  gdisplay_mask_bounds (gdisp, &edit_select.x1, &edit_select.y1,
			&edit_select.x2, &edit_select.y2);

  /*  pause the current selection  */
  selection_pause (gdisp->select);

  /*  Create and start the selection core  */
  edit_select.core = draw_core_new (edit_selection_draw);
  draw_core_start (edit_select.core,
		   gdisp->canvas->window,
		   tool);
}


void
edit_selection_button_release (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  int x, y;
  GDisplay * gdisp;

  gdisp = (GDisplay *) gdisp_ptr;

  /*  resume the current selection and ungrab the pointer  */
  selection_resume (gdisp->select);

  gdk_pointer_ungrab (bevent->time);
  gdk_flush ();

  /*  Stop and free the selection core  */
  draw_core_stop (edit_select.core, tool);
  draw_core_free (edit_select.core);
  edit_select.core = NULL;
  tool->state      = INACTIVE;

  tool->button_release_func = edit_select.old_button_release;
  tool->motion_func         = edit_select.old_motion;
  tool->control_func        = edit_select.old_control;
  tool->scroll_lock         = edit_select.old_scroll_lock;

  /*  If the cancel button is down...Do nothing  */
  if (! (bevent->state & GDK_BUTTON3_MASK))
    {
      /*  Move the (x, y) point from screen to image space  */
      gdisplay_untransform_coords (gdisp, bevent->x, bevent->y, &x, &y, FALSE, 1);
      
      /* if there has been movement, move the selection  */
      if (edit_select.origx != x || edit_select.origy != y)
	{
	  /*  translate the selection  */
	  floating_sel_translate (gdisp->gimage, (x - edit_select.origx),
				  (y - edit_select.origy));
	  gdisplays_flush ();
	}
      /*  if no movement has occured, clear the current selection  */
      else
	{
	  gimage_mask_clear (gdisp->gimage);

	  /*  show selection on all views  */
	  gdisplays_selection_visibility (gdisp->gimage->ID, 1);
	  gdisplays_flush ();
	}
    }
}


void
edit_selection_motion (tool, mevent, gdisp_ptr)
     Tool *tool;
     GdkEventMotion *mevent;
     gpointer gdisp_ptr;
{
  GDisplay * gdisp;

  if (tool->state != ACTIVE)
    return;

  gdisp = (GDisplay *) gdisp_ptr;

  if (mevent->x != edit_select.ex || mevent->y != edit_select.ey)
    {
      draw_core_pause (edit_select.core, tool);

      edit_select.ex = mevent->x;
      edit_select.ey = mevent->y;

      draw_core_resume (edit_select.core, tool);
    }
}


void
edit_selection_draw (tool)
     Tool *tool;
{
  int i;
  int diff_x, diff_y;
  GDisplay * gdisp;
  GdkSegment * seg;
  Selection * select;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  select = gdisp->select;

  diff_x = edit_select.ex - edit_select.sx;
  diff_y = edit_select.ey - edit_select.sy;

  /*  Draw only the selection's bounding box if App resource "useBBox" is TRUE  */
  if (0 /*use_bbox*/)
    {
      gdk_draw_rectangle (edit_select.core->win, edit_select.core->gc, 0,
			  edit_select.x1 + diff_x, edit_select.y1 + diff_y,
			  (edit_select.x2 - edit_select.x1),
			  (edit_select.y2 - edit_select.y1));
    }

  /*  Otherwise, draw the entire outline  */
  else
    {
      /*  offset the current selection  */
      seg = select->segs;
      for (i = 0; i < select->num_segs; i++)
	{
	  seg->x1 += diff_x;
	  seg->x2 += diff_x;
	  seg->y1 += diff_y;
	  seg->y2 += diff_y;
	  seg++;
	}

      gdk_draw_segments (edit_select.core->win, edit_select.core->gc,
			 select->segs, select->num_segs);
      
      /*  reset the the current selection  */
      seg = select->segs;
      for (i = 0; i < select->num_segs; i++)
	{
	  seg->x1 -= diff_x;
	  seg->x2 -= diff_x;
	  seg->y1 -= diff_y;
	  seg->y2 -= diff_y;
	  seg++;
	}
    }
}


void
edit_selection_control (tool, action, gdisp_ptr)
     Tool * tool;
     int action;
     gpointer gdisp_ptr;
{
  switch (action)
    {
    case PAUSE : 
      draw_core_pause (edit_select.core, tool);
      break;
    case RESUME :
      draw_core_resume (edit_select.core, tool);
      break;
    case HALT :
      draw_core_stop (edit_select.core, tool);
      draw_core_free (edit_select.core);
      break;
    }
}


void
edit_sel_arrow_keys_func (tool, kevent, gdisp_ptr)
     Tool *tool;
     GdkEventKey *kevent;
     gpointer gdisp_ptr;
{
  int inc_x, inc_y;
  GDisplay * gdisp;

  gdisp = (GDisplay *) gdisp_ptr;

  inc_x = inc_y = 0;

  switch (kevent->keyval)
    {
    case XK_Up    : inc_y = -1; break;
    case XK_Left  : inc_x = -1; break;
    case XK_Right : inc_x =  1; break;
    case XK_Down  : inc_y =  1; break;
    }

  /*  If the shift key is down, move by an accelerated increment  */
  if (kevent->state & GDK_SHIFT_MASK)
    {
      inc_y *= ARROW_VELOCITY;
      inc_x *= ARROW_VELOCITY;
    }

  /*  translate the selection  */
  floating_sel_translate (gdisp->gimage, inc_x, inc_y);
  gdisplays_flush ();
}



