/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <signal.h>
#include <stdlib.h>

#include "appenv.h"
#include "actionarea.h"
#include "app_procs.h"
#include "brushes.h"
#include "gdisplay.h"
#include "colormaps.h"
#include "gdither.h"
#include "gimprc.h"
#include "global_edit.h"
#include "gximage.h"
#include "image_render.h"
#include "interface.h"
#include "paint_funcs.h"
#include "palette.h"
#include "patterns.h"
#include "plug_in.h"
#include "temp_buf.h"
#include "tools.h"
#include "undo.h"
#include "visual.h"

/*  Function prototype for affirmation dialog when exiting application  */
static void app_exit_finish    (void);
static void really_quit_dialog (void);


void
app_pre_init ()
{
  get_standard_visuals ();
  create_standard_colormaps ();
}

void
app_init ()
{
  gdither_init ();
  gximage_init ();
  get_active_brush ();
  get_active_pattern ();
  paint_funcs_setup ();
  render_setup (transparency_type, transparency_size);
  tools_options_dialog_new ();

  /*  Select the initial tool */
  tools_select (RECT_SELECT);
}

static void
app_exit_finish ()
{
  free_standard_colormaps ();
  gdither_free ();
  gximage_free ();
  gdisplays_delete ();
  undo_free ();
  global_edit_free ();
  named_buffers_free ();
  swapping_free ();
  brushes_free ();
  patterns_free ();
  palettes_free ();
  brush_select_dialog_free ();
  pattern_select_dialog_free ();
  palette_free ();
  paint_funcs_free ();
  tools_options_dialog_free ();
  plug_in_kill ();
  free_chunks ();

  gtk_exit (0);
}

void
app_exit (kill_it)
     int kill_it;
{
  /*  If it's the user's perogative, and there are dirty images  */
  if (kill_it == 0 && gdisplays_dirty ())
    really_quit_dialog ();
  else
    app_exit_finish ();
}

/********************************************************
 *   Routines to query exiting the application          *
 ********************************************************/

static void
really_quit_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  GtkWidget *mbox;

  mbox = (GtkWidget *) client_data;
  gtk_widget_destroy (mbox);
  app_exit_finish ();
}


static void
dont_quit_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  GtkWidget *mbox;

  mbox = (GtkWidget *) client_data;
  gtk_widget_destroy (mbox);
}


static void
really_quit_dialog ()
{
  static ActionAreaItem mbox_action_items[2] =
  {
    { "Quit", really_quit_callback, NULL, NULL },
    { "Cancel", dont_quit_callback, NULL, NULL }
  };
  GtkWidget *mbox;
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *action_area;

  mbox = gtk_window_new ("Really Quit?", GTK_WINDOW_DIALOG);
  vbox = gtk_vbox_new (FALSE, 10);
  gtk_container_add (mbox, vbox);
  label = gtk_label_new ("Some files unsaved.  Quit the GIMP?");
  gtk_box_pack (vbox, label, TRUE, FALSE, 0, GTK_PACK_START);

  mbox_action_items[0].user_data = mbox;
  mbox_action_items[1].user_data = mbox;
  action_area = build_action_area (mbox_action_items, 2);
  gtk_box_pack (vbox, action_area, FALSE, FALSE, 0, GTK_PACK_START);

  gtk_widget_show (label);
  gtk_widget_show (action_area);
  gtk_widget_show (vbox);
  map_dialog (mbox);
}
