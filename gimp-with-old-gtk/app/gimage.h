/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GIMAGE_H__
#define __GIMAGE_H__

#include "boundary.h"
#include "channel.h"
#include "layer.h"
#include "linked.h"
#include "gregion.h"
#include "paint_funcs.h"
#include "temp_buf.h"

/* the image types */
#define RGB_GIMAGE       0
#define RGBA_GIMAGE      1
#define GRAY_GIMAGE      2
#define GRAYA_GIMAGE     3
#define INDEXED_GIMAGE   4
#define INDEXEDA_GIMAGE  5

#define GRAY_PIX         0
#define ALPHA_G_PIX      1
#define RED_PIX          0
#define GREEN_PIX        1
#define BLUE_PIX         2
#define ALPHA_PIX        3

#define RGB              0
#define GRAY             1
#define INDEXED          2

#define MAX_CHANNELS     4

/* the image fill types */
#define BACKGROUND_FILL  0
#define WHITE_FILL       1
#define TRANSPARENT_FILL 2

#define COLORMAP_SIZE    768

/* structure declarations */

typedef struct _GImage GImage;

struct _GImage
  {
    char *filename;		        /*  original filename            */
    int has_filename;                   /*  has a valid filename         */

    long width, height;		        /*  width and height attributes  */

    int type;                           /*  RGB color, gray, or indexed  */
    int bpp;                            /*  bytes per pixel              */

    unsigned char * cmap;               /*  colormap--for indexed        */
    int num_cols;                       /*  number of cols--for indexed  */

    unsigned char *shadow_buf;          /*  shadow buffer                */

    int dirty;                          /*  dirty flag -- # of ops       */

    int instance_count;                 /*  number of instances          */
    int ref_count;                      /*  number of references         */

    int shadow_shmid;                   /*  Shm ID of shadow buf         */
    void * shadow_shmaddr;              /*  Shm addr of shadow buf       */

    int ID;                             /*  Unique gimage identifier     */

    link_ptr plug_ins;                  /*  a list of plug-ins to be run */
    short busy;                         /*  is this image in use         */

    /*  Projection attributes  */
    int flat;                           /*  Is the gimage flat?          */
    int construct_flag;                 /*  flag for construction        */

    unsigned char * projection;         /*  The projection of all layers,*/
                                        /*  channels, and floating sels  */
    int proj_bytes;			/*  # bytes/pixel in projection  */
    int proj_type;                      /*  type of the projection image */

    /*  Layer/Channel attributes  */
    link_ptr layers;                    /*  the list of layers           */
    link_ptr channels;                  /*  the list of masks            */

    int active_layer;                   /*  ID of active layer           */
    int active_channel;                 /*  ID of active channel         */

    int visible [MAX_CHANNELS];         /*  visible channels             */
    int active  [MAX_CHANNELS];         /*  active channels              */

    /*  Selection attributes  */
    int floating_sel;                   /*  ID of floating sel layer     */

    int boundary_known;                 /*  is the current boundary valid*/
    BoundSeg  * boundary;               /*  outline of selected region   */
    int num_segments;                   /*  number of lines in boundary  */

    GRegion * region;                   /*  selection mask               */
    int by_color_select;                /*  TRUE if there's an active    */
                                        /*  "by color" selection dialog  */
};


/* function declarations */

GImage *        gimage_new               (long, long, int);
GImage *        gimage_get_named         (char *);
GImage *        gimage_get_ID            (int);
int             gimage_get_shadow_ID     (GImage *);
unsigned char * gimage_get_shadow_addr   (GImage *);
void            gimage_free_shadow       (GImage *);
void            gimage_delete            (GImage *);
void            gimage_add_plug_in       (GImage *, void *);
void            gimage_update            (GImage *, int, int, int, int);
void            gimage_fill              (GImage *, int);
void            gimage_apply_image       (GImage *, PixelRegion *, int, int, int);
void            gimage_get_foreground    (GImage *, unsigned char *);
void            gimage_get_background    (GImage *, unsigned char *);
void            gimage_get_color         (GImage *, unsigned char *,
					  unsigned char *);
void            gimage_transform_color   (GImage *, unsigned char *,
					  unsigned char *, int);
					  

/*  layer/channel functions  */

Layer *         gimage_add_layer         (GImage *, char *, int, int, int);
Layer *         gimage_add_floating_layer(GImage *, Layer *);
Layer *         gimage_remove_layer      (GImage *, int);
Channel *       gimage_add_channel       (GImage *, char *, int, unsigned char *);
Channel *       gimage_remove_channel    (GImage *, int);
void            gimage_construct         (GImage *, int, int, int, int);
void            gimage_inflate           (GImage *);


/*  Access functions  */

int             gimage_type              (GImage *);
int             gimage_has_alpha         (GImage *);
int             gimage_type_with_alpha   (GImage *);
int             gimage_color             (GImage *);
int             gimage_gray              (GImage *);
int             gimage_indexed           (GImage *);
unsigned char * gimage_data              (GImage *);
int             gimage_bytes             (GImage *);
int             gimage_width             (GImage *);
int             gimage_height            (GImage *);
void            gimage_offsets           (GImage *, int *, int *);
int             gimage_shm_ID            (GImage *);
unsigned char * gimage_cmap              (GImage *);


/*  projection access functions  */

unsigned char * gimage_projection        (GImage *);
int             gimage_projection_type   (GImage *);
int             gimage_projection_bytes  (GImage *);


#endif /* __GIMAGE_H__ */

