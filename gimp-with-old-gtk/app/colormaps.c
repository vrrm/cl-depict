/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <math.h>
#include "appenv.h"
#include "errors.h"
#include "gimprc.h"
#include "general.h"
#include "gimprc.h"
#include "visual.h"
#include "colormaps.h"

GdkColormap *rgbcmap;		/* rgb colormap */
int      rgbcmap_alloced;       /* has the colormap been created, or is it default  */
guint32  color_cube[256];       /* pixel values of rgbpal */
GdkColor rgb_palette[256];      /* GdkColors corresponding to RGB palette */
int      num_color_entries;     /* Number of colors in RGB palette */

GdkColormap *graycmap;		/* grayscale colormap */
int      graycmap_alloced;      /* has the colormap been created, or is it default  */
guint32  grays[256];            /* pixel values of graypal */
GdkColor gray_palette[256];     /* GdkColors corresponding to RGB palette */
int      num_gray_entries;      /* Number of colors in RGB palette */

int      num_entries;           /* Total number of entries taken from colormap  */
guint32  alloced_pixels[256];   /* Pixels allocated for the GIMP colormap */

unsigned int shades_r;
unsigned int shades_g;
unsigned int shades_b;
unsigned int shades_gray;

guint32 color_black_pixel;
guint32 color_gray_pixel;
guint32 color_white_pixel;
guint32 gray_black_pixel;
guint32 gray_gray_pixel;
guint32 gray_white_pixel;

guint32 foreground_pixel;
guint32 background_pixel;

guint32 old_color_pixel;
guint32 new_color_pixel;

guint32 marching_ants_pixels[8];

int color_cycled = 0;
int gray_cycled = 0;

static int reserved_entries = 7;  /* extra colors aside from color cube */


static void
set_app_colors (palette, pixels, black_pix, gray_pix, white_pix, cycle)
     GdkColor *palette;
     guint32 *pixels;
     guint32 *black_pix;
     guint32 *gray_pix;
     guint32 *white_pix;
     int cycle;
{
  int i;
  int index = 0;

  /* foreground */
  palette[index].red = 0;
  palette[index].green = 0;
  palette[index].blue = 0;
  palette[index].pixel = pixels[index];
  index++;

  /* background */
  palette[index].red = 255 << 8;
  palette[index].green = 255 << 8;
  palette[index].blue = 255 << 8;
  palette[index].pixel = pixels[index];
  index++;

  /* old color */
  palette[index].red = 0;
  palette[index].green = 0;
  palette[index].blue = 0;
  palette[index].pixel = pixels[index];
  index++;

  /* new color */
  palette[index].red = 255 << 8;
  palette[index].green = 255 << 8;
  palette[index].blue = 255 << 8;
  palette[index].pixel = pixels[index];
  index++;

  /* black pixel */
  palette[index].red = 0;
  palette[index].green = 0;
  palette[index].blue = 0;
  *black_pix = palette[index].pixel = pixels[index];
  index++;

  /* gray pixel */
  palette[index].red = 127 << 8;
  palette[index].green = 127 << 8;
  palette[index].blue = 127 << 8;
  *gray_pix = palette[index].pixel = pixels[index];
  index++;

  /* white pixel */
  palette[index].red = 255 << 8;
  palette[index].green = 255 << 8;
  palette[index].blue = 255 << 8;
  *white_pix = palette[index].pixel = pixels[index];
  index++;

  /* marching ants pixels--if enabled */
  if (cycle)
    for (i = 0; i < 8; i++)
      {
	palette[index].red = (i < 4) ? 0 : (255 << 8);
	palette[index].green = (i < 4) ? 0 : (255 << 8);
	palette[index].blue = (i < 4) ? 0 : (255 << 8);
	marching_ants_pixels[i] = palette[index].pixel = pixels[index];
	index++;
      }
}


static unsigned int
gamma_correct (int intensity, double gamma)
{
  unsigned int val;
  double ind;
  double one_over_gamma;

  if (gamma != 0.0)
    one_over_gamma = 1.0 / gamma;

  ind = (double) intensity / 256.0;
  val = (int) (256 * pow (ind, one_over_gamma));

  return val;
}


static void
create_8_bit_RGB (palette, pixels)
     GdkColor *palette;
     guint32 *pixels;
{
  unsigned int r, g, b;
  unsigned int rv, gv, bv;
  unsigned int dr, dg, db;
  int i = 0;

  dr = (shades_r > 1) ? (shades_r - 1) : (1);
  dg = (shades_g > 1) ? (shades_g - 1) : (1);
  db = (shades_b > 1) ? (shades_b - 1) : (1);

  for (r = 0; r < shades_r; r++)
    for (g = 0; g < shades_g; g++)
      for (b = 0; b < shades_b; b++)
	{
	  palette[i].pixel = pixels[i];
	  color_cube[i] = pixels[i];

	  rv = (unsigned int) ((r * color_visual->colormap_size) / dr);
	  gv = (unsigned int) ((g * color_visual->colormap_size) / dg);
	  bv = (unsigned int) ((b * color_visual->colormap_size) / db);
	  palette[i].red = (gamma_correct (rv, gamma_val)) * 0xff;
	  palette[i].green = (gamma_correct (gv, gamma_val)) * 0xff;
	  palette[i].blue = (gamma_correct (bv, gamma_val)) * 0xff;

	  i++;
	}
}


static void
match_grayscale_colors (pal)
     GdkColor *pal;
{
  int i;
  int last, next;
  int index = 0;
  guint32 closest;

  last = pal[0].red;

  for (i = 0; i < gray_visual->colormap_size; i++)
    {
      next = pal[index + 1].red;

      /*  our current shade of gray is closer to the next shade  */
      if (abs (next - i) < abs (last - i))
	closest = pal[index + 1].pixel;
      else
	closest = pal[index].pixel;

      grays[i] = closest;

      if (i == next)
	{
	  last = next;
	  index ++;
	}
    }
}


static void
create_8_bit_grayscale (palette, pixels)
     GdkColor *palette;
     guint32 *pixels;
{
  int i, d;
  int total;

  total = shades_gray;

  d = (total > 1) ? (total - 1) : (1);

  for (i = 0; i < num_gray_entries; i++)
    {
      palette[i].pixel = pixels[i];

      palette[i].red = palette[i].green = palette[i].blue =
	(i * gray_visual->colormap_size) / d;
    }

  match_grayscale_colors (palette);

  /*  Do gamma correction  */
  for (i = 0; i < num_gray_entries; i++)
    {
      palette[i].red = (gamma_correct (palette[i].red, gamma_val)) * 0xff;
      palette[i].green = palette[i].blue = palette[i].red;
    }
}


static void
update_palette (palette, col)
     GdkColor *palette;
     GdkColor *col;
{
  int i;

  for (i = 0; i < num_entries; i++)
    {
      if (palette[i].pixel == col->pixel)
	{
	  palette[i].red = col->red;
	  palette[i].green = col->green;
	  palette[i].blue = col->blue;
	}
    }
}


static void
trim_color_cube (cmap, red, green, blue)
     GdkColormap *cmap;
     int *red;
     int *green;
     int *blue;
{
  int init_r, init_g, init_b;
  int total;
  int success;

  init_r = *red;
  init_g = *green;
  init_b = *blue;

  total = (*red) * (*green) * (*blue) + reserved_entries;
  while (total > 256)
    {
      if (*blue >= *red && *blue >= *green)
	*blue = *blue - 1;
      else if (*red >= *green && *red >= *blue)
	*red = *red - 1;
      else
	*green = *green - 1;

      total = (*red) * (*green) * (*blue) + reserved_entries;
    }

  success = gdk_colors_alloc (cmap, 0, NULL, 0, alloced_pixels, total);
  while (!success)
    {
      if (*blue >= *red && *blue >= *green)
	*blue = *blue - 1;
      else if (*red >= *green && *red >= *blue)
	*red = *red - 1;
      else
	*green = *green - 1;

      total = (*red) * (*green) * (*blue) + reserved_entries;
      if (*red <= 2 || *green <= 2 || *blue <= 2)
	success = 1;
      else
	success = gdk_colors_alloc (cmap, 0, NULL, 0, alloced_pixels, total);
    }

  /*  If any shades value has been reduced to nothing, exit application  */
  if (*red <= 2 || *green <= 2 || *blue <= 2)
    fatal_error ("Unable to allocate sufficient color entries.\nTry exiting other color intensive applications, or run GIMP with -install.");

  num_color_entries = num_gray_entries = total - reserved_entries;
  num_entries = total;

  /*  If any of the shade values has changed, issue a warning  */
  if (init_r != *red || init_g != *green || init_b != *blue)
    {
      warning ("Not enough colors to satisfy requested color cube.");
      warning ("Reduced color cube shades from");
      warning ("[%d of Red, %d of Green, %d of Blue] ==> [%d of Red, %d of Green, %d of Blue]",
	      init_r, init_g, init_b, *red, *green, *blue);
    }
}


/*************************************************************************/


void
store_color (pixel_ptr, r, g, b)
     guint32 * pixel_ptr;
     int r, g, b;
{
  GdkColor col;

  r = gamma_correct (r, gamma_val);
  g = gamma_correct (g, gamma_val);
  b = gamma_correct (b, gamma_val);

  col.red = r << 8;
  col.green = g << 8;
  col.blue = b << 8;
  col.pixel = *pixel_ptr;

  if (color_visual->depth == 8)
    {
      gdk_color_change (rgbcmap, &col);
      update_palette (rgb_palette, &col);
    }
  else
    gdk_color_alloc (rgbcmap, &col);

  /*
  if (gray_visual->depth == 8)
    {
      gdk_color_change (graycmap, &col);
      update_palette (gray_palette, &col);
    }
  else
    gdk_color_alloc (graycmap, &col);
    */

  *pixel_ptr = col.pixel;
}


void
install_colormap (color_type)
     int color_type;
{
  switch (color_type)
    {
    case RGB: case INDEXED:
      if (color_visual->depth == 8)
	gdk_colors_store (rgbcmap, rgb_palette, num_entries);
      break;
    case GRAY:
      if (gray_visual->depth == 8)
	gdk_colors_store (graycmap, gray_palette, num_entries);
      break;
    default:
      break;
    }
}


GdkColormap *
get_colormap (color_type)
     int color_type;
{
  switch (color_type)
    {
    case RGB: case INDEXED:
      return rgbcmap;
      break;
    case GRAY:
      return graycmap;
      break;
    default :
      return None;
      break;
    }
}


void
create_standard_colormaps ()
{
  GdkColormap *cmap;

  /*  Parse the requested color cube and check for errors...  */
  if ((color_visual->type != GDK_VISUAL_TRUE_COLOR) &&
      (color_visual->type != GDK_VISUAL_DIRECT_COLOR))
    {
      shades_r = color_cube_shades[0];
      shades_g = color_cube_shades[1];
      shades_b = color_cube_shades[2];

      /*  If color-cycled marching ants are enabled, increase
       *  the reserved colors by 8...
       */
      if (cycled_marching_ants)
	reserved_entries += 8;

      if (install_cmap)
	cmap = gdk_colormap_new (color_visual, 0);
      else
	cmap = gdk_colormap_get_system ();

      /*  Attempt to fit the requested colors into the colormap
       *  Trim if necessary
       */
      trim_color_cube (cmap, &shades_r, &shades_g, &shades_b, cycled_marching_ants);
    }
  else
    {
      switch (color_visual->depth)
	{
	case 15:
	case 16:
	  shades_r = shades_g = shades_b = 5;
	  break;
	case 24:
	  shades_r = shades_g = shades_b = 8;
	  break;
	}
    }

  /*  For color visuals, we have to decide on how to create an appropriate
   *  colormap based on depth
   */
  switch (color_visual->depth)
    {
      /*  Pseudo Color  */
    case 8 :
      rgbcmap = cmap;
      rgbcmap_alloced = (install_cmap) ? 1 : 0;
      color_cycled = cycled_marching_ants;

      create_8_bit_RGB (rgb_palette + reserved_entries,
			alloced_pixels + reserved_entries);
      set_app_colors (rgb_palette, alloced_pixels,
		      &color_black_pixel, &color_gray_pixel, &color_white_pixel,
		      color_cycled);

      foreground_pixel = rgb_palette[0].pixel;
      background_pixel = rgb_palette[1].pixel;
      old_color_pixel = rgb_palette[2].pixel;
      new_color_pixel = rgb_palette[3].pixel;

      break;

      /*  True Color  */
    case 15 :
    case 16 :
    case 24 :
      rgbcmap = gdk_colormap_new (color_visual, 0);
      rgbcmap_alloced = 1;
      color_cycled = 0;

      /*  Set foreground/background  */
      foreground_pixel = COLOR_COMPOSE (0, 0, 0);
      background_pixel = COLOR_COMPOSE (255, 255, 255);
      color_black_pixel = foreground_pixel;
      color_gray_pixel = COLOR_COMPOSE (127, 127, 127);
      color_white_pixel = background_pixel;
      break;

    default:
      break;
    }

  /*  Calculate the number of gray tones from the total number of
      Colors requested in the RGB color space                      */
  shades_gray = shades_r * shades_g * shades_b;

  /* Handles the case of emulating grayscale with a truecolor visual...  */
  if (emulate_gray)
    {
      graycmap = gdk_colormap_new (gray_visual, FALSE);
      graycmap_alloced = 1;
      gray_cycled = 0;

      gray_black_pixel = color_black_pixel;
      gray_gray_pixel = color_gray_pixel;
      gray_white_pixel = color_white_pixel;
    }
  /* In the case of an 8 bit grayscale... */
  else
    {
      graycmap = cmap;
      graycmap_alloced = (install_cmap) ? ((rgbcmap_alloced) ? 0 : 1) : 0;
      gray_cycled = cycled_marching_ants;

      create_8_bit_grayscale (gray_palette + reserved_entries,
			      alloced_pixels + reserved_entries);
      set_app_colors (gray_palette, alloced_pixels,
		      &gray_black_pixel, &gray_gray_pixel, &gray_white_pixel,
		      gray_cycled);
    }

  install_colormap (RGB);

  gtk_push_visual (color_visual);
  gtk_push_colormap (rgbcmap);
}


void
free_standard_colormaps ()
{
  if (rgbcmap_alloced)
    gdk_colormap_destroy (rgbcmap);
  if (graycmap_alloced)
    gdk_colormap_destroy (graycmap);
}
