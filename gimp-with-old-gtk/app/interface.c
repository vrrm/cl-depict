/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "actionarea.h"
#include "buildmenu.h"
#include "colormaps.h"
#include "color_area.h"
#include "commands.h"
#include "errors.h"
#include "gdisplay.h"
#include "gimage.h"
#include "gimprc.h"
#include "general.h"
#include "interface.h"
#include "linked.h"
#include "menus.h"
#include "tools.h"
#include "visual.h"

#include "pixmaps.h"


/*  local functions  */
static void tools_select_callback (GtkWidget *, gpointer, gpointer);

typedef struct _ToolButton ToolButton;

struct _ToolButton
{
  char **data;
  GtkCallback callback;
  gpointer user_data;
};

static ToolButton tool_data[] =
{
  { (char **) rect_select_bits, tools_select_callback, (gpointer) RECT_SELECT },
  { (char **) ellipse_select_bits, tools_select_callback, (gpointer) ELLIPSE_SELECT },
  { (char **) free_select_bits, tools_select_callback, (gpointer) FREE_SELECT },
  { (char **) fuzzy_select_bits, tools_select_callback, (gpointer) FUZZY_SELECT },
  { (char **) bezier_select_bits, tools_select_callback, (gpointer) BEZIER_SELECT },
  { (char **) iscissors_bits, tools_select_callback, (gpointer) ISCISSORS },
  { (char **) move_bits, tools_select_callback, (gpointer) MOVE },
  { (char **) magnify_bits, tools_select_callback, (gpointer) MAGNIFY },
  { (char **) crop_bits, tools_select_callback, (gpointer) CROP },
  { (char **) scale_tool_bits, tools_select_callback, (gpointer) ROTATE },
  { (char **) flip_bits, tools_select_callback, (gpointer) FLIP_HORZ },
  { (char **) text_tool_bits, tools_select_callback, (gpointer) TEXT },
  { (char **) color_picker_bits, tools_select_callback, (gpointer) COLOR_PICKER },
  { (char **) bucket_fill_bits, tools_select_callback, (gpointer) BUCKET_FILL },
  { (char **) blend_bits, tools_select_callback, (gpointer) BLEND },
  { (char **) pencil_bits, tools_select_callback, (gpointer) PENCIL },
  { (char **) brush_bits, tools_select_callback, (gpointer) PAINTBRUSH },
  { (char **) eraser_bits, tools_select_callback, (gpointer) ERASER },
  { (char **) airbrush_bits, tools_select_callback, (gpointer) AIRBRUSH },
  { (char **) clone_bits, tools_select_callback, (gpointer) CLONE },
  { (char **) convolve_bits, tools_select_callback, (gpointer) CONVOLVE },
};

static int pixmap_colors[8][3] =
{
  { 0x00, 0x00, 0x00 }, /* a - 0   */
  { 0x24, 0x24, 0x24 }, /* b - 36  */
  { 0x49, 0x49, 0x49 }, /* c - 73  */
  { 0x6D, 0x6D, 0x6D }, /* d - 109 */
  { 0x92, 0x92, 0x92 }, /* e - 146 */
  { 0xB6, 0xB6, 0xB6 }, /* f - 182 */
  { 0xDB, 0xDB, 0xDB }, /* g - 219 */
  { 0xFF, 0xFF, 0xFF }, /* h - 255 */
};

#define NUM_TOOLS 21
#define COLUMNS   3
#define ROWS      7
#define MARGIN    2

/*  Widgets for each tool button--these are used from command.c to activate on
 *  tool selection via both menus and keyboard accelerators.
 */
GtkWidget *tool_widgets[(sizeof (tool_data) / sizeof (ToolButton))];

/*  The popup shell is a pointer to the gdisplay shell that posted the latest
 *  popup menu.  When this is null, and a command is invoked, then the
 *  assumption is that the command was a result of a keyboard accelerator
 */
GtkWidget *popup_shell = NULL;


static void
tools_select_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  tools_select ((ToolType) client_data);
}


static GtkWidget*
create_pixmap_widget (GdkWindow *parent, GdkImage *image, GdkGC *gc, GdkColor *colors,
		      char **data, int width, int height)
{
  GtkWidget *pixmap_widget;
  GdkPixmap *pixmap[3];
  gint j, r, s, t;
  guchar *mem;
  guint32 *lmem;
  guchar value;
  guint32 pixel;

  pixmap[0] = gdk_pixmap_new (parent, width, height, -1);
  pixmap[1] = gdk_pixmap_new (parent, width, height, -1);
  pixmap[2] = gdk_pixmap_new (parent, width, height, -1);

  for (j = 0; j < 3; j++)
    {
      for (r = 0; r < height; r++)
	{
	  mem = image->mem;
	  mem += image->bpl * r;

	  for (s = 0; s < width; s++)
	    {
	      value = data[r][s];

	      if (value == '.')
		pixel = colors[8+j].pixel;
	      else
		pixel = colors[value - 'a'].pixel;

	      if (image->bpp == 4)
		{
		  lmem = (guint32*) mem;
		  *lmem = pixel;
		  mem += 4;
		}
	      else
		{
		  if (image->byte_order == GDK_LSB_FIRST)
		    {
		      for (t = 0; t < image->bpp; t++)
			*mem++ = (unsigned char) ((pixel >> (t * 8)) & 0xFF);
		    }
		  else
		    {
		      for (t = 0; t < image->bpp; t++)
			*mem++ = (unsigned char) ((pixel >> ((image->bpp - t - 1) * 8)) & 0xFF);
		    }
		}
	    }
	}

      gdk_draw_image (pixmap[j], gc, image, 0, 0, 0, 0, width, height);
    }

  pixmap_widget = gtk_pixmap_new (pixmap[0], pixmap[1], pixmap[2]);

  return pixmap_widget;
}


static void
create_logo (GtkWidget *parent)
{
  GtkStyle *style;
  GtkWidget *frame;
  GtkWidget *button;
  GtkWidget *alignment;
  GtkWidget *pixmap;
  GdkColor color[11];
  GdkImage *image;
  GdkGC *gc;
  gint i;

  style = gtk_style_new (1);
  gtk_push_style (style);

  frame = gtk_frame_new (NULL);
  gtk_box_pack (parent, frame, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_realize (frame);

  for (i = 0; i < 8; i++)
    {
      color[i].red = pixmap_colors[i][0] << 8;
      color[i].blue = pixmap_colors[i][1] << 8;
      color[i].green = pixmap_colors[i][2] << 8;

      gdk_color_alloc (frame->window->colormap, &color[i]);
    }

  color[8] = frame->style->background[GTK_STATE_NORMAL];
  gdk_color_alloc (frame->window->colormap, &color[8]);

  color[9] = frame->style->background[GTK_STATE_ACTIVE];
  gdk_color_alloc (frame->window->colormap, &color[9]);

  color[10] = frame->style->background[GTK_STATE_PRELIGHT];
  gdk_color_alloc (frame->window->colormap, &color[10]);

  gc = frame->style->foreground_gc[0];

  image = gdk_image_new (GDK_IMAGE_NORMAL, frame->window->visual,
			 gimp_logo_width, gimp_logo_height);

  button = gtk_push_button_new ();
  gtk_container_add (frame, button);

  alignment = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
  gtk_container_add (button, alignment);
  gtk_container_set_border_width (alignment, 3);

  pixmap = create_pixmap_widget (frame->window, image, gc, color, gimp_logo_bits,
				 gimp_logo_width, gimp_logo_height);
  gtk_container_add (alignment, pixmap);

  gdk_image_destroy (image);
  gtk_widget_show (pixmap);
  gtk_widget_show (alignment);
  gtk_widget_show (button);
  gtk_widget_show (frame);

  gtk_pop_style ();
}


static void
create_color_area (GtkWidget *parent)
{
  GtkWidget *frame;
  GtkWidget *alignment;
  GtkWidget *col_area;

  frame = gtk_frame_new (NULL);
  gtk_frame_set_type (frame, GTK_SHADOW_IN);
  gtk_box_pack (parent, frame, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_realize (frame);

  alignment = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
  gtk_container_add (frame, alignment);
  gtk_container_set_border_width (alignment, 3);

  col_area = color_area_create (gimp_logo_width, 84);
  gtk_container_add (alignment, col_area);

  gtk_widget_show (col_area);
  gtk_widget_show (alignment);
  gtk_widget_show (frame);
}


static void
create_tools (GtkWidget *parent)
{
  GtkWidget *frame;
  GtkWidget *table;
  GtkWidget *button;
  GtkWidget *alignment;
  GtkWidget *pixmap;
  GdkColor color[11];
  GdkImage *image;
  GdkGC *gc;
  GtkData *data;
  GtkData *owner;
  gint i;

  create_logo (parent);

  frame = gtk_frame_new (NULL);
  gtk_box_pack (parent, frame, TRUE, TRUE, 0, GTK_PACK_START);

  table = gtk_table_new (7, 3, TRUE);
  gtk_container_add (frame, table);

  gtk_widget_realize (frame);
  owner = NULL;

  for (i = 0; i < 8; i++)
    {
      color[i].red = pixmap_colors[i][0] << 8;
      color[i].blue = pixmap_colors[i][1] << 8;
      color[i].green = pixmap_colors[i][2] << 8;

      gdk_color_alloc (frame->window->colormap, &color[i]);
    }

  color[8] = frame->style->background[GTK_STATE_NORMAL];
  gdk_color_alloc (frame->window->colormap, &color[8]);

  color[9] = frame->style->background[GTK_STATE_ACTIVE];
  gdk_color_alloc (frame->window->colormap, &color[9]);

  color[10] = frame->style->background[GTK_STATE_PRELIGHT];
  gdk_color_alloc (frame->window->colormap, &color[10]);

  gc = frame->style->foreground_gc[0];
  image = gdk_image_new (GDK_IMAGE_NORMAL, frame->window->visual, 32, 32);

  for (i = 0; i < 21; i++)
    {
      tool_widgets[i] = button = gtk_toggle_button_new (owner);
      gtk_table_attach (table, button,
			(i % 3), (i % 3) + 1,
			(i / 3), (i / 3) + 1,
			TRUE, TRUE, 0,
			TRUE, TRUE, 0);

      if (!owner)
	owner = gtk_button_get_owner (button);

      alignment = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
      gtk_container_add (button, alignment);
      gtk_container_set_border_width (alignment, 2);

      pixmap = create_pixmap_widget (frame->window, image, gc, color, tool_data[i].data, 32, 32);
      gtk_container_add (alignment, pixmap);

      data = gtk_button_get_state (button);
      gtk_callback_add (data, tool_data[i].callback, tool_data[i].user_data);

      gtk_widget_show (pixmap);
      gtk_widget_show (alignment);
      gtk_widget_show (button);
    }

  gtk_gc_release (gc);
  gdk_image_destroy (image);

  gtk_container_set_border_width (frame, 0);
  gtk_container_set_border_width (table, 2);

  gtk_widget_show (table);
  gtk_widget_show (frame);
}


void
create_toolbox ()
{
  GtkWidget *window;
  GtkWidget *main_vbox;
  GtkWidget *vbox;
  GtkWidget *menubar;
  GtkWidget *menu_item;
  GtkAcceleratorTable *table;
  MenuItem *main_menu;

  table = gtk_accelerator_table_new ();
  window = gtk_window_new ("The GIMP", GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_uposition (window, toolbox_x, toolbox_y);
  main_vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (main_vbox, 0);
  gtk_container_add (window, main_vbox);

  /*  Build the menu bar with menus  */
  menubar = gtk_menu_bar_new ();
  gtk_box_pack (main_vbox, menubar, TRUE, TRUE, 0, GTK_PACK_START);
  main_menu = get_main_menu ();
  while (main_menu->label)
    {
      menu_item = gtk_menu_item_new_with_label (main_menu->label);
      gtk_container_add (menubar, menu_item);
      if (main_menu->subitems)
	gtk_menu_item_set_submenu (menu_item, build_menu (main_menu->subitems, table));
      gtk_widget_show (menu_item);

      main_menu ++;
    }
  gtk_widget_show (menubar);

  /*  Install the accelerator table in the main window  */
  gtk_window_add_accelerator_table (window, table);

  vbox = gtk_vbox_new (FALSE, 3);
  gtk_box_pack (main_vbox, vbox, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_container_set_border_width (vbox, 3);

  create_tools (vbox);
  create_color_area (vbox);

  gtk_widget_show (vbox);
  gtk_widget_show (main_vbox);
  gtk_widget_show (window);
}


void
create_display_shell (shell, canvas, hsb, vsb, hrule, vrule,
		      popup, hsbdata, vsbdata, hruledata, vruledata,
		      canvas_events, canvas_event_mask,
		      width, height, scale, title, type)
     GtkWidget **shell;
     GtkWidget **canvas;
     GtkWidget **hsb, **vsb;
     GtkWidget **hrule, **vrule;
     GtkWidget **popup;
     GtkDataAdjustment **hsbdata, **vsbdata;
     GtkDataAdjustment **hruledata, **vruledata;
     GtkEventFunction canvas_events;
     GdkEventMask canvas_event_mask;
     int width, height;
     unsigned int *scale;
     char *title;
     int type;
{
  static GtkWidget *image_popup_menu = NULL;
  static GtkAcceleratorTable *image_accelerator_table = NULL;
  GdkVisual *visual;
  GtkWidget *table;
  GtkWidget *frame;
  GtkWidget *origin_frame;
  int n_width, n_height;
  int s_width, s_height;
  int scalesrc, scaledest;

  /*  adjust the initial scale -- so that window fits on screen */
  {
    s_width = gdk_screen_width ();
    s_height = gdk_screen_height ();

    scalesrc = *scale & 0x00ff;
    scaledest = *scale >> 8;

    n_width = (width * scaledest) / scalesrc;
    n_height = (height * scaledest) / scalesrc;

    /*  Limit to the size of the screen...  */
    while (n_width > s_width || n_height > s_height)
      {
	if (scaledest > 1)
	  scaledest--;
	else
	  if (scalesrc < 0xff)
	    scalesrc++;

	n_width = (width * scaledest) / scalesrc;
	n_height = (height * scaledest) / scalesrc;
      }

    *scale = (scaledest << 8) + scalesrc;
  }

  switch (type)
    {
    case RGB_GIMAGE: case RGBA_GIMAGE: case INDEXED_GIMAGE:
      visual = color_visual;
      break;
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      visual = gray_visual;
      break;
    }

  /*  The adjustment datums  */
  *hsbdata = (GtkDataAdjustment *) gtk_data_adjustment_new (0, 0, width, 1, 1, width);
  *vsbdata = (GtkDataAdjustment *) gtk_data_adjustment_new (0, 0, height, 1, 1, height);
  *hruledata = (GtkDataAdjustment *) gtk_data_adjustment_new (0, 0, width, 1, 1, 1);
  *vruledata = (GtkDataAdjustment *) gtk_data_adjustment_new (0, 0, height, 1, 1, 1);

  /*  The toplevel shell */
  *shell = gtk_window_new (title, GTK_WINDOW_TOPLEVEL);

  /*  the table containing all widgets  */
  table = gtk_table_new (3, 3, FALSE);
  gtk_table_set_col_spacing (table, 0, 2);
  gtk_table_set_col_spacing (table, 1, 2);
  gtk_table_set_row_spacing (table, 0, 2);
  gtk_table_set_row_spacing (table, 1, 2);
  gtk_container_set_border_width (table, 2);
  gtk_container_add (*shell, table);

  /*  scrollbars, rulers, canvas, menu popup button  */
  origin_frame = gtk_frame_new (NULL);
  gtk_frame_set_type (origin_frame, GTK_SHADOW_OUT);
  *hrule = gtk_hruler_new (*hruledata);
  gtk_ruler_set_metric (*hrule, INCHES);
  *vrule = gtk_vruler_new (*vruledata);
  gtk_ruler_set_metric (*vrule, INCHES);
  *hsb = gtk_hscrollbar_new (*hsbdata);
  *vsb = gtk_vscrollbar_new (*vsbdata);
  frame = gtk_frame_new (NULL);
  gtk_frame_set_type (frame, GTK_SHADOW_IN);

  *canvas = gtk_drawing_area_new (n_width, n_height, canvas_events, canvas_event_mask);
  gtk_container_add (frame, *canvas);
  GTK_WIDGET_SET_FLAGS (*canvas, GTK_CAN_FOCUS);

  /*  pack all the widgets  */
  gtk_table_attach (table, origin_frame, 0, 1, 0, 1, TRUE, TRUE, 0, TRUE, TRUE, 0);
  gtk_table_attach (table, *hrule, 1, 2, 0, 1, TRUE, TRUE, 0, TRUE, TRUE, 0);
  gtk_table_attach (table, *vrule, 0, 1, 1, 2, TRUE, TRUE, 0, TRUE, TRUE, 0);
  gtk_table_attach (table, frame, 1, 2, 1, 2, TRUE, TRUE, 0, TRUE, TRUE, 0);
  gtk_table_attach (table, *vsb, 2, 3, 0, 2, FALSE, FALSE, 0, TRUE, TRUE, 0);
  gtk_table_attach (table, *hsb, 0, 2, 2, 3, TRUE, TRUE, 0, FALSE, FALSE, 0);

  if (! image_accelerator_table)
    image_accelerator_table = gtk_accelerator_table_new ();
  if (! image_popup_menu)
    image_popup_menu = build_menu (get_image_menu (), image_accelerator_table);

  /*  the popup menu  */
  *popup = image_popup_menu;

  /*  the accelerator table for images  */
  gtk_window_add_accelerator_table (*shell, image_accelerator_table);

  gtk_widget_show (*hsb);
  gtk_widget_show (*vsb);
  gtk_widget_show (origin_frame);
  gtk_widget_show (*hrule);
  gtk_widget_show (*vrule);
  gtk_widget_show (*canvas);
  gtk_widget_show (frame);
  gtk_widget_show (table);
  gtk_widget_show (*shell);

  /*  set the focus to the canvas area  */
  gtk_widget_grab_focus (*canvas);
}


void
map_dialog (dialog)
     GtkWidget *dialog;
{
  gint root_x, root_y;
  gint s_width, s_height;
  gint d_width, d_height;
  gint start_x, start_y;

  /*  realize the dialog so we can determine width, height, etc  */
  gtk_widget_realize (dialog);
  gdk_window_get_pointer (NULL, &root_x, &root_y, NULL);

  d_width = dialog->window->width;
  d_height = dialog->window->height;

  s_width = gdk_screen_width ();
  s_height = gdk_screen_height ();

  root_x -= (d_width>>1);
  root_y -= (d_height>>1);

  start_x = BOUNDS (root_x, 0, (s_width - d_width));
  start_y = BOUNDS (root_y, 0, (s_height - d_height));

  gdk_window_move (dialog->window, start_x, start_y);

  gtk_widget_show (dialog);
}

/*
 *  A text string query box
 */

typedef struct _QueryBox QueryBox;

struct _QueryBox
{
  GtkWidget *qbox;
  GtkWidget *entry;
  GtkCallback callback;
  gpointer data;
};

static void query_box_cancel_callback (GtkWidget *, gpointer, gpointer);
static void query_box_ok_callback (GtkWidget *, gpointer, gpointer);

GtkWidget *
query_name_box (title, message, callback, data)
     char *title;
     char *message;
     GtkCallback callback;
     gpointer data;
{
  static ActionAreaItem qbox_action_items[2] =
  {
    { "Ok", query_box_ok_callback, NULL, NULL },
    { "Cancel", query_box_cancel_callback, NULL, NULL }
  };
  QueryBox  *query_box;
  GtkWidget *qbox;
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *entry;
  GtkWidget *action_area;

  query_box = (QueryBox *) xmalloc (sizeof (QueryBox));
  qbox = gtk_window_new (title, GTK_WINDOW_DIALOG);
  vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_add (qbox, vbox);
  label = gtk_label_new (message);
  gtk_box_pack (vbox, label, TRUE, FALSE, 0, GTK_PACK_START);
  entry = gtk_text_entry_new ();
  gtk_box_pack (vbox, entry, TRUE, TRUE, 0, GTK_PACK_START);

  qbox_action_items[0].user_data = query_box;
  qbox_action_items[1].user_data = qbox;
  action_area = build_action_area (qbox_action_items, 2);
  gtk_box_pack (vbox, action_area, FALSE, FALSE, 0, GTK_PACK_START);

  query_box->qbox = qbox;
  query_box->entry = entry;
  query_box->callback = callback;
  query_box->data = data;

  gtk_widget_show (label);
  gtk_widget_show (entry);
  gtk_widget_show (action_area);
  gtk_widget_show (vbox);
  map_dialog (qbox);

  return qbox;
}

static void
query_box_cancel_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  QueryBox *query_box;

  query_box = (QueryBox *) client_data;

  /*  Destroy the box  */
  gtk_widget_destroy (query_box->qbox);

  xfree (query_box);
}

static void
query_box_ok_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  QueryBox *query_box;
  char *string;

  query_box = (QueryBox *) client_data;

  /*  Get the entry data  */
  string = xstrdup (gtk_text_entry_get_text (query_box->entry));

  /*  Call the user defined callback  */
  (* query_box->callback) (w, query_box->data, (gpointer) string);

  /*  Destroy the box  */
  gtk_widget_destroy (query_box->qbox);

  xfree (query_box);
}


/*
 *  Message Boxes...
 */

typedef struct _MessageBox MessageBox;

struct _MessageBox
{
  GtkWidget *mbox;
  GtkCallback callback;
  gpointer data;
};

static void message_box_close_callback (GtkWidget *, gpointer, gpointer);

GtkWidget *
message_box (message, callback, data)
     char * message;
     GtkCallback callback;
     gpointer data;
{
  static ActionAreaItem mbox_action_items[1] =
  {
    { "Close", message_box_close_callback, NULL, NULL }
  };
  MessageBox *msg_box;
  GtkWidget *mbox;
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *action_area;

  msg_box = (MessageBox *) xmalloc (sizeof (MessageBox));
  mbox = gtk_window_new ("GIMP Message", GTK_WINDOW_DIALOG);
  vbox = gtk_vbox_new (FALSE, 10);
  gtk_container_add (mbox, vbox);
  label = gtk_label_new (message);
  gtk_box_pack (vbox, label, TRUE, FALSE, 0, GTK_PACK_START);

  mbox_action_items[0].user_data = msg_box;
  action_area = build_action_area (mbox_action_items, 1);
  gtk_box_pack (vbox, action_area, FALSE, FALSE, 0, GTK_PACK_START);

  msg_box->mbox = mbox;
  msg_box->callback = callback;
  msg_box->data = data;

  gtk_widget_show (label);
  gtk_widget_show (action_area);
  gtk_widget_show (vbox);
  map_dialog (mbox);

  return mbox;
}

static void
message_box_close_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  MessageBox *msg_box;

  msg_box = (MessageBox *) client_data;

  /*  If there is a valid callback, invoke it  */
  if (msg_box->callback)
    (* msg_box->callback) (w, msg_box->data, call_data);

  /*  Destroy the box  */
  gtk_widget_destroy (msg_box->mbox);

  xfree (msg_box);
}
