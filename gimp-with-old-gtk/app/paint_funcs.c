/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "appenv.h"
#include "paint_funcs.h"
#include "boundary.h"
#include "memutils.h"

#define STD_BUF_SIZE  4096
#define MAXDIFF       195076

/*  Local function prototypes  */
static int * make_curve         (double, int *);
static void  run_length_encode  (unsigned char *, int *, int, int);
static void  draw_segments      (PixelRegion *, BoundSeg *, int, int, int, int);

static unsigned char * tmp_buffer;  /* temporary buffer available upon request */
static int tmp_buffer_size;
static unsigned char no_mask = OPAQUE;


static unsigned char *
paint_funcs_get_buffer (size)
     int size;
{
  if (size > tmp_buffer_size)
    {
      tmp_buffer_size = size;
      tmp_buffer = (unsigned char *) xrealloc (tmp_buffer, size);
    }

  return tmp_buffer;
}


/*
 * The equations: g(r) = exp (- r^2 / (2 * sigma^2))
 *                   r = sqrt (x^2 + y ^2)
 */

static int *
make_curve (sigma, length)
     double sigma;
     int *length;
{
  int *curve;
  double sigma2;
  double l;
  int temp;
  int i, n;

  sigma2 = 2 * sigma * sigma;
  l = sqrt (-sigma2 * log (1.0 / 255.0));

  n = ceil (l) * 2;
  if ((n % 2) == 0)
    n += 1;

  curve = xmalloc (sizeof (int) * n);

  *length = n / 2;
  curve += *length;
  curve[0] = 255;

  for (i = 1; i <= *length; i++)
    {
      temp = (int) (exp (- (i * i) / sigma2) * 255);
      curve[-i] = temp;
      curve[i] = temp;
    }

  return curve;
}


static void
run_length_encode (src, dest, w, bytes)
     unsigned char * src;
     int * dest;
     int w, bytes;
{
  int start;
  int i;
  int j;
  unsigned char last;

  last = *src;
  src += bytes;
  start = 0;

  for (i = 1; i < w; i++)
    {
      if (*src != last)
	{
	  for (j = start; j < i; j++)
	    {
	      *dest++ = (i - j);
	      *dest++ = last;
	    }
	  start = i;
	  last = *src;
	}
      src += bytes;
    }

  for (j = start; j < i; j++)
    {
      *dest++ = (i - j);
      *dest++ = last;
    }
}


static void
draw_segments (destPR, bs, num_segs, off_x, off_y, opacity)
     PixelRegion * destPR;
     BoundSeg * bs;
     int num_segs;
     int off_x, off_y;
     int opacity;
{
  unsigned char * dest;
  int x1, y1, x2, y2;
  int i, j;

  for (i = 0; i < num_segs; i++)
    {
      x1 = bs[i].x1 + off_x;
      y1 = bs[i].y1 + off_y;
      x2 = bs[i].x2 + off_x;
      y2 = bs[i].y2 + off_y;

      if (bs[i].open == 0)
	{
	  /*  If it is vertical  */
	  if (x1 == x2)
	    {
	      x1 -= 1;
	      x2 -= 1;
	    }
	  else
	    {
	      y1 -= 1;
	      y2 -= 1;
	    }
	}
      
      /*  render segment  */
      x1 = BOUNDS (x1, 0, destPR->w);
      x2 = BOUNDS (x2, 0, destPR->w);
      y1 = BOUNDS (y1, 0, destPR->h);
      y2 = BOUNDS (y2, 0, destPR->h);
      dest = destPR->data + y1 * destPR->rowstride +
	x1 * destPR->bytes + (destPR->bytes - 1);

      if (x1 == x2)
	{
	  for (j = y1; j < y2; j++)
	    {
	      *dest = opacity;
	      dest += destPR->rowstride;
	    }
	}
      else
	{
	  for (j = x1; j < x2; j++)
	    {
	      *dest = opacity;
	      dest += destPR->bytes;
	    }
	}
    }
}


/*********************/
/*  FUNCTIONS        */
/*********************/

void
paint_funcs_setup ()
{
  /*  allocate the temporary buffer  */
  tmp_buffer = (unsigned char *) xmalloc (STD_BUF_SIZE);
  tmp_buffer_size = STD_BUF_SIZE;
}


void
paint_funcs_free ()
{
  /*  free the temporary buffer  */
  xfree (tmp_buffer);
}


void
color_pixels (dest, color, w, bytes)
     unsigned char * dest;
     unsigned char * color;
     int w;
     int bytes;
{
  int b;

  while (w--)
    {
      for (b = 0; b < bytes; b++)
	dest[b] = color[b];

      dest += bytes;
    }
}


void
blend_pixels (src1, src2, dest, blend, w, bytes, has_alpha)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int blend;
     int w;
     int bytes;
     int has_alpha;
{
  int alpha, b;
  unsigned char blend2 = (255 - blend);

  alpha = (has_alpha) ? bytes - 1 : bytes;
  while (w --)
    {
      for (b = 0; b < alpha; b++)
	dest[b] = (src1[b] * blend2 + src2[b] * blend) / 255;

      if (has_alpha)
	dest[alpha] = src1[alpha];  /*  alpha channel--assume src2 has none  */

      src1 += bytes;
      src2 += bytes;
      dest += bytes;
    }
}


void
shade_pixels (src, dest, col, blend, w, bytes, has_alpha)
     unsigned char * src;
     unsigned char * dest;
     unsigned char * col;
     int blend;
     int w;
     int bytes;
     int has_alpha;
{
  int alpha, b;
  unsigned char blend2 = (255 - blend);

  alpha = (has_alpha) ? bytes - 1 : bytes;
  while (w --)
    {
      for (b = 0; b < alpha; b++)
	dest[b] = (src[b] * blend2 + col[b] * blend) / 255;

      if (has_alpha)
	dest[alpha] = src[alpha];  /* alpha channel */
      
      src += bytes;
      dest += bytes;
    }
}


void
extract_alpha_pixels (src, mask, dest, w, bytes)
     unsigned char * src;
     unsigned char * mask;
     unsigned char * dest;
     int w;
     int bytes;
{
  int alpha;
  unsigned char * m;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  alpha = bytes - 1;
  while (w --)
    {
      *dest++ = (src[alpha] * *m) / 255;
      
      if (mask)
	m++;
      src += bytes;
    }
}


void
darken_pixels (src1, src2, dest, length, b1, b2, ha1, ha2)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int b1, b2;
     int ha1, ha2;
{
  int b, alpha;
  unsigned char s1, s2;

  alpha = (ha1 || ha2) ? MAXIMUM (b1, b2) - 1 : b1;

  while (length--)
    {
      for (b = 0; b < alpha; b++)
	{
	  s1 = src1[b];
	  s2 = src2[b];
	  dest[b] = (s1 < s2) ? s1 : s2;
	}

      if (ha2)
	dest[alpha] = src2[alpha];

      src1 += b1;
      src2 += b2;
      dest += b2;
    }
}


void
lighten_pixels (src1, src2, dest, length, b1, b2, ha1, ha2)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int b1, b2;
     int ha1, ha2;
{
  int b, alpha;
  unsigned char s1, s2;

  alpha = (ha1 || ha2) ? MAXIMUM (b1, b2) - 1 : b1;

  while (length--)
    {
      for (b = 0; b < alpha; b++)
	{
	  s1 = src1[b];
	  s2 = src2[b];
	  dest[b] = (s1 < s2) ? s2 : s1;
	}

      if (ha2)
	dest[alpha] = src2[alpha];

      src1 += b1;
      src2 += b2;
      dest += b2;
    }
}


void
hsv_only_pixels (src1, src2, dest, mode, length, bytes1, bytes2, ha1, ha2)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int mode;
     int bytes1, bytes2;
     int ha1, ha2;
{
  int r1, g1, b1;
  int r2, g2, b2;

  /*  assumes inputs are only 4 byte RGBA pixels  */
  while (length--)
    {
      r1 = src1[0]; g1 = src1[1]; b1 = src1[2];
      r2 = src2[0]; g2 = src2[1]; b2 = src2[2];
      rgb_to_hsv (&r1, &g1, &b1);
      rgb_to_hsv (&r2, &g2, &b2);

      switch (mode)
	{
	case HUE_MODE:
	  r1 = r2;
	  break;
	case SATURATION_MODE:
	  g1 = g2;
	  break;
	case VALUE_MODE:
	  b1 = b2;
	  break;
	}

      /*  set the destination  */
      hsv_to_rgb (&r1, &g1, &b1);
      dest[0] = r1; dest[1] = g1; dest[2] = b1;

      if (ha2)
	dest[3] = src2[3];
      
      src1 += bytes1;
      src2 += bytes2;
      dest += bytes2;
    }
}


void
color_only_pixels (src1, src2, dest, mode, length, bytes1, bytes2, ha1, ha2)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int mode;
     int bytes1, bytes2;
     int ha1, ha2;
{
  int r1, g1, b1;
  int r2, g2, b2;

  /*  assumes inputs are only 4 byte RGBA pixels  */
  while (length--)
    {
      r1 = src1[0]; g1 = src1[1]; b1 = src1[2];
      r2 = src2[0]; g2 = src2[1]; b2 = src2[2];
      rgb_to_hls (&r1, &g1, &b1);
      rgb_to_hls (&r2, &g2, &b2);

      /*  transfer hue and saturation to the source pixel  */
      r1 = r2;
      b1 = b2;

      /*  set the destination  */
      hls_to_rgb (&r1, &g1, &b1);
      dest[0] = r1; dest[1] = g1; dest[2] = b1;

      if (ha2)
	dest[3] = src2[3];
      
      src1 += bytes1;
      src2 += bytes2;
      dest += bytes2;
    }
}


void
multiply_pixels (src1, src2, dest, length, b1, b2, ha1, ha2)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int b1, b2;
     int ha1, ha2;
{
  int alpha, b;

  alpha = (ha1 || ha2) ? MAXIMUM (b1, b2) - 1 : b1;

  while (length --)
    {
      for (b = 0; b < alpha; b++)
	dest[b] = (src1[b] * src2[b]) / 255;

      if (ha2)
	dest[alpha] = src2[alpha];

      src1 += b1;
      src2 += b2;
      dest += b2;
    }
}


void
screen_pixels (src1, src2, dest, length, b1, b2, ha1, ha2)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int b1, b2;
     int ha1, ha2;
{
  int alpha, b;

  alpha = (ha1 || ha2) ? MAXIMUM (b1, b2) - 1 : b1;

  while (length --)
    {
      for (b = 0; b < alpha; b++)
	dest[b] = 255 - ((255 - src1[b]) * (255 - src2[b])) / 255;

      if (ha2)
	dest[alpha] = src2[alpha];

      src1 += b1;
      src2 += b2;
      dest += b2;
    }
}


void
add_pixels (src1, src2, dest, length, b1, b2, ha1, ha2)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int b1, b2;
     int ha1, ha2;
{
  int alpha, b;
  int sum;

  alpha = (ha1 || ha2) ? MAXIMUM (b1, b2) - 1 : b1;

  while (length --)
    {
      for (b = 0; b < alpha; b++)
	{
	  sum = src1[b] + src2[b];
	  dest[b] = (sum > 255) ? 255 : sum;
	}

      if (ha2)
	dest[alpha] = src2[alpha];

      src1 += b1;
      src2 += b2;
      dest += b2;
    }
}


void
subtract_pixels (src1, src2, dest, length, b1, b2, ha1, ha2)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int b1, b2;
     int ha1, ha2;
{
  int alpha, b;
  int diff;

  alpha = (ha1 || ha2) ? MAXIMUM (b1, b2) - 1 : b1;

  while (length --)
    {
      for (b = 0; b < alpha; b++)
	{
	  diff = src1[b] - src2[b];
	  dest[b] = (diff < 0) ? 0 : diff;
	}

      if (ha2)
	dest[alpha] = src2[alpha];

      src1 += b1;
      src2 += b2;
      dest += b2;
    }
}


void
difference_pixels (src1, src2, dest, length, b1, b2, ha1, ha2)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int b1, b2;
     int ha1, ha2;
{
  int alpha, b;
  int diff;

  alpha = (ha1 || ha2) ? MAXIMUM (b1, b2) - 1 : b1;

  while (length --)
    {
      for (b = 0; b < alpha; b++)
	{
	  diff = src1[b] - src2[b];
	  dest[b] = (diff < 0) ? -diff : diff;
	}

      if (ha2)
	dest[alpha] = src2[alpha];

      src1 += b1;
      src2 += b2;
      dest += b2;
    }
}


void
dissolve_pixels (src, dest, opacity, length, sb, db, has_alpha)
     unsigned char * src;
     unsigned char * dest;
     int opacity;
     int length;
     int sb, db;
     int has_alpha;
{
  int alpha, b;
  int rand_val;

  alpha = db - 1;

  while (length --)
    {
      /*  preserve the intensity values  */
      for (b = 0; b < alpha; b++)
	dest[b] = src[b];

      /*  dissolve if random value is > opacity  */
      rand_val = (rand() & 0xFF);

      if (has_alpha) 
	dest[alpha] = (rand_val > opacity) ? 0 : src[alpha];
      else
	dest[alpha] = (rand_val > opacity) ? 0 : 255;

      dest += db;
      src += sb;
    }
}


void
swap_pixels (src, dest, swap, length)
     unsigned char * src;
     unsigned char * dest;
     unsigned char * swap;
     int length;
{
  memcpy (swap, src, length);
  memcpy (src, dest, length);
  memcpy (dest, swap, length);
}


void
scale_pixels (src, dest, length, scale)
     unsigned char * src;
     unsigned char * dest;
     int length;
     int scale;
{
  while (length --)
    *dest++ = (unsigned char) ((*src++ * scale) / 255);
}


void
add_alpha_pixels (src, dest, length, bytes)
     unsigned char * src, * dest;
     int length;
     int bytes;
{
  int alpha, b;
  
  alpha = bytes + 1;
  while (length --)
    {
      for (b = 0; b < bytes; b++)
	dest[b] = src[b];

      dest[b] = OPAQUE;

      src += bytes;
      dest += alpha;
    }
}


void
gray_to_rgb_pixels (src, dest, length, bytes)
     unsigned char * src, * dest;
     int length;
     int bytes;
{
  int b;
  int dest_bytes;
  int has_alpha;

  has_alpha = (bytes == 2) ? 1 : 0;
  dest_bytes = (has_alpha) ? 4 : 3;

  while (length --)
    {
      for (b = 0; b < bytes; b++)
	dest[b] = src[0];

      if (has_alpha)
	dest[3] = src[1];

      src += bytes;
      dest += dest_bytes;
    }
}


void
apply_mask_to_alpha_channel (src, mask, opacity, length, bytes)
     unsigned char * src;
     unsigned char * mask;
     int opacity;
     int length;
     int bytes;
{
  int alpha;

  alpha = bytes - 1;
  while (length --)
    {
      src[alpha] = (src[alpha] * *mask++ * opacity) / 65025;
      src += bytes;
    }
}


void
combine_mask_and_alpha_channel (src, mask, opacity, length, bytes)
     unsigned char * src;
     unsigned char * mask;
     int opacity;
     int length;
     int bytes;
{
  unsigned char mask_val;
  int alpha;

  alpha = bytes - 1;
  while (length --)
    {
      mask_val = (*mask++ * opacity) / 255;
      src[alpha] = src[alpha] + ((255 - src[alpha]) * mask_val) / 255;
      src += bytes;
    }
}


void
initial_channel_pixels (src, dest, length, bytes)
     unsigned char * src, * dest;
     int length;
     int bytes;  /* this represents the dest, src_bytes == 1 */
{
  int alpha, b;

  alpha = bytes - 1;
  while (length --)
    {
      for (b = 0; b < alpha; b++)
	dest[b] = src[0];

      dest[alpha] = OPAQUE;

      dest += bytes;
      src ++;
    }
}


void  
initial_indexed_pixels (src, dest, cmap, affect, length)
     unsigned char * src, * dest;
     unsigned char * cmap;
     int * affect;
     int length;
{
  int col_index;

  /*  This function assumes always that we're mapping from
   *  an RGB colormap to an RGBA image...
   */
  while (length--)
    {
      col_index = *src++ * 3;
      *dest++ = cmap[col_index++];
      *dest++ = cmap[col_index++];
      *dest++ = cmap[col_index++];
      *dest++ = (*affect) ? OPAQUE : TRANSPARENT;
    }
}


void
initial_inten_pixels (src, dest, mask, opacity, affect, length, bytes)
     unsigned char * src, * dest;
     unsigned char * mask;
     int opacity;
     int * affect;
     int length;
     int bytes;
{
  int b, dest_bytes;
  unsigned char * m;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  /*  This function assumes the source has no alpha channel and
   *  the destination has an alpha channel.  So dest_bytes = bytes + 1
   */
  dest_bytes = bytes + 1;

  while (length --)
    {
      for (b = 0; b < bytes; b++)
	dest[b] = src[b] * affect[b];

      /*  Set the alpha channel  */
      dest[b] = (opacity * *m * affect[b]) / 255;

      if (mask)
	m++;

      dest += dest_bytes;
      src += bytes;
    }
}


void
initial_inten_a_pixels (src, dest, mask, opacity, affect, length, bytes)
     unsigned char * src, * dest;
     unsigned char * mask;
     int opacity;
     int * affect;
     int length;
     int bytes;
{
  int alpha, b;
  unsigned char * m;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  alpha = bytes - 1;
  while (length --)
    {
      for (b = 0; b < alpha; b++)
	dest[b] = src[b] * affect[b];

      /*  Set the alpha channel  */
      dest[alpha] = (opacity * src[alpha] * *m * affect[alpha]) / 65025;

      if (mask)
	m++;

      dest += bytes;
      src += bytes;
    }
}


void
combine_indexed_and_indexed_pixels (src1, src2, dest, mask,
				    opacity, length, bytes)
     unsigned char * src1, * src2;
     unsigned char * dest, * mask;
     int opacity;
     int length;
     int bytes;
{
  int b;
  unsigned char new_alpha;
  unsigned char * m;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  while (length --)
    {
      new_alpha = (*m * opacity) / 255;

      for (b = 0; b < bytes; b++)
	dest[b] = (new_alpha > 127) ? src2[b] : src1[b];
      
      if (mask)
	m++;

      src1 += bytes;
      src2 += bytes;
      dest += bytes;
    }
}


void
combine_indexed_and_indexed_a_pixels (src1, src2, dest, mask,
				      opacity, length, bytes)
     unsigned char * src1, * src2;
     unsigned char * dest, * mask;
     int opacity;
     int length;
     int bytes;
{
  int b, alpha;
  unsigned char new_alpha;
  unsigned char * m;
  int src2_bytes;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  alpha = 1;
  src2_bytes = 2;

  while (length --)
    {
      new_alpha = (src2[alpha] * *m * opacity) / 65025;

      for (b = 0; b < bytes; b++)
	dest[b] = (new_alpha > 127) ? src2[b] : src1[b];
      
      if (mask)
	m++;

      src1 += bytes;
      src2 += src2_bytes;
      dest += bytes;
    }
}


void
combine_indexed_a_and_indexed_a_pixels (src1, src2, dest, mask,
					opacity, length, bytes)
     unsigned char * src1, * src2;
     unsigned char * dest, * mask;
     int opacity;
     int length;
     int bytes;  /*  == 2  */
{
  int b, alpha;
  unsigned char new_alpha;
  unsigned char * m;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  alpha = 1;

  while (length --)
    {
      new_alpha = (src2[alpha] * *m * opacity) / 65025;

      for (b = 0; b < alpha; b++)
	dest[b] = (new_alpha > src1[alpha]) ? src2[b] : src1[b];

      dest[alpha] = (new_alpha > src1[alpha]) ? new_alpha : src1[alpha];

      if (mask)
	m++;

      src1 += bytes;
      src2 += bytes;
      dest += bytes;
    }
}


void
combine_inten_a_and_indexed_a_pixels (src1, src2, dest, mask, cmap,
				      opacity, length, bytes)
     unsigned char * src1, * src2;
     unsigned char * dest, * mask;
     unsigned char * cmap;
     int opacity;
     int length;
     int bytes;  /* 4 for RGBA */
{
  int b, alpha;
  unsigned char new_alpha;
  unsigned char * m;
  int src2_bytes;
  int index;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  alpha = 1;
  src2_bytes = 2;

  while (length --)
    {
      new_alpha = (src2[alpha] * *m * opacity) / 65025;

      index = src2[0] * 3;

      for (b = 0; b < bytes-1; b++)
	dest[b] = (new_alpha > 127) ? cmap[index + b] : src1[b];

      dest[b] = OPAQUE;  /*  alpha channel is opaque  */

      if (mask)
	m++;

      src1 += bytes;
      src2 += src2_bytes;
      dest += bytes;
    }
}


void
combine_inten_and_inten_pixels (src1, src2, dest, mask,
				opacity, affect, length, bytes)
     unsigned char * src1, * src2;
     unsigned char * dest, * mask;
     int opacity;
     int * affect;
     int length;
     int bytes;
{
  int b;
  unsigned char new_alpha;
  unsigned char * m;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  while (length --)
    {
      new_alpha = (*m * opacity) / 255;

      for (b = 0; b < bytes; b++)
	dest[b] = (affect[b]) ? 
	  (src2[b] * new_alpha + src1[b] * (255 - new_alpha)) / 255 : 
	src1[b];
      
      if (mask)
	m++;

      src1 += bytes;
      src2 += bytes;
      dest += bytes;
    }
}


void
combine_inten_and_inten_a_pixels (src1, src2, dest, mask,
				  opacity, affect, length, bytes)
     unsigned char * src1, * src2;
     unsigned char * dest, * mask;
     int opacity;
     int * affect;
     int length;
     int bytes;  /*  3 or 1 depending on RGB or GRAY  */
{
  int alpha, b;
  int src2_bytes;
  unsigned char new_alpha;
  unsigned char * m;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  alpha = bytes;
  src2_bytes = bytes + 1;

  while (length --)
    {
      new_alpha = (src2[alpha] * *m * opacity) / 65025;

      for (b = 0; b < bytes; b++)
	dest[b] = (affect[b]) ? 
	  (src2[b] * new_alpha + src1[b] * (255 - new_alpha)) / 255 :
	src1[b];
      
      if (mask)
	m++;

      src1 += bytes;
      src2 += src2_bytes;
      dest += bytes;
    }
}


void
combine_inten_a_and_inten_pixels (src1, src2, dest, mask, opacity,
				  affect, length, bytes)
     unsigned char * src1, * src2;
     unsigned char * dest;
     unsigned char * mask;
     int opacity;
     int * affect;
     int length;
     int bytes;  /*  4 or 2 depending on RGBA or GRAYA  */
{
  int alpha, b;
  int src2_bytes;
  unsigned char src2_alpha;
  unsigned char new_alpha;
  unsigned char * m;
  float ratio, compl_ratio;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  src2_bytes = bytes - 1;
  alpha = bytes - 1;

  while (length --)
    {
      src2_alpha = (*m * opacity) / 255;
      new_alpha = src1[alpha] + ((255 - src1[alpha]) * src2_alpha) / 255;
      if (new_alpha)
	ratio = (float) src2_alpha / new_alpha;
      else
	ratio = 0.0;
      compl_ratio = 1.0 - ratio;

      for (b = 0; b < alpha; b++)
	dest[b] = (affect[b]) ?
	  (unsigned char) (src2[b] * ratio + src1[b] * compl_ratio) :
	src1[b];

      dest[alpha] = (affect[alpha]) ? new_alpha : src1[alpha];

      if (mask)
	m++;

      src1 += bytes;
      src2 += src2_bytes;
      dest += bytes;
    }
}


void
combine_inten_a_and_inten_a_pixels (src1, src2, dest, mask, opacity,
				    affect, length, bytes)
     unsigned char * src1, * src2;
     unsigned char * dest;
     unsigned char * mask;
     int opacity;
     int * affect;
     int length;
     int bytes;  /*  4 or 2 depending on RGBA or GRAYA  */
{
  int alpha, b;
  unsigned char src2_alpha;
  unsigned char new_alpha;
  unsigned char * m;
  float ratio, compl_ratio;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  alpha = bytes - 1;
  while (length --)
    {
      src2_alpha = (src2[alpha] * *m * opacity) / 65025;
      new_alpha = src1[alpha] + ((255 - src1[alpha]) * src2_alpha) / 255;
      if (new_alpha)
	ratio = (float) src2_alpha / new_alpha;
      else
	ratio = 0.0;
      compl_ratio = 1.0 - ratio;

      for (b = 0; b < alpha; b++)
	dest[b] = (affect[b]) ? 
	  (unsigned char) (src2[b] * ratio + src1[b] * compl_ratio) :
	src1[b];

      dest[alpha] = (affect[alpha]) ? new_alpha : src1[alpha];

      if (mask)
	m++;

      src1 += bytes;
      src2 += bytes;
      dest += bytes;
    }
}


void
combine_inten_a_and_channel_pixels (src, dest, channel, col, opacity, length, bytes)
     unsigned char * src;
     unsigned char * dest;
     unsigned char * channel;
     unsigned char * col;
     int opacity;
     int length;
     int bytes;
{
  int alpha, b;
  unsigned char channel_alpha;
  unsigned char new_alpha;
  float ratio, compl_ratio;

  alpha = bytes - 1;
  while (length --)
    {
      channel_alpha = (opacity * *channel++) / 255;
      new_alpha = src[alpha] + ((255 - src[alpha]) * channel_alpha) / 255;
      if (new_alpha)
	ratio = (float) channel_alpha / new_alpha;
      else
	ratio = 0.0;
      compl_ratio = 1.0 - ratio;

      for (b = 0; b < alpha; b++)
	dest[b] = (unsigned char) (col[b] * ratio + src[b] * compl_ratio);
      dest[b] = new_alpha;

      /*  advance pointers  */
      src+=bytes;
      dest+=bytes;
    }
}


void
replace_inten_pixels (src1, src2, dest, mask, opacity,
		      affect, length, b1, b2, ha1, ha2)
     unsigned char * src1, * src2;
     unsigned char * dest;
     unsigned char * mask;
     int opacity;
     int * affect;
     int length;
     int b1, b2;
     int ha1, ha2;
{
  int bytes, b;
  unsigned char mask_alpha;
  unsigned char * m;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  bytes = MINIMUM (b1, b2);
  while (length --)
    {
      mask_alpha = (*m * opacity) / 255;

      for (b = 0; b < bytes; b++)
	dest[b] = (affect[b]) ? 
	  (src2[b] * mask_alpha + src1[b] * (255 - mask_alpha)) / 255 :
	src1[b];

      if (ha1 && !ha2)
	dest[b] = src1[b];

      if (mask)
	m++;

      src1 += b1;
      src2 += b2;
      dest += b1;
    }
}


void
erase_inten_pixels (src1, src2, dest, mask, opacity,
		    affect, length, bytes)
     unsigned char * src1, * src2;
     unsigned char * dest;
     unsigned char * mask;
     int opacity;
     int * affect;
     int length;
     int bytes;
{
  int alpha, b;
  unsigned char src2_alpha;
  unsigned char * m;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  alpha = bytes - 1;
  while (length --)
    {
      for (b = 0; b < alpha; b++)
	dest[b] = src1[b];
      
      src2_alpha = (src2[alpha] * *m * opacity) / 65025;
      dest[alpha] = src1[alpha] - (src1[alpha] * src2_alpha) / 255;

      if (mask)
	m++;

      src1 += bytes;
      src2 += bytes;
      dest += bytes;
    }
}


void  
extract_from_inten_pixels (src, dest, mask, bg, cut, length, bytes, has_alpha)
     unsigned char * src, * dest, * mask;
     unsigned char * bg;
     int cut;
     int length;
     int bytes;
     int has_alpha;
{
  int b, alpha;
  int dest_bytes;
  unsigned char * m;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  alpha = (has_alpha) ? bytes - 1 : bytes;
  dest_bytes = (has_alpha) ? bytes : bytes + 1;
  while (length --)
    {
      for (b = 0; b < alpha; b++)
	dest[b] = src[b];
      
      if (has_alpha)
	{
	  dest[alpha] = (*m * src[alpha]) / 255;
	  if (cut)
	    src[alpha] = ((255 - *m) * src[alpha]) / 255;
	}
      else
	{
	  dest[alpha] = *m;
	  if (cut)
	    for (b = 0; b < bytes; b++)
	      src[b] = (*m * bg[b] + (255 - *m) * src[b]) / 255;
	}

      if (mask)
	m++;

      src += bytes;
      dest += dest_bytes;
    }
}


void
extract_from_indexed_pixels (src, dest, mask, cmap, bg, cut, length, bytes, has_alpha)
     unsigned char * src, * dest, * mask;
     unsigned char * cmap, * bg;
     int cut;
     int length;
     int bytes;
     int has_alpha;  /*  4 for RGBA  */
{
  int b;
  int index;
  unsigned char * m;

  if (mask)
    m = mask;
  else
    m = &no_mask;

  while (length --)
    {
      index = src[0] * 3;
      for (b = 0; b < 3; b++)
	dest[b] = cmap[index + b];
      
      if (has_alpha)
	{
	  dest[3] = (*m * src[1]) / 255;
	  if (cut)
	    src[1] = ((255 - *m) * src[1]) / 255;
	}
      else
	{
	  dest[3] = *m;
	  if (cut)
	    src[0] = (*m > 127) ? bg[0] : src[0];
	}

      if (mask)
	m++;

      src += bytes;
      dest += 4;
    }
}


int
map_rgb_to_indexed (cmap, num_cols, r, g, b)
     unsigned char * cmap;
     int num_cols;
     int r, g, b;
{
  unsigned char *col;
  int diff, sum, max;
  int i, index;

  max = MAXDIFF;
  index = 0;

  col = cmap;
  for (i = 0; i < num_cols; i++)
    {
      diff = r - *col++;
      sum = diff * diff;
      diff = g - *col++;
      sum += diff * diff;
      diff = b - *col++;
      sum += diff * diff;
      
      if (sum < max)
	{
	  index = i;
	  max = sum;
	}
    }

  return index;
}


     
/**************************************************/
/*    REGION FUNCTIONS                            */
/**************************************************/


void
color_region (src, col)
     PixelRegion * src;
     unsigned char * col;
{
  int h;
  unsigned char * s;

  h = src->h;
  s = src->data;

  while (h--)
    {
      color_pixels (s, col, src->w, src->bytes);
      s += src->rowstride;
    }
}


void  
blend_region (src1, src2, dest, blend)
     PixelRegion * src1, * src2;
     PixelRegion * dest;
     int blend;
{
  int h;
  unsigned char * s1, * s2, * d;

  s1 = src1->data;
  s2 = src2->data;
  d = dest->data;
  h = src1->h;

  while (h --)
    {
/*      blend_pixels (s1, s2, d, blend, src1->w, src1->bytes);*/
      s1 += src1->rowstride;
      s2 += src2->rowstride;
      d += dest->rowstride;
    }
}


void  
shade_region (src, dest, col, blend)
     PixelRegion * src, * dest;
     unsigned char * col;
     int blend;
{
  int h;
  unsigned char * s, * d;

  s = src->data;
  d = dest->data;
  h = src->h;

  while (h --)
    {
/*      blend_pixels (s, d, col, blend, src->w, src->bytes);*/
      s += src->rowstride;
      d += dest->rowstride;
    }
}


void
copy_region (src, dest)
     PixelRegion * src, * dest;
{
  int h;
  int pixelwidth;
  unsigned char * s, * d;

  pixelwidth = src->w * src->bytes;
  s = src->data;
  d = dest->data;
  h = src->h;

  while (h --)
    {
      memcpy (d, s, pixelwidth);
      s += src->rowstride;
      d += dest->rowstride;
    }
}


void
add_alpha_region (src, dest)
     PixelRegion * src, * dest;
{
  int h;
  unsigned char * s, * d;

  s = src->data;
  d = dest->data;
  h = src->h;

  while (h --)
    {
      add_alpha_pixels (s, d, src->w, src->bytes);
      s += src->rowstride;
      d += dest->rowstride;
    }
}


void
extract_alpha_region (src, mask, dest)
     PixelRegion * src, * mask, * dest;
{
  int h;
  unsigned char * s, * m, * d;

  s = src->data;
  d = dest->data;
  if (mask)
    m = mask->data;
  else
    m = NULL;

  h = src->h;
  while (h --)
    {
      extract_alpha_pixels (s, m, d, src->w, src->bytes);
      s += src->rowstride;
      d += dest->rowstride;
      if (mask)
	m += mask->rowstride;
    }
}


void
extract_from_region (src, dest, mask, cmap, bg, type, has_alpha, cut)
     PixelRegion * src, * dest, * mask;
     unsigned char * cmap;
     unsigned char * bg;
     int type;
     int has_alpha;
     int cut;
{
  int h;
  unsigned char * s, * d, * m;

  s = src->data;
  d = dest->data;
  m = (mask) ? mask->data : NULL;
  h = src->h;

  while (h --)
    {
      switch (type)
	{
	case 0:  /*  RGB      */
	case 1:  /*  GRAY     */
	  extract_from_inten_pixels (s, d, m, bg, cut, src->w,
				     src->bytes, has_alpha);
	  break;
	case 2:  /*  INDEXED  */
	  extract_from_indexed_pixels (s, d, m, cmap, bg, cut, src->w,
				       src->bytes, has_alpha);
	  break;
	}

      s += src->rowstride;
      d += dest->rowstride;
      if (mask)
	m += mask->rowstride;
    }
}


void
convolve_region (srcR, destR, matrix, size, divisor, mode)
     PixelRegion * srcR;
     PixelRegion * destR;
     int * matrix;
     int size;
     int divisor;
     int mode;
{
  /*  Convolve the src image using the convolution matrix, writing to dest  */
  unsigned char *src, *s_row, * s;
  unsigned char *dest, * d;
  int * m;
  int total [4];
  int b, bytes;
  int length;
  int wraparound;
  int margin;      /*  margin imposed by size of conv. matrix  */
  int i, j;
  int x, y;
  int offset;
  
  /*  If the mode is NEGATIVE, the offset should be 128  */
  if (mode == NEGATIVE)
    {
      offset = 128;
      mode = 0;
    }
  else
    offset = 0;

  /*  check for the boundary cases  */
  if (srcR->w < (size - 1) || srcR->h < (size - 1))
    return;

  /*  Initialize some values  */
  bytes = srcR->bytes;
  length = bytes * srcR->w;
  margin = size / 2;
  src = srcR->data;
  dest = destR->data;

  /*  calculate the source wraparound value  */
  wraparound = srcR->rowstride - size * bytes;

  /* copy the first (size / 2) scanlines of the src image... */
  for (i = 0; i < margin; i++)
    {
      memcpy (dest, src, length);
      src += srcR->rowstride;
      dest += destR->rowstride;
    }

  src = srcR->data;

  for (y = margin; y < srcR->h - margin; y++)
    {
      s_row = src;
      s = s_row + srcR->rowstride*margin;
      d = dest;

      /* handle the first margin pixels... */
      b = bytes * margin;
      while (b --)
	*d++ = *s++;

      /* now, handle the center pixels */
      x = srcR->w - margin*2;
      while (x--)
	{
	  s = s_row;

	  m = matrix;
	  total [0] = total [1] = total [2] = total [3] = 0;
	  i = size;
	  while (i --)
	    {
	      j = size;
	      while (j --)
		{
		  for (b = 0; b < bytes; b++)
		    total [b] += *m * *s++;
		  m ++;
		}

	      s += wraparound;
	    }	      

	  for (b = 0; b < bytes; b++)
	    {
	      total [b] = total [b] / divisor + offset;

	      /*  only if mode was ABSOLUTE will mode by non-zero here  */
	      if (total [b] < 0 && mode)
		total [b] = - total [b];

	      if (total [b] < 0)
		*d++ = 0;
	      else
		*d++ = (total [b] > 255) ? 255 : (unsigned char) total [b];
	    }

	  s_row += bytes;

	}

      /* handle the last pixel... */
      s = s_row + (srcR->rowstride + bytes) * margin;
      b = bytes * margin;
      while (b --)
	*d++ = *s++;

      /* set the memory pointers */
      src += srcR->rowstride;
      dest += destR->rowstride;
    }

  src += srcR->rowstride*margin;

  /* copy the last (margin) scanlines of the src image... */
  for (i = 0; i < margin; i++)
    {
      memcpy (dest, src, length);
      src += srcR->rowstride;
      dest += destR->rowstride;
    }
}


void
gaussian_blur_region (srcR, destR, radius, op)
     PixelRegion * srcR;
     PixelRegion * destR;
     double radius;
     int op;
{
  double std_dev;
  long width, height;
  long rowstride;
  unsigned char *dest, *dp;
  unsigned char *src, *sp;
  int *buf, *b;
  int pixels;
  int total;
  int i, row, col;
  int start, end;
  int *curve;
  int *sum;
  int val;
  int length;
  int alpha;
  int initial_p, initial_m;

  std_dev = sqrt (-(radius * radius) / (2 * log (1.0 / 255.0)));

  curve = make_curve (std_dev, &length);
  sum = xmalloc (sizeof (int) * (2 * length + 1));

  sum[0] = 0;
  
  for (i = 1; i <= length*2; i++)
    sum[i] = curve[i-length] + sum[i-1];
  sum += length;

  width = srcR->w;
  height = srcR->h;
  rowstride = srcR->rowstride;
  alpha = srcR->bytes - 1;

  buf = xmalloc (sizeof (int) * MAXIMUM (width, height) * 2);

  src = srcR->data;

  total = sum[length] - sum[-length];
	  
  for (col = 0; col < width; col++)
    {
      sp = src + alpha;

      src += srcR->bytes;
      
      initial_p = sp[0];
      initial_m = sp[(height-1) * rowstride];

      /*  Determine a run-length encoded version of the column  */
      run_length_encode (sp, buf, height, rowstride);

      for (row = 0; row < height; row++)
	{
	  start = (row < length) ? -row : -length;
	  end = (height <= (row + length)) ? (height - row - 1) : length;
	  
	  val = 0;
	  i = start;
	  b = buf + (row + i) * 2;

	  if (start != -length)
	    val += initial_p * (sum[start] - sum[-length]);

	  while (i < end)
	    {
	      pixels = b[0];
	      i += pixels;
	      if (i > end)
		i = end;
	      val += b[1] * (sum[i] - sum[start]);
	      b += (pixels * 2);
	      start = i;
	    }

	  if (end != length)
	    val += initial_m * (sum[length] - sum[end]);
	  
	  sp[row * rowstride] = val / total;
	}
    }

  src = srcR->data;
  dest = destR->data;

  for (row = 0; row < height; row++)
    {
      sp = src + alpha;
      dp = dest + alpha;
      
      src += rowstride;
      dest += rowstride;
      
      initial_p = sp[0];
      initial_m = sp[(width-1) * srcR->bytes];

      /*  Determine a run-length encoded version of the row  */
      run_length_encode (sp, buf, width, srcR->bytes);

      for (col = 0; col < width; col++)
	{
	  start = (col < length) ? -col : -length;
	  end = (width <= (col + length)) ? (width - col - 1) : length;
	  
	  val = 0;
	  i = start;
	  b = buf + (col + i) * 2;
	  
	  if (start != -length)
	    val += initial_p * (sum[start] - sum[-length]);

	  while (i < end)
	    {
	      pixels = b[0];
	      i += pixels;
	      if (i > end)
		i = end;
	      val += b[1] * (sum[i] - sum[start]);
	      b += (pixels * 2);
	      start = i;
	    }
	  
	  if (end != length)
	    val += initial_m * (sum[length] - sum[end]);

	  if (op == 0)  /*  op == ADD  */
	    {
	      val =  dp[col * srcR->bytes ] + (val / total);
	      if (val > 255) val = 255;
	    }
	  else if (op == 1)  /*  op == SUBTRACT  */
	    {
	      val = dp[col * srcR->bytes] - (val / total);
	      if (val < 0) val = 0;
	    }
	  else /*  op == REPLACE  */
	    val = val / total;

	  dp[col * srcR->bytes] = val;
	}
    }

  xfree (buf);
  xfree (sum - length);
  xfree (curve - length);
}


void
border_region (destPR, bs_ptr, bs_segs, radius)
     PixelRegion * destPR;
     void * bs_ptr;
     int bs_segs;
     int radius;
{
  BoundSeg * bs;
  unsigned char *dest, *d;
  unsigned char opacity;
  int r, i, j;

  bs = (BoundSeg *) bs_ptr;

  /*  clear out the destination's alpha channel  */
  dest = destPR->data + (destPR->bytes - 1);
  for (i = 0; i < destPR->h; i++)
    {
      d = dest;
      for (j = 0; j < destPR->w; j++)
	{
	  *d = TRANSPARENT;
	  d += destPR->bytes;
	}
      dest += destPR->rowstride;
    }

  /*  draw the border  */
  for (r = 0; r <= radius; r++)
    {
      opacity = 255 * (r + 1) / (radius + 1);
      j = radius - r;
      
      for (i = 0; i <= j; i++)
	{
	  /*  northwest  */
	  draw_segments (destPR, bs, bs_segs, -i, -(j - i), opacity);

	  /*  only draw the rest if they are different  */
	  if (j)
	    {
	      /*  northeast  */
	      draw_segments (destPR, bs, bs_segs, i, -(j - i), opacity);
	      /*  southeast  */
	      draw_segments (destPR, bs, bs_segs, i, (j - i), opacity);
	      /*  southwest  */
	      draw_segments (destPR, bs, bs_segs, -i, (j - i), opacity);
	    }
	}
    }
}


void
scale_region (srcPR, destPR)
     PixelRegion * srcPR;
     PixelRegion * destPR;
{
  unsigned char * src, * s;
  unsigned char * dest, * d;
  double * row, * r;
  long srcwidth, destwidth;
  int src_row, src_col;
  int bytes, b;
  int width, height;
  int orig_width, orig_height;
  double x_rat, y_rat;
  double x_cum, y_cum;
  double x_last, y_last;
  double * x_frac, y_frac, tot_frac;
  int i, j;
  int frac;
  int x1, y1, x2, y2;
  int x3, y3, x4, y4;
  int advance_dest;

  x1 = srcPR->x;
  y1 = srcPR->y;
  x2 = x1 + srcPR->w;
  y2 = y1 + srcPR->h;
  orig_width = srcPR->w;
  orig_height = srcPR->h;

  x3 = destPR->x;
  y3 = destPR->y;
  x4 = x3 + destPR->w;
  y4 = y3 + destPR->h;
  width = destPR->w;
  height = destPR->h;

  /*  Some calculations...  */
  bytes = srcPR->bytes;
  srcwidth = srcPR->rowstride;
  destwidth = destPR->rowstride;

  /*  the data pointers...  */
  src  = srcPR->data;
  dest = destPR->data;

  /*  find the ratios of old x to new x and old y to new y  */
  x_rat = (double) orig_width / (double) width;
  y_rat = (double) orig_height / (double) height;

  /*  allocate an array to help with the calculations  */
  row    = (double *) xmalloc (sizeof (double) * destwidth);
  x_frac = (double *) xmalloc (sizeof (double) * (width + orig_width));

  /*  initialize the pre-calculated pixel fraction array  */
  src_col = x1;
  x_cum = (double) src_col;
  x_last = x_cum;

  for (i = 0; i < width + orig_width; i++)
    {
      if (x_cum + x_rat <= src_col + 1)
	{
	  x_cum += x_rat;
	  x_frac[i] = x_cum - x_last;
	}
      else
	{
	  src_col ++;
	  x_frac[i] = src_col - x_last;
	}
      x_last += x_frac[i];
    }

  /*  clear the "row" array  */
  memset (row, 0, sizeof (double) * width * bytes);

  /*  counters...  */
  src_row = y1;
  y_cum = (double) src_row;
  y_last = y_cum;

  /*  Scale the selected region  */
  i = height;
  while (i)
    {
      src_col = x1;
      x_cum = (double) src_col;

      /* determine the fraction of the src pixel we are using for y */
      if (y_cum + y_rat <= src_row + 1)
	{
	  y_cum += y_rat;
	  y_frac = y_cum - y_last;
	  advance_dest = TRUE;
	}
      else
	{
	  src_row ++;
	  y_frac = src_row - y_last;
	  advance_dest = FALSE;
	}

      y_last += y_frac;

      s = src;
      r = row;

      frac = 0;
      j = width;

      while (j)
	{
	  tot_frac = x_frac[frac++] * y_frac;

	  for (b = 0; b < bytes; b++)
	    r[b] += s[b] * tot_frac;

	  /*  increment the destination  */
	  if (x_cum + x_rat <= src_col + 1)
	    {
	      r += bytes;
	      x_cum += x_rat;
	      j--;
	    }

	  /* increment the source */
	  else
	    {
	      s += bytes;
	      src_col++;
	    }
	}

      if (advance_dest)
	{
	  tot_frac = 1.0 / (x_rat * y_rat);

	  /*  copy "row" to "dest"  */
	  d = dest;
	  r = row;

	  j = width;
	  while (j--)
	    {
	      b = bytes;
	      while (b--)
		*d++ = (unsigned char) (*r++ * tot_frac);
	    }

	  dest += destwidth;

	  /*  clear the "row" array  */
	  memset (row, 0, sizeof (double) * destwidth);

	  i--;
	}
      else
	src += srcwidth;
    }

  /*  free up temporary arrays  */
  xfree (row);
  xfree (x_frac);
}


void 
swap_region (src, dest)
     PixelRegion * src;
     PixelRegion * dest;
{
  int h;
  int length;
  unsigned char * s, * d, * swap;

  s = src->data;
  h = src->h;
  d = dest->data;
  length = src->w * src->bytes;
  swap = paint_funcs_get_buffer (length);

  while (h --)
    {
      swap_pixels (s, d, swap, length);
      s += src->rowstride;
      d += dest->rowstride;
    }
}


void
apply_mask_to_region (src, mask, opacity)
     PixelRegion * src;
     PixelRegion * mask;
     int opacity;
{
  int h;
  unsigned char * s, * m;

  s = src->data;
  m = mask->data;
  h = src->h;

  while (h --)
    {
      apply_mask_to_alpha_channel (s, m, opacity, src->w, src->bytes);
      s += src->rowstride;
      m += mask->rowstride;
    }
}


void
combine_mask_and_region (src, mask, opacity)
     PixelRegion * src;
     PixelRegion * mask;
     int opacity;
{
  int h;
  unsigned char * s, * m;

  s = src->data;
  m = mask->data;
  h = src->h;

  while (h --)
    {
      combine_mask_and_alpha_channel (s, m, opacity, src->w, src->bytes);
      s += src->rowstride;
      m += mask->rowstride;
    }
}


void
initial_region (src, dest, mask, data, opacity, mode, affect, type)
     PixelRegion * src;
     PixelRegion * dest;
     PixelRegion * mask;
     unsigned char * data;
     int opacity;
     int mode;
     int * affect;
     int type;
{
  int h;
  unsigned char * s, * d, * m;
  unsigned char * buf;

  s = src->data;
  d = dest->data;
  m = (mask) ? mask->data : NULL;
  h = src->h;

  buf = paint_funcs_get_buffer (src->w * (src->bytes + 1));

  while (h --)
    {
      /*  based on the type of the initial image...  */
      switch (type)
	{
	case INITIAL_CHANNEL:
	  initial_channel_pixels (s, d, src->w, dest->bytes);
	  break;
	case INITIAL_INDEXED:
	  initial_indexed_pixels (s, d, data, affect, src->w);
	  break;
	case INITIAL_INTENSITY:
	  if (mode == DISSOLVE_MODE)
	    {
	      dissolve_pixels (s, buf, opacity, src->w, src->bytes,
			       src->bytes + 1, 0);
	      initial_inten_pixels (buf, d, m, opacity, affect,
				    src->w, src->bytes);
	    }
	  else
	    initial_inten_pixels (s, d, m, opacity, affect, src->w, src->bytes);
	  break;
	case INITIAL_INTENSITY_ALPHA:
	  if (mode == DISSOLVE_MODE)
	    {
	      dissolve_pixels (s, buf, opacity, src->w, src->bytes,
			       src->bytes, 1);
	      initial_inten_a_pixels (buf, d, m, opacity, affect,
				      src->w, src->bytes);
	    }
	  else
	    initial_inten_a_pixels (s, d, m, opacity, affect, src->w, src->bytes);
	  break;
	}

      s += src->rowstride;
      d += dest->rowstride;
      if (mask)
	m += mask->rowstride;
    }
}


void
combine_regions (src1, src2, dest, mask, data, opacity, mode, affect, type)
     PixelRegion * src1, * src2;
     PixelRegion * dest, * mask;
     unsigned char * data;
     int opacity;
     int mode;
     int * affect;
     int type;
{
  int h;
  int ha1, ha2;
  int combine;
  unsigned char * s, * s1, * s2;
  unsigned char * d, * m;
  unsigned char * buf;

  s1 = src1->data;
  s2 = src2->data;
  d = dest->data;
  m = (mask) ? mask->data : NULL;
  h = src1->h;

  /*  Determine which sources have alpha channels  */
  switch (type)
    {
    case COMBINE_INTEN_INTEN:
      ha1 = ha2 = 0;
      break;
    case COMBINE_INTEN_A_INTEN:
      ha1 = 1;
      ha2 = 0;
      break;
    case COMBINE_INTEN_INTEN_A:
      ha1 = 0;
      ha2 = 1;
      break;
    case COMBINE_INTEN_A_INTEN_A:
      ha1 = ha2 = 1;
      break;
    default:
      ha1 = ha2 = 0;
    }

  buf = paint_funcs_get_buffer (src1->w * (src1->bytes + 1));

  while (h --)
    {
      s = buf;

      /*  apply the paint mode based on the combination type & mode  */
      switch (type)
	{
	case COMBINE_INDEXED_INDEXED:
	case COMBINE_INDEXED_INDEXED_A:
	case COMBINE_INDEXED_A_INDEXED_A:
	case COMBINE_INTEN_A_INDEXED_A:
	case COMBINE_INTEN_A_CHANNEL:
	  combine = type;
	  break;

	case COMBINE_INTEN_INTEN_A:
	case COMBINE_INTEN_A_INTEN:
	case COMBINE_INTEN_INTEN:
	case COMBINE_INTEN_A_INTEN_A:
	  /*  Now, apply the paint mode  */
	  combine = apply_layer_mode (s1, s2, &s, opacity, src1->w, mode, 
				      src1->bytes, src2->bytes, ha1, ha2);
	  break;

	default:
	  break;
	}

      /*  based on the type of the initial image...  */
      switch (combine)
	{
	case COMBINE_INDEXED_INDEXED:
	  combine_indexed_and_indexed_pixels (s1, s2, d, m, opacity,
					      src1->w, src1->bytes);
	  break;

	case COMBINE_INDEXED_INDEXED_A:
	  combine_indexed_and_indexed_a_pixels (s1, s2, d, m, opacity,
						src1->w, src1->bytes);
	  break;

	case COMBINE_INDEXED_A_INDEXED_A:
	  combine_indexed_a_and_indexed_a_pixels (s1, s2, d, m, opacity,
						  src1->w, src1->bytes);
	  break;

	case COMBINE_INTEN_A_INDEXED_A:
	  /*  assume the data passed to this procedure is the
	   *  indexed layer's colormap
	   */
	  combine_inten_a_and_indexed_a_pixels (s1, s2, d, m, data, opacity,
						src1->w, src1->bytes);
	  break;

	case COMBINE_INTEN_A_CHANNEL:
	  /*  assume the data passed to this procedure is the
	   *  channel's color
	   */
	  combine_inten_a_and_channel_pixels (s1, s2, d, data, opacity,
					      src1->w, dest->bytes);
	  break;

	case COMBINE_INTEN_INTEN:
	  combine_inten_and_inten_pixels (s1, s, d, m, opacity,
					  affect, src1->w, src1->bytes);
	  break;

	case COMBINE_INTEN_INTEN_A:
	  combine_inten_and_inten_a_pixels (s1, s, d, m, opacity,
					    affect, src1->w, src1->bytes);
	  break;

	case COMBINE_INTEN_A_INTEN:
	  combine_inten_a_and_inten_a_pixels (s1, s, d, m, opacity,
					      affect, src1->w, src1->bytes);
	  break;

	case COMBINE_INTEN_A_INTEN_A:
	  combine_inten_a_and_inten_a_pixels (s1, s, d, m, opacity,
					      affect, src1->w, src1->bytes);
	  break;

	case REPLACE_INTEN_INTEN:
	  replace_inten_pixels (s1, s, d, m, opacity,
				affect, src1->w, src1->bytes,
				src2->bytes, ha1, ha2);
	  break;

	case ERASE_INTEN_INTEN:
	  erase_inten_pixels (s1, s, d, m, opacity,
			      affect, src1->w, src1->bytes);

	default:
	  break;
	}
      
      s1 += src1->rowstride;
      s2 += src2->rowstride;
      d += dest->rowstride;
      if (mask)
	m += mask->rowstride;
    }
}


/*********************************
 *   color conversion routines   *
 *********************************/

void
rgb_to_hsv (r, g, b)
     int *r, *g, *b;
{
  int red, green, blue;
  float h, s, v;
  int min, max;
  int delta;
  
  red = *r;
  green = *g;
  blue = *b;

  if (red > green)
    {
      if (red > blue)
	max = red;
      else
	max = blue;
      
      if (green < blue)
	min = green;
      else
	min = blue;
    }
  else
    {
      if (green > blue)
	max = green;
      else
	max = blue;
      
      if (red < blue)
	min = red;
      else
	min = blue;
    }
  
  v = max;
  
  if (max != 0)
    s = ((max - min) * 255) / (float) max;
  else
    s = 0;
  
  if (s == 0)
    h = 0;
  else
    {
      delta = max - min;
      if (red == max)
	h = (green - blue) / (float) delta;
      else if (green == max)
	h = 2 + (blue - red) / (float) delta;
      else if (blue == max)
	h = 4 + (red - green) / (float) delta;
      h *= 42.5;

      if (h < 0)
	h += 255;
      if (h > 255)
	h -= 255;
    }

  *r = h;
  *g = s;
  *b = v;
}


void
hsv_to_rgb (h, s, v)
     int *h, *s, *v;
{
  float hue, saturation, value;
  float f, p, q, t;

  if (*s == 0)
    {
      *h = *v;
      *s = *v;
      *v = *v;
    }
  else
    {
      hue = *h * 6.0 / 255.0;
      saturation = *s / 255.0;
      value = *v / 255.0;

      f = hue - (int) hue;
      p = value * (1.0 - saturation);
      q = value * (1.0 - (saturation * f));
      t = value * (1.0 - (saturation * (1.0 - f)));
      
      switch ((int) hue)
	{
	case 0:
	  *h = value * 255;
	  *s = t * 255;
	  *v = p * 255;
	  break;
	case 1:
	  *h = q * 255;
	  *s = value * 255;
	  *v = p * 255;
	  break;
	case 2:
	  *h = p * 255;
	  *s = value * 255;
	  *v = t * 255;
	  break;
	case 3:
	  *h = p * 255;
	  *s = q * 255;
	  *v = value * 255;
	  break;
	case 4:
	  *h = t * 255;
	  *s = p * 255;
	  *v = value * 255;
	  break;
	case 5:
	  *h = value * 255;
	  *s = p * 255;
	  *v = q * 255;
	  break;
	}
    }
}


void
rgb_to_hls (r, g, b)
     int *r, *g, *b;
{
  int red, green, blue;
  float h, l, s;
  int min, max;
  int delta;
  
  red = *r;
  green = *g;
  blue = *b;

  if (red > green)
    {
      if (red > blue)
	max = red;
      else
	max = blue;
      
      if (green < blue)
	min = green;
      else
	min = blue;
    }
  else
    {
      if (green > blue)
	max = green;
      else
	max = blue;
      
      if (red < blue)
	min = red;
      else
	min = blue;
    }
  
  l = (max + min) / 2.0;
  
  if (max == min)
    {
      s = 0.0;
      h = 0.0;
    }
  else
    {
      delta = (max - min);

      if (l < 128)
	s = 255 * (float) delta / (float) (max + min);
      else
	s = 255 * (float) delta / (float) (511 - max - min);

      if (red == max)
	h = (green - blue) / (float) delta;
      else if (green == max)
	h = 2 + (blue - red) / (float) delta;
      else
	h = 4 + (red - green) / (float) delta;

      h = h * 42.5;

      if (h < 0)
	h += 255;
      if (h > 255)
	h -= 255;
    }

  *r = h;
  *g = l;
  *b = s;
}


static int
hls_value (n1, n2, hue)
     float n1, n2, hue;
{
  float value;

  if (hue > 255)
    hue -= 255;
  else if (hue < 0)
    hue += 255;
  if (hue < 42.5)
    value = n1 + (n2 - n1) * (hue / 42.5);
  else if (hue < 127.5)
    value = n2;
  else if (hue < 170)
    value = n1 + (n2 - n1) * ((170 - hue) / 42.5);
  else
    value = n1;
  
  return (int) (value * 255);
}


void
hls_to_rgb (h, l, s)
     int *h, *l, *s;
{
  float hue, lightness, saturation;
  float m1, m2;

  hue = *h;
  lightness = *l;
  saturation = *s;

  if (lightness < 128)
    m2 = (lightness * (255 + saturation)) / 65025.0;
  else
    m2 = (lightness + saturation - (lightness * saturation)/255.0) / 255.0;

  m1 = (lightness / 127.5) - m2;
  if (saturation == 0)
    {
      /*  achromatic case  */
      *h = lightness;
      *l = lightness;
      *s = lightness;
    }
  else
    {
      /*  chromatic case  */
      *h = hls_value (m1, m2, hue + 85);
      *l = hls_value (m1, m2, hue);
      *s = hls_value (m1, m2, hue - 85);
    }
}


/************************************/
/*       apply layer modes          */
/************************************/

int
apply_layer_mode (src1, src2, dest, opacity, length, mode, b1, b2, ha1, ha2)
     unsigned char * src1, * src2;
     unsigned char ** dest;
     int opacity;
     int length;
     int mode;
     int b1, b2;   /* bytes */
     int ha1, ha2; /* has alpha */
{
  int combine;

  if (!ha1 && !ha2)
    combine = COMBINE_INTEN_INTEN;
  else if (!ha1 && ha2)
    combine = COMBINE_INTEN_INTEN_A;
  else if (ha1 && !ha2)
    combine = COMBINE_INTEN_A_INTEN;
  else
    combine = COMBINE_INTEN_A_INTEN_A;

  /*  assumes we're applying src2 TO src1  */
  switch (mode)
    {
    case NORMAL_MODE:
      *dest = src2;
      break;
      
    case DISSOLVE_MODE:
      /*  Since dissolve requires an alpha channels...  */
      if (! ha2)
	add_alpha_pixels (src2, *dest, length, b2);

      dissolve_pixels (src2, *dest, opacity, length, b2,
		       ((ha2) ? b2 : b2 + 1), ha2);
      combine = (ha1) ? COMBINE_INTEN_A_INTEN_A : COMBINE_INTEN_INTEN_A;
      break;

    case MULTIPLY_MODE:
      multiply_pixels (src1, src2, *dest, length, b1, b2, ha1, ha2);
      break;

    case SCREEN_MODE:
      screen_pixels (src1, src2, *dest, length, b1, b2, ha1, ha2);
      break;

    case DIFFERENCE_MODE:
      difference_pixels (src1, src2, *dest, length, b1, b2, ha1, ha2);
      break;

    case ADDITION_MODE:
      add_pixels (src1, src2, *dest, length, b1, b2, ha1, ha2);
      break;

    case SUBTRACT_MODE:
      subtract_pixels (src1, src2, *dest, length, b1, b2, ha1, ha2);
      break;

    case DARKEN_ONLY_MODE:
      darken_pixels (src1, src2, *dest, length, b1, b2, ha1, ha2);
      break;
      
    case LIGHTEN_ONLY_MODE:
      lighten_pixels (src1, src2, *dest, length, b1, b2, ha1, ha2);
      break;
      
    case HUE_MODE: case SATURATION_MODE: case VALUE_MODE:
      /*  only works on RGB color images  */
      if (b1 > 2)
	hsv_only_pixels (src1, src2, *dest, mode, length, b1, b2, ha1, ha2);
      else
	*dest = src2;
      break;

    case COLOR_MODE:
      /*  only works on RGB color images  */
      if (b1 > 2)
	color_only_pixels (src1, src2, *dest, mode, length, b1, b2, ha1, ha2);
      else
	*dest = src2;
      break;

    case REPLACE_MODE:
      *dest = src2;
      combine = REPLACE_INTEN_INTEN;
      break;

    case ERASE_MODE:
      *dest = src2;
      /*  If both sources have alpha channels, call erase function.
       *  Otherwise, just combine in the normal manner
       */
      combine = (ha1 && ha2) ? ERASE_INTEN_INTEN : combine;
      break;

    default :
      break;
    }

  return combine;
}


