/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "colormaps.h"
#include "cursorutil.h"
#include "gdisplay.h"
#include "gimprc.h"
#include "interface.h"
#include "scale.h"
#include "scroll.h"
#include "general.h"
#include "disp_callbacks.h"
#include "tools.h"

static void
redraw (gdisp, x, y, w, h)
     GDisplay *gdisp;
     int x, y;
     int w, h;
{
  long x1, y1, x2, y2;    /*  coordinate of rectangle corners  */

  x1 = x;
  y1 = y;
  x2 = (x+w);
  y2 = (y+h);
      
  x1 = BOUNDS (x1, 0, gdisp->disp_width);
  y1 = BOUNDS (y1, 0, gdisp->disp_height);
  x2 = BOUNDS (x2, 0, gdisp->disp_width);
  y2 = BOUNDS (y2, 0, gdisp->disp_height);

  if ((x2 - x1) && (y2 - y1))
    {
      gdisplay_expose_area (gdisp, x1, y1, (x2 - x1), (y2 - y1));
      gdisplays_flush ();
    }
}

gint
gdisplay_canvas_events (canvas, event)
     GtkWidget *canvas;
     GdkEvent *event;
{
  GDisplay *gdisp;
  GdkEventExpose *eevent;
  GdkEventMotion *mevent;
  GdkEventButton *bevent;
  GdkEventKey *kevent;
  gint sx, sy;
  gint tx, ty;
  double x, y;

  gdisp = (GDisplay *) gtk_widget_get_user_data (canvas);

  switch (event->type)
    {
    case GDK_EXPOSE:
      /*  If this is the first expose event...  */
      if (!gdisp->select)
	{
	  /*  create the selection object  */
	  gdisp->select = selection_create (gdisp->canvas->window, gdisp,
					    gdisp->gimage->height,
					    gdisp->gimage->width, marching_speed);

	  gdisp->disp_width = gdisp->canvas->window->width;
	  gdisp->disp_height = gdisp->canvas->window->height;

	  /*  set up the scrollbar observers  */
	  gdisp->hsb_observer.update = scrollbar_horz_update;
	  gdisp->hsb_observer.disconnect = scrollbar_disconnect;
	  gdisp->hsb_observer.user_data = (gpointer) gdisp;
	  gdisp->vsb_observer.update = scrollbar_vert_update;
	  gdisp->vsb_observer.disconnect = scrollbar_disconnect;
	  gdisp->vsb_observer.user_data = (gpointer) gdisp;
	  gtk_data_attach ((GtkData *) gdisp->hsbdata, &gdisp->hsb_observer);
	  gtk_data_attach ((GtkData *) gdisp->vsbdata, &gdisp->vsb_observer);

	  /*  setup scale properly  */
	  setup_scale (gdisp);
	}

      eevent = (GdkEventExpose *) event;
      redraw (gdisp, eevent->area.x, eevent->area.y,
	      eevent->area.width, eevent->area.height);
      break;

    case GDK_RESIZE:
      gdisp->disp_width = gdisp->canvas->window->width;
      gdisp->disp_height = gdisp->canvas->window->height;

      resize_display (gdisp);
      break;

    case GDK_BUTTON_PRESS:
      bevent = (GdkEventButton *) event;

      switch (bevent->button)
	{
	case 1:
	  if (active_tool)
	    (* active_tool->button_press_func) (active_tool, bevent, gdisp);
	  break;

	case 2:
	  start_grab_and_scroll (gdisp, bevent);
	  break;

	case 3:
	  popup_shell = gdisp->shell;
	  gdk_window_get_pointer (NULL, &sx, &sy, NULL);
	  gtk_widget_popup (gdisp->popup, sx, sy);
	  break;

	default:
	  break;
	}
      break;

    case GDK_BUTTON_RELEASE:
      bevent = (GdkEventButton *) event;
      
      switch (bevent->button)
	{
	case 1:
	  if (active_tool)
	    if (active_tool->state == ACTIVE)
	      (* active_tool->button_release_func) (active_tool, bevent, gdisp);
	  break;

	case 2:
	  end_grab_and_scroll (gdisp, bevent);
	  break;

	case 3:
	  gtk_widget_hide (gdisp->popup);
	  popup_shell = NULL;
	  break;

	default:
	  break;
	}
      break;

    case GDK_MOTION_NOTIFY:
      mevent = (GdkEventMotion *) event;
      if (mevent->is_hint)
	{
	  gdk_window_get_pointer (canvas->window, &tx, &ty, NULL);
	  mevent->x = tx;
	  mevent->y = ty;
	}
      
      gdisplay_untransform_coords_f (gdisp, mevent->x, mevent->y, &x, &y, FALSE);
      gdisp->hruledata->value = x;
      gdisp->vruledata->value = y;
      gtk_data_notify ((GtkData *) gdisp->hruledata);
      gtk_data_notify ((GtkData *) gdisp->vruledata);

      if (active_tool && (mevent->state & GDK_BUTTON1_MASK))
	{
	  if (active_tool->state == ACTIVE)
	    {
	      /*  if the first mouse button is down, check for automatic
	       *  scrolling...
	       */
	      if ((mevent->state & Button1Mask) && !active_tool->scroll_lock)
		{
		  /* if (mevent->x < 0 || mevent->y < 0 ||
		     mevent->x > gdisp->disp_width ||
		     mevent->y > gdisp->disp_height)
		     scroll_to_pointer_position (gdisp, mevent);
		     */
		}
	      
	      (* active_tool->motion_func) (active_tool, mevent, gdisp);
	    }
	}
      else if (mevent->state & GDK_BUTTON2_MASK)
	grab_and_scroll (gdisp, mevent);
      break;

    case GDK_KEY_PRESS:
      kevent = (GdkEventKey *) event;

      switch (kevent->keyval)
	{
	case XK_Left : case XK_Right :
	case XK_Up   : case XK_Down  :
	  if (active_tool)
	    (* active_tool->arrow_keys_func) (active_tool, kevent, gdisp);
	  break;
	}
      break;

    case GDK_KEY_RELEASE:
      break;

    case GDK_ENTER_NOTIFY:
      install_colormap (gdisp->color_type);
      break;

    default:
      break;
    }

  return FALSE;
}

