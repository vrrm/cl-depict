/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "by_color_select.h"
#include "channel.h"
#include "errors.h"
#include "floating_sel.h"
#include "gdisplay.h"
#include "gimage_mask.h"
#include "gimprc.h"
#include "tools.h"
#include "undo.h"
#include "layer.h"
#include "layers_dialog.h"
#include "linked.h"
#include "paint_core.h"
#include "paint_funcs.h"
#include "transform_core.h"


typedef int (* UndoPopFunc)  (int, int, int, void *);
typedef void    (* UndoFreeFunc) (int, void *);

typedef struct _undo Undo;

struct _undo
{
  int           ID;             /*  ID of gdisplay or gimage structure  */
  int           type;           /*  undo type                           */
  void *        data;           /*  data to implement the undo          */
  long          bytes;          /*  size of undo item                   */

  UndoPopFunc   pop_func;       /*  function pointer to undo pop proc   */
  UndoFreeFunc  free_func;      /*  function with specifics for freeing */
};


/*  Pop functions  */

int      undo_pop_image            (int, int, int, void *);
int      undo_pop_mask             (int, int, int, void *);
int      undo_pop_fs_swap          (int, int, int, void *);
int      undo_pop_fs_translate     (int, int, int, void *);
int      undo_pop_fs_mask          (int, int, int, void *);
int      undo_pop_layer_displace   (int, int, int, void *);
int      undo_pop_global_edit      (int, int, int, void *);
int      undo_pop_transform        (int, int, int, void *);
int      undo_pop_paint            (int, int, int, void *);

/*  Free functions  */

void         undo_free_image           (int, void *);
void         undo_free_mask            (int, void *);
void         undo_free_fs_swap         (int, void *);
void         undo_free_fs_translate    (int, void *);
void         undo_free_fs_mask         (int, void *);
void         undo_free_layer_displace  (int, void *);
void         undo_free_global_edit     (int, void *);
void         undo_free_transform       (int, void *);
void         undo_free_paint           (int, void *);


/*  Sizing functions  */
static int   layer_size                (Layer *);
static int   channel_size              (Channel *);

/*  the undo stack  */
static link_ptr undo_stack     = NULL;
static link_ptr redo_stack     = NULL;
static long     total_bytes    = 0;
static long     total_levels   = 0;
static int      pushing_group  = 0;

extern TempBuf *global_buf;


#define UNDO          0
#define REDO          1
#define VERIFY        2


static int
layer_size (layer)
     Layer * layer;
{
  int size;
  
  size = sizeof (Layer) + layer->width * layer->height * layer->bytes +
    strlen (layer->name);
  
  if (layer_mask (layer))
    size += channel_size (layer_mask (layer));

  return size;
}


static int
channel_size (channel)
     Channel * channel;
{
  int size;
  
  size = sizeof (Channel) + channel->width * channel->height + 
    strlen (channel->name);

  return size;
}


static void
undo_free_list (state, list)
     int state;
     link_ptr list;
{
  link_ptr orig;
  Undo * undo;

  orig = list;

  while (list)
    {
      undo = (Undo *) list->data;
      if (undo)
	{
	  (* undo->free_func) (state, undo->data);
	  total_bytes -= undo->bytes;
	  xfree (undo);
	}
      list = next_item (list);
    }
  
  free_list (orig);
}


static link_ptr
undo_verify_list (state, list)
     int state;
     link_ptr list;
{
  Undo * undo;

  if (!list)
    return NULL;

  undo = (Undo *) list->data;
  if (undo)
    {
      if (! (* undo->pop_func) (undo->ID, VERIFY, undo->type, undo->data))
	{
	  (* undo->free_func) (state, undo->data);
	  total_bytes -= undo->bytes;
	  xfree (undo);
	  if (state == UNDO)
	    total_levels --;
	  return undo_verify_list (state, next_item (list));
	}
      else
	list->next = undo_verify_list (state, next_item (list));
    }
  else
    list->next = undo_verify_list (state, next_item (list));

  return list;
}


static link_ptr
remove_stack_bottom ()
{
  link_ptr list, last;
  int in_group = 0;

  list = undo_stack;

  last = NULL;
  while (list)
    {
      if (list->next == NULL)
	{
	  if (last)
	    undo_free_list (UNDO, last->next);
	  else
	    undo_free_list (UNDO, list);

	  total_levels --;
	  if (last)
	    last->next = NULL;
	  list = NULL;
	}
      else
	{
	  if (list->data == NULL)
	    in_group = (in_group) ? 0 : 1;

	  /*  Make sure last points to only a single item, or the
	   *  tail of an aggregate undo item
	   */
	  if ((list->data && !in_group) ||
	      (!list->data && !in_group))
	  last = list;

	  list = next_item (list);
	}
    }

  if (last)
    return undo_stack;
  else
    return NULL;

}


static int
undo_free_up_space ()
{
  /*  If there are 0 levels of undo return FALSE.
   */
  if (levels_of_undo == 0)
    return FALSE;
  
  /*  Delete the item on the bottom of the stack if we have the maximum
   *  levels of undo already
   */
  while (total_levels >= levels_of_undo)
    undo_stack = remove_stack_bottom ();

  return TRUE;
}



static Undo *
undo_push (size, type)
     long size;
     int type;
{
  Undo * new;

  size += sizeof (Undo);

  if (redo_stack)
    {
      undo_free_list (REDO, redo_stack);
      redo_stack = NULL;
    }

  if (!pushing_group)
    if (! undo_free_up_space ())
      return NULL;

  new = (Undo *) xmalloc (sizeof (Undo));

  new->bytes = size;
  total_bytes += size;
  if (!pushing_group)  /*  only increment levels if not in a group  */
    {
      new->type = type;
      total_levels ++;
    }
  else
    new->type = pushing_group;

  undo_stack = add_to_list (undo_stack, (void *) new);

  return new;
}


static int
pop_stack (stack_ptr, unstack_ptr, state)
     link_ptr *stack_ptr;
     link_ptr *unstack_ptr;
     int state;
{
  Undo * object;
  link_ptr stack;
  link_ptr tmp;
  int status = 0;
  int in_group = 0;

  /*  Keep popping until we pop a valid object
   *  or get to the end of a group if we're in one
   */
  while (*stack_ptr)
    {
      stack = *stack_ptr;

      object = (Undo *) stack->data;
      if (object == NULL)
	{
	  in_group = (in_group) ? 0 : 1;
	  if (in_group)
	    total_levels += (state == UNDO) ? -1 : 1;

	  if (status && !in_group)
	    status = 1;
	  else
	    status = 0;
	}
      else
	{
	  status = (* object->pop_func) (object->ID, state,
					 object->type, object->data);
	  if (!in_group)
	    total_levels += (state == UNDO) ? -1 : 1;
	}

      *unstack_ptr = add_to_list (*unstack_ptr, (void *) object);

      tmp = stack;
      *stack_ptr = next_item (*stack_ptr);
      tmp->next = NULL;
      free_list (tmp);

      if (status && !in_group)
	{
	  /*  Flush any image updates and displays  */
	  gdisplays_flush ();
	  return TRUE;
	}
    }

  return FALSE;
}


int
undo_pop ()
{
  return pop_stack (&undo_stack, &redo_stack, UNDO);
}


int
undo_redo ()
{
  return pop_stack (&redo_stack, &undo_stack, REDO);
}


void
undo_clean_stack ()
{
  /*  Clean out any dated undo structures in the stack  */
  undo_stack = undo_verify_list (UNDO, undo_stack);
  redo_stack = undo_verify_list (REDO, redo_stack);
}


void
undo_free ()
{
  undo_free_list (UNDO, undo_stack);
  undo_free_list (REDO, redo_stack);
  undo_stack = NULL;
  redo_stack = NULL;
}


/**********************/
/*  group Undo funcs  */

int
undo_push_group_start (type)
     int type;
{
  if (redo_stack)
    {
      undo_free_list (REDO, redo_stack);
      redo_stack = NULL;
    }

  if (! undo_free_up_space ())
    return FALSE;
  
  pushing_group = type;
  undo_stack = add_to_list (undo_stack, NULL);
  total_levels++;

  return TRUE;
}


int
undo_push_group_end (void)
{
  pushing_group = 0;
  undo_stack = add_to_list (undo_stack, NULL);

  return TRUE;
}


/*********************************/
/*  Image Undo functions         */


int
undo_push_image (gimage_ID, layer_ID, channel_ID, x1, y1, x2, y2)
     int gimage_ID;
     int layer_ID;
     int channel_ID;
     int x1, y1;
     int x2, y2;
{
  GImage * gimage;
  long size;
  Undo * new;
  TempBuf * buf;
  void ** data;

  if (! (gimage = gimage_get_ID (gimage_ID)))
    return FALSE;

  /*  increment the dirty flag for this gimage  */
  if (gimage->dirty < 0)
    gimage->dirty = 2;
  else
    gimage->dirty ++;

  /*  If we cannot create a new temp buf--either because our parameters are
   *  degenerate or something else failed, simply return an unsuccessful push.
   */
  if (! (buf = temp_buf_load (NULL, gimage, x1, y1, (x2 - x1), (y2 - y1))))
    return FALSE;

  size = buf->width * buf->height * buf->bytes;

  if ((new = undo_push (size, IMAGE_UNDO)))
    {
      data           = malloc (sizeof (void *) * 3);
      data[0]        = buf;
      /*  kinda kludgy here...  */
      data[1]        = (void *) layer_ID;
      data[2]        = (void *) channel_ID;

      new->ID        = gimage_ID;
      new->data      = data;
      new->pop_func  = undo_pop_image;
      new->free_func = undo_free_image;

      /*  disk swap this temporary buffer -- in the interests of memory usage */
      temp_buf_swap (buf);

      return TRUE;
    }
  else
    return FALSE;
}


int
undo_push_image_mod (gimage_ID, layer_ID, channel_ID, buf_ptr)
     int gimage_ID;
     int layer_ID;
     int channel_ID;
     void * buf_ptr;
{
  GImage * gimage;
  long size;
  Undo * new;
  TempBuf * buf;
  void ** data;

  if (! (gimage = gimage_get_ID (gimage_ID)))
    return FALSE;

  /*  increment the dirty flag for this gimage  */
  if (gimage->dirty < 0)
    gimage->dirty = 2;
  else
    gimage->dirty ++;

  if (! buf_ptr)
    return FALSE;

  buf = (TempBuf *) buf_ptr;
  size = buf->width * buf->height * buf->bytes;

  if ((new = undo_push (size, IMAGE_MOD_UNDO)))
    {
      data           = malloc (sizeof (void *) * 3);
      data[0]        = buf_ptr;
      /*  kinda kludgy here...  */
      data[1]        = (void *) layer_ID;
      data[2]        = (void *) channel_ID;

      new->ID        = gimage_ID;
      new->data      = data;
      new->pop_func  = undo_pop_image;
      new->free_func = undo_free_image;

      /*  disk swap this temporary buffer -- in the interests of memory usage */
      temp_buf_swap (buf);

      return TRUE;
    }
  else
    return FALSE;
}


int
undo_pop_image (ID, state, type, data_ptr)
     int ID;
     int state;
     int type;
     void * data_ptr;
{
  GImage * gimage;
  Layer * layer;
  Channel * channel;
  int layer_ID;
  int channel_ID;
  TempBuf * temp_buf;
  PixelRegion PR1, PR2;
  void ** data;

  if (! (gimage = gimage_get_ID (ID)))
    return FALSE;

  switch (state)
    {
    case VERIFY: 
      return TRUE; break;
    case UNDO:
      gimage->dirty--; break;
    case REDO:
      gimage->dirty++; break;
    default : break;
    }
    
  data = (void **) data_ptr;
  temp_buf = (TempBuf *) data[0];
  layer_ID = (int) data[1];
  channel_ID = (int) data[2];

  /*  unswap the temporary buffer data  */
  temp_buf_unswap (temp_buf);

  PR1.bytes = temp_buf->bytes;
  PR1.w = temp_buf->width;
  PR1.h = temp_buf->height;
  PR1.rowstride = temp_buf->width * PR1.bytes;
  PR1.data = temp_buf_data (temp_buf);

  if (layer_ID >= 0)
    {
      if ((layer = layer_get_ID (layer_ID)))
	{
	  PR2.bytes = layer->bytes;
	  PR2.rowstride = layer->width * PR2.bytes;
	  PR2.data = layer_data (layer) + temp_buf->y * PR2.rowstride +
	    temp_buf->x * PR2.bytes;

	  layers_dialog_dirty_layer (layer->layer_widget);
	}
      else
	return FALSE;
    }
  else if (channel_ID >= 0)
    {
      if ((channel = channel_get_ID (layer_ID)))
	{
	  PR2.bytes = channel->bytes;
	  PR2.rowstride = channel->width * PR2.bytes;
	  PR2.data = channel_data (channel) + temp_buf->y * PR2.rowstride +
	    temp_buf->x * PR2.bytes;
	}
      else
	return FALSE;
    }
  else
    return FALSE;

  /*  swap the regions  */
  swap_region (&PR1, &PR2);

  /*  disk swap this temporary buffer -- in the interests of memory usage */
  temp_buf_swap (temp_buf);

  gimage_update (gimage, temp_buf->x, temp_buf->y,
		 temp_buf->width, temp_buf->height);

  return TRUE;
}


void
undo_free_image (state, data_ptr)
     int state;
     void * data_ptr;
{
  void ** data;

  data = (void **) data_ptr;

  /*  the 0th element of the data array is the temp buffer  */
  temp_buf_free ((TempBuf *) data[0]);
}


/******************************/
/*  Mask Undo functions  */

int
undo_push_mask (ID, mask_ptr)
     int ID;
     void * mask_ptr;
{
  Undo * new;
  MaskBuf * mask;
  GImage * gimage;
  int size;

  if (! (gimage = gimage_get_ID (ID)))
    return FALSE;

  mask = (MaskBuf *) mask_ptr;
  if (mask)
    size = mask->width * mask->height;
  else
    size = 0;

  if ((new = undo_push (size, MASK_UNDO)))
    {
      new->ID            = ID;
      new->data          = (void *) xmalloc (sizeof (MaskBuf *));
      new->pop_func      = undo_pop_mask;
      new->free_func     = undo_free_mask;

      /*  disk swap this mask buffer -- in the interests of memory usage */
      if (mask)
	temp_buf_swap (mask);

      *((MaskBuf **) new->data) = mask;
      return TRUE;
    }
  else
    return FALSE;
}


int
undo_pop_mask (ID, state, type, mask_ptr)
     int ID;
     int state;
     int type;
     void * mask_ptr;
{
  GImage * gimage;
  MaskBuf * mask, * gmask, * new;
  PixelRegion srcPR, destPR;
  int selection;
  int x1, y1, x2, y2;
  unsigned char empty = 0;

  if (! (gimage = gimage_get_ID (ID)))
    return FALSE;

  switch (state)
    {
    case VERIFY : return TRUE; break;
    case UNDO : break;  /*  modifying regions doesn't mess with the dirty flag  */
    case REDO : break;
    default : break;
    }

  mask = *((MaskBuf **) mask_ptr);

  /*  save current region  */
  gmask = gimage_mask (gimage);
  selection = gimage_mask_bounds (gimage, &x1, &y1, &x2, &y2);
  srcPR.bytes = 1;
  srcPR.w = (x2 - x1);
  srcPR.h = (y2 - y1);
  srcPR.rowstride = gmask->width;
  srcPR.data = mask_buf_data (gmask) + y1 * srcPR.rowstride + x1;

  if (selection)
    {
      new = mask_buf_new (srcPR.w, srcPR.h);
      new->x = x1;  new->y = y1;
      destPR.rowstride = new->width;
      destPR.data = mask_buf_data (new);

      copy_region (&srcPR, &destPR);
      color_region (&srcPR, &empty);
    }
  else
    new = NULL;

  /*  swap the new mask buffer data  */
  if (new)
    temp_buf_swap (new);

  /*  unswap the old mask buffer data  */
  if (mask)
    {
      temp_buf_unswap (mask);

      srcPR.w = mask->width;
      srcPR.h = mask->height;
      srcPR.data = mask_buf_data (mask);
      srcPR.rowstride = mask->width;
      /* ... */
      destPR.data = mask_buf_data (gmask) + mask->y * gmask->width + mask->x;
      destPR.rowstride = gmask->width;
      /* ... */
      copy_region (&srcPR, &destPR);

      mask_buf_free (mask);
    }

  *((MaskBuf **) mask_ptr) = new;

  /*  hide the current selection--for all views  */
  gdisplays_selection_visibility (gimage->ID, 0);

  /* invalidate the current bounds and boundary of the gregion */
  gimage->region->bounds_known = FALSE;
  gimage->region->boundary_known = FALSE;

  /*  recalculate the current selection--for all views  */
  gdisplays_selection_visibility (gimage->ID, 1);

  /*  if there is a "by color" selection dialog active
   *  for this gimage's mask, send it an update notice
   */
  if (gimage->by_color_select)
    by_color_select_initialize ((void *) gimage);

  return TRUE;
}


void
undo_free_mask (state, mask_ptr)
     int state;
     void * mask_ptr;
{
  MaskBuf * mask;

  mask = *((MaskBuf **) mask_ptr);
  if (mask)
    mask_buf_free (mask);
  xfree (mask_ptr);
}


/*********************************/
/*  FS swap Undo functions  */

int
undo_push_fs_swap (ID)
     int ID;
{
  Undo * new;
  int size;
  Layer * layer;
  GImage * gimage;

  if (! (gimage = gimage_get_ID (ID)))
    return FALSE;

  /*  increment the dirty flag for this gimage  */
  if (gimage->dirty < 0)
    gimage->dirty = 2;
  else
    gimage->dirty ++;

  layer = layer_get_ID (gimage->floating_sel);
  if (layer)
    size = layer_size (layer);

  if ((new = undo_push (size, FS_SWAP_UNDO)))
    {
      new->ID            = ID;
      new->data          = (void *) xmalloc (sizeof (int));
      new->pop_func      = undo_pop_fs_swap;
      new->free_func     = undo_free_fs_swap;

      *((int *) new->data) = gimage->floating_sel;

      return TRUE;
    }
  else
    return FALSE;
}


int
undo_pop_fs_swap (ID, state, type, layer_id_ptr)
     int ID;
     int state;
     int type;
     void * layer_id_ptr;
{
  GImage * gimage;
  Layer * layer, * old_layer;
  int layer_id;

  if (! (gimage = gimage_get_ID (ID)))
    return FALSE;

  switch (state)
    {
    case VERIFY: 
      return TRUE; break;
    case UNDO:
      gimage->dirty--; break;
    case REDO:
      gimage->dirty++; break;
    default : break;
    }

  layer_id = *((int *) layer_id_ptr);
  *((int *) layer_id_ptr) = gimage->floating_sel;

  layer = layer_get_ID (layer_id);
  old_layer = gimage_remove_layer (gimage, gimage->floating_sel);

  if (layer)
    {
      /*  add the floating layer to the gimage  */
      gimage_add_floating_layer (gimage, layer);

      /*  invalidate the gimage's boundary variables  */
      gimage->boundary_known = FALSE;

      /*  calculate the layer positions in gimage coords, not layer  */
      gdisplays_update_area (gimage->ID, layer->offset_x, layer->offset_y,
			     layer->width, layer->height);
    }

  /*  Only update the old_layer if the state is UNDO, otherwise
   *  this update is handled by the IMAGE_UNDO
   */
  if (old_layer)
    {
      /*  calculate the layer positions in gimage coords, not layer  */
      gdisplays_update_area (gimage->ID, old_layer->offset_x, old_layer->offset_y,
			     old_layer->width, old_layer->height);
    }

  return TRUE;
}


void
undo_free_fs_swap (state, layer_id_ptr)
     int state;
     void * layer_id_ptr;
{
  Layer * layer;

  layer = layer_get_ID (*((int *) layer_id_ptr));
  if (layer)
    layer_delete (layer);

  xfree (layer_id_ptr);
}


/*********************************/
/*  FS translate Undo functions  */

int
undo_push_fs_translate (ID)
     int ID;
{
  Undo * new;
  Layer * layer;
  GImage * gimage;
  int * offsets;

  if (! (gimage = gimage_get_ID (ID)))
    return FALSE;

  layer = layer_get_ID (gimage->floating_sel);

  if ((new = undo_push (8, FS_TRANSLATE_UNDO)))
    {
      new->ID            = ID;
      new->data          = (void *) xmalloc (sizeof (int) * 2);
      new->pop_func      = undo_pop_fs_translate;
      new->free_func     = undo_free_fs_translate;

      offsets = (int *) new->data;
      offsets[0] = layer->offset_x;
      offsets[1] = layer->offset_y;

      return TRUE;
    }
  else
    return FALSE;
}


int
undo_pop_fs_translate (ID, state, type, offsets_ptr)
     int ID;
     int state;
     int type;
     void * offsets_ptr;
{
  GImage * gimage;
  Layer * layer;
  int old_offsets[2];
  int * offsets;

  if (! (gimage = gimage_get_ID (ID)))
    return FALSE;

  switch (state)
    {
    case VERIFY : return TRUE; break;
    case UNDO : break;  /*  translating floating selections doesn't dirty  */
    case REDO : break;
    default : break;
    }

  offsets = (int *) offsets_ptr;
  layer = layer_get_ID (gimage->floating_sel);
  if (layer)
    {
      old_offsets[0] = layer->offset_x;
      old_offsets[1] = layer->offset_y;

      layer->offset_x = offsets[0];
      layer->offset_y = offsets[1];

      offsets[0] = old_offsets[0];
      offsets[1] = old_offsets[1];
      
      /*  update the affected regions  */
      gdisplays_update_area (ID, old_offsets[0], old_offsets[1],
			     layer->width, layer->height);
      gdisplays_update_area (ID, layer->offset_x, layer->offset_y,
			     layer->width, layer->height);
      
      return TRUE;
    }
  else
    return FALSE;
}


void
undo_free_fs_translate (state, offsets_ptr)
     int state;
     void * offsets_ptr;
{
  xfree (offsets_ptr);
}


/*********************************/
/*  FS mask Undo functions       */

int
undo_push_fs_mask (ID)
     int ID;
{
  Undo * new;
  GImage * gimage;
  MaskBuf * mask;
  int size;

  if (! (gimage = gimage_get_ID (ID)))
    return FALSE;

  /*  increment the dirty flag for this gimage  */
  if (gimage->dirty < 0)
    gimage->dirty = 2;
  else
    gimage->dirty ++;

  /*  If we cannot create a new temp buf--either because our parameters are
   *  degenerate or something else failed, simply return an unsuccessful push.
   */
  if (! (mask = mask_buf_load (NULL, gimage, 0, 0,
			       gimage_width (gimage),
			       gimage_height (gimage))))
    return FALSE;

  size = mask->width * mask->height * mask->bytes;
  if ((new = undo_push (size, FS_MASK_UNDO)))
    {
      new->ID            = ID;
      new->data          = (void *) mask;
      new->pop_func      = undo_pop_fs_mask;
      new->free_func     = undo_free_fs_mask;

      return TRUE;
    }
  else
    return FALSE;
}


int
undo_pop_fs_mask (ID, state, type, buf_ptr)
     int ID;
     int state;
     int type;
     void * buf_ptr;
{
  GImage * gimage;
  Layer * layer;
  MaskBuf * mask;
  unsigned char * m, * d;
  unsigned char swap;
  int alpha;
  int size;

  if (! (gimage = gimage_get_ID (ID)))
    return FALSE;

  switch (state)
    {
    case VERIFY: 
      return TRUE; break;
    case UNDO:
      gimage->dirty--; break;
    case REDO:
      gimage->dirty++; break;
    default : break;
    }

  mask = (MaskBuf *) buf_ptr;
  layer = layer_get_ID (gimage->floating_sel);
  if (layer)
    {
      /*  Swap the alpha values  */
      size = layer->width * layer->height;
      alpha = layer->bytes - 1;

      m = mask_buf_data (mask);
      d = layer_data (layer);

      while (size--)
	{
	  swap = *m;
	  *m = d[alpha];
	  d[alpha] = swap;

	  m++;
	  d += layer->bytes;
	}

      /* invalidate the current boundary of the gimage */
      gimage->boundary_known = FALSE;

      /*  update the gimage  */
      gimage_update (gimage, 0, 0, layer->width, layer->height);

      return TRUE;
    }
  else
    return FALSE;
}


void
undo_free_fs_mask (state, buf_ptr)
     int state;
     void * buf_ptr;
{
  mask_buf_free ((MaskBuf *) buf_ptr);
}


/***************************************/
/*  Layer displacement Undo functions  */

int
undo_push_layer_displace (ID, layer_ID)
     int ID;
     int layer_ID;
{
  Undo * new;
  Layer * layer;
  GImage * gimage;
  int * info;

  if (! (gimage = gimage_get_ID (ID)))
    return FALSE;

  layer = layer_get_ID (layer_ID);

  if ((new = undo_push (12, LAYER_DISPLACE_UNDO)))
    {
      new->ID            = ID;
      new->data          = (void *) xmalloc (sizeof (int) * 3);
      new->pop_func      = undo_pop_layer_displace;
      new->free_func     = undo_free_layer_displace;

      info = (int *) new->data;
      info[0] = layer_ID;
      info[1] = layer->offset_x;
      info[2] = layer->offset_y;

      return TRUE;
    }
  else
    return FALSE;
}


int
undo_pop_layer_displace (ID, state, type, info_ptr)
     int ID;
     int state;
     int type;
     void * info_ptr;
{
  GImage * gimage;
  Layer * layer;
  int old_offsets[2];
  int *info;

  if (! (gimage = gimage_get_ID (ID)))
    return FALSE;

  switch (state)
    {
    case VERIFY : return TRUE; break;
    case UNDO : break;  /*  translating floating selections doesn't dirty  */
    case REDO : break;
    default : break;
    }

  info = (int *) info_ptr;
  layer = layer_get_ID (info[0]);
  if (layer)
    {
      old_offsets[0] = layer->offset_x;
      old_offsets[1] = layer->offset_y;

      layer->offset_x = info[1];
      layer->offset_y = info[2];

      info[1] = old_offsets[0];
      info[2] = old_offsets[1];

      gdisplays_update_full (gimage->ID);

      return TRUE;
    }
  else
    return FALSE;
}


void
undo_free_layer_displace (state, info_ptr)
     int state;
     void * info_ptr;
{
  xfree (info_ptr);
}


/***************************************/
/*  Global edit Undo functions         */

int
undo_push_global_edit (buf_ptr)
     void * buf_ptr;
{
  Undo * new;
  TempBuf * buf;
  int size;

  buf = (TempBuf *) (TempBuf *) buf_ptr;
  if (buf)
    size = buf->width * buf->height * buf->bytes + 4;
  else
    size = 4;

  if ((new = undo_push (size, GLOBAL_EDIT_UNDO)))
    {
      new->ID            = 0;
      new->data          = xmalloc (sizeof (TempBuf *));
      new->pop_func      = undo_pop_global_edit;
      new->free_func     = undo_free_global_edit;

      /*  swap the buffer to disk in interests of memory conservation  */
      if (buf)
	temp_buf_swap (buf);

      *((TempBuf **) new->data) = buf;
      return TRUE;
    }
  else
    return FALSE;
}


int
undo_pop_global_edit (ID, state, type, buf_ptr)
     int ID;
     int state;
     int type;
     void * buf_ptr;
{
  TempBuf ** buf;
  TempBuf * temp;

  switch (state)
    {
    case VERIFY: return TRUE; break;
    case UNDO: break;
    case REDO: break;
    default : break;
    }

  buf = (TempBuf **) buf_ptr;
  if (*buf)
    temp_buf_unswap (*buf);
  temp = global_buf;
  global_buf = *buf;
  *buf = temp;
  if (temp)
    temp_buf_swap (temp);

  return TRUE;
}


void
undo_free_global_edit (state, buf_ptr)
     int state;
     void * buf_ptr;
{
  TempBuf ** buf;

  buf = (TempBuf **) buf_ptr;
  if (*buf)
    temp_buf_free (*buf);
}


/*********************************/
/*  Transform Undo               */

int
undo_push_transform (ID, tu_ptr)
     int ID;
     void * tu_ptr;
{
  Undo * new;
  int size;

  size = sizeof (TransformUndo);

  if ((new = undo_push (size, TRANSFORM_UNDO)))
    {
      new->ID            = ID;
      new->data          = tu_ptr;
      new->pop_func      = undo_pop_transform;
      new->free_func     = undo_free_transform;

      return TRUE;
    }
  else
    return FALSE;
}


int
undo_pop_transform (ID, state, type, tu_ptr)
     int ID;
     int state;
     int type;
     void * tu_ptr;
{
  TransformCore * tc;
  TransformUndo * tu;
  TempBuf * temp;
  double d;
  int i;

  /*  only pop if the active tool is the tool that pushed this undo  */
  if (ID != active_tool->ID)
    return TRUE;

  switch (state)
    {
    case VERIFY: 
      return TRUE; break;
    case UNDO:
      break;
    case REDO:
      break;
    default : break;
    }

  tc = (TransformCore *) active_tool->private;
  tu = (TransformUndo *) tu_ptr;

  /*  swap the transformation information arrays  */
  for (i = 0; i < TRAN_INFO_SIZE; i++)
    {
      d = tu->trans_info[i];
      tu->trans_info[i] = tc->trans_info[i];
      tc->trans_info[i] = d;
    }

  /*  if this is the first transform in a string, swap the
   *  original buffer--the source buffer for repeated transforms
   */
  if (tu->first)
    {
      temp = tu->original;
      tu->original = tc->original;
      tc->original = temp;
      
      /*  If we're re-implementing the first transform, reactivate tool  */
      if (state == REDO && tc->original)
	{
	  active_tool->state = ACTIVE;
	  draw_core_resume (tc->core, active_tool);
	}
    }

  return TRUE;
}


void
undo_free_transform (state, tu_ptr)
     int state;
     void * tu_ptr;
{
  TransformUndo * tu;

  tu = (TransformUndo *) tu_ptr;
  if (tu->original)
    temp_buf_free (tu->original);
  xfree (tu);
}


/*********************************/
/*  Paint Undo                   */

int
undo_push_paint (ID, pu_ptr)
     int ID;
     void * pu_ptr;
{
  Undo * new;
  int size;

  size = sizeof (PaintUndo);

  if ((new = undo_push (size, PAINT_UNDO)))
    {
      new->ID            = ID;
      new->data          = pu_ptr;
      new->pop_func      = undo_pop_paint;
      new->free_func     = undo_free_paint;

      return TRUE;
    }
  else
    return FALSE;
}


int
undo_pop_paint (ID, state, type, pu_ptr)
     int ID;
     int state;
     int type;
     void * pu_ptr;
{
  PaintCore * pc;
  PaintUndo * pu;
  double tmp;

  /*  only pop if the active tool is the tool that pushed this undo  */
  if (ID != active_tool->ID)
    return TRUE;

  switch (state)
    {
    case VERIFY: 
      return TRUE; break;
    case UNDO:
      break;
    case REDO:
      break;
    default : break;
    }

  pc = (PaintCore *) active_tool->private;
  pu = (PaintUndo *) pu_ptr;

  /*  swap the paint core information  */
  tmp = pc->lastx;
  pc->lastx = pu->lastx;
  pu->lastx = tmp;

  tmp = pc->lasty;
  pc->lasty = pu->lasty;
  pu->lasty = tmp;

  return TRUE;
}


void
undo_free_paint (state, pu_ptr)
     int state;
     void * pu_ptr;
{
  PaintUndo * pu;

  pu = (PaintUndo *) pu_ptr;
  xfree (pu);
}
