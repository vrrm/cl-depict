/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "autodialog.h"
#include "brushes.h"
#include "errors.h"
#include "gdisplay.h"
#include "paint_funcs.h"
#include "paint_core.h"
#include "palette.h"
#include "airbrush.h"
#include "selection.h"
#include "tools.h"


/*  The maximum amount of pressure that can be exerted  */
#define             MAX_PRESSURE  0.075

#define             OFF           0
#define             ON            1

typedef struct _AirbrushOptions AirbrushOptions;
struct _AirbrushOptions
{
  double rate;
  double pressure;

  GtkObserver rate_observer;
  GtkObserver pressure_observer;
};


/*  forward function declarations  */
static void         airbrush_motion      (Tool *);
static gint         airbrush_time_out    (gpointer);


/*  local variables  */
static gint         timer;                 /*  timer for successive paint applications  */
static int          timer_state = OFF;     /*  state of airbrush tool  */
static AirbrushOptions *airbrush_options = NULL;

static gint
airbrush_scale_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataAdjustment *adj_data;
  double *scale_val;

  scale_val = (double *) observer->user_data;
  adj_data = (GtkDataAdjustment *) data;
  *scale_val = adj_data->value;

  return FALSE;
}

static void
airbrush_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}

AirbrushOptions *
create_airbrush_options ()
{
  AirbrushOptions *options;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *rate_scale;
  GtkData *rate_scale_data;
  GtkWidget *pressure_scale;
  GtkData *pressure_scale_data;

  /*  the new options structure  */
  options = (AirbrushOptions *) xmalloc (sizeof (AirbrushOptions));
  options->rate = 25.0;
  options->pressure = 10.0;

  /*  the main vbox  */
  vbox = gtk_vbox_new (FALSE, 5);
  
  /*  the main label  */
  label = gtk_label_new ("Airbrush Options");
  gtk_box_pack (vbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  /*  the rate scale  */
  hbox = gtk_hbox_new (FALSE, 4);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  
  label = gtk_label_new ("Rate");
  gtk_box_pack (hbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  rate_scale_data = gtk_data_adjustment_new (25.0, 0.0, 100.0, 1.0, 1.0, 1.0);
  rate_scale = gtk_hscale_new ((GtkDataAdjustment *) rate_scale_data);
  gtk_box_pack (hbox, rate_scale, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_scale_set_value_pos (rate_scale, GTK_POS_TOP);
  options->rate_observer.update = airbrush_scale_update;
  options->rate_observer.disconnect = airbrush_disconnect;
  options->rate_observer.user_data = &options->rate;
  gtk_data_attach (rate_scale_data, &options->rate_observer);
  gtk_widget_show (rate_scale);
  gtk_widget_show (hbox);

  /*  the pressure scale  */
  hbox = gtk_hbox_new (FALSE, 4);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  
  label = gtk_label_new ("Pressure");
  gtk_box_pack (hbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  pressure_scale_data = gtk_data_adjustment_new (10.0, 0.0, 100.0, 1.0, 1.0, 1.0);
  pressure_scale = gtk_hscale_new ((GtkDataAdjustment *) pressure_scale_data);
  gtk_box_pack (hbox, pressure_scale, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_scale_set_value_pos (pressure_scale, GTK_POS_TOP);
  options->pressure_observer.update = airbrush_scale_update;
  options->pressure_observer.disconnect = airbrush_disconnect;
  options->pressure_observer.user_data = &options->pressure;
  gtk_data_attach (pressure_scale_data, &options->pressure_observer);
  gtk_widget_show (pressure_scale);
  gtk_widget_show (hbox);

  /*  Register this selection options widget with the main tools options dialog  */
  tools_register_options (AIRBRUSH, vbox);

  return options;
}

void *
airbrush_paint_func (tool, state)
     Tool * tool;
     int state;
{
  GDisplay * gdisp;
  GBrushP brush;
  PaintCore * paint_core;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  paint_core = (PaintCore *) tool->private;
  brush = get_active_brush ();

  switch (state)
    {
    case INIT_PAINT :
      timer_state = OFF;
      break;

    case MOTION_PAINT :
      if (timer_state == ON)
	gtk_timeout_remove (timer);
      timer_state = OFF;

      airbrush_motion (tool);

      if (airbrush_options->rate != 0.0)
	{
	  timer = gtk_timeout_add ((10000 / airbrush_options->rate),
				   airbrush_time_out,
				   (gpointer) tool);
	  timer_state = ON;
	}
      break;

    case FINISH_PAINT :
      if (timer_state == ON)
	gtk_timeout_remove (timer);
      timer_state = OFF;
      break;

    default :
      break;
    }

  return NULL;
}


Tool *
tools_new_airbrush ()
{
  Tool * tool;
  PaintCore * private;

  if (! airbrush_options)
    airbrush_options = create_airbrush_options ();

  tool = paint_core_new (AIRBRUSH);

  private = (PaintCore *) tool->private;
  private->paint_func = airbrush_paint_func;

  return tool;
}


void
tools_free_airbrush (tool)
     Tool * tool;
{
  paint_core_free (tool);
  
  if (timer_state == ON)
    gtk_timeout_remove (timer);
  timer_state = OFF;
}


static gint
airbrush_time_out (client_data)
     gpointer client_data;
{
  Tool * tool;

  tool = (Tool *) client_data;

  /*  service the timer  */
  airbrush_motion (tool);
  gdisplays_flush ();

  /*  restart the timer  */
  if (airbrush_options->rate != 0.0)
    return TRUE;
  else
    return FALSE;
}


static void
airbrush_motion (tool)
     Tool * tool;
{
  GDisplay * gdisp;
  PaintCore * paint_core;
  TempBuf * area;
  unsigned char col[MAX_CHANNELS];

  gdisp = (GDisplay *) tool->gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  gimage_get_foreground (gdisp->gimage, col);

  if (! (area = paint_core_get_paint_area (tool, 0)))
    return;

  /*  color the pixels  */
  col[area->bytes - 1] = OPAQUE;

  /*  color the pixels  */
  color_pixels (temp_buf_data (area), col,
		area->width * area->height, area->bytes);

  /*  paste the newly painted area to the image  */
  paint_core_paste_canvas (tool,
			   (int) (airbrush_options->pressure * 2.55),
			   (int) (get_brush_opacity () * 255),
			   get_brush_paint_mode (),
			   SOFT, CONSTANT);
}


