/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "appenv.h"
#include "colormaps.h"
#include "commands.h"
#include "fileops.h"
#include "menus.h"
#include "scale.h"
#include "tools.h"

/*  Main menus  */
MenuItem main_file_items[] =
{
  { "New",  'N',   GDK_CONTROL_MASK,
    file_new_cmd_callback, (gpointer) 0, NULL, NULL },
  { "Open",  'O',   GDK_CONTROL_MASK,  
    file_open_cmd_callback, NULL, NULL, NULL },
  { "Preferences...",  0,    0,
    file_pref_cmd_callback, NULL, NULL, NULL },
  { "-", 0, 0, NULL, NULL, NULL, NULL },
  { "Quit",  'Q',   GDK_CONTROL_MASK,
    file_quit_cmd_callback, NULL, NULL, NULL },
  { NULL, 0, 0, NULL, NULL, NULL, NULL },
};

MenuItem main_menu[] =
{
  { "File", 0, 0, NULL, NULL, main_file_items, NULL },
  { NULL, 0, 0, NULL, NULL, NULL, NULL },
};

/*  Image menu data  */

MenuItem image_file_items[] =
{
  { "New",  'N',   GDK_CONTROL_MASK,
    file_new_cmd_callback, (gpointer) 1, NULL, NULL },
  { "Open",  'O',   GDK_CONTROL_MASK, 
    file_open_cmd_callback, NULL, NULL, NULL },
  { "Save",  'S',   GDK_CONTROL_MASK,
    file_save_cmd_callback, NULL, NULL, NULL },
  { "Preferences...",  0,  0,
    file_pref_cmd_callback, NULL, NULL, NULL },
  { "-", 0, 0, NULL, NULL,  NULL, NULL },
  { "Quit",  'Q',  GDK_CONTROL_MASK,
    file_quit_cmd_callback, NULL, NULL, NULL },
  { NULL, 0, 0, NULL, NULL, NULL, NULL },
};

MenuItem image_edit_items[] =
{
  { "Cut",  'X',  GDK_CONTROL_MASK,
    edit_cut_cmd_callback, NULL, NULL, NULL },
  { "Copy",  'C',  GDK_CONTROL_MASK,
    edit_copy_cmd_callback, NULL, NULL, NULL },
  { "Paste",  'V',  GDK_CONTROL_MASK,
    edit_paste_cmd_callback, NULL, NULL, NULL },
  { "Paste Into",  0,  0,
    edit_paste_into_cmd_callback, NULL, NULL, NULL },
  { "Clear",  'K',  GDK_CONTROL_MASK,
    edit_clear_cmd_callback, NULL, NULL, NULL },
  { "Undo",  'Z',  GDK_CONTROL_MASK,
    edit_undo_cmd_callback, NULL, NULL, NULL },
  { "Redo",  'R',  GDK_CONTROL_MASK,
    edit_redo_cmd_callback, NULL, NULL, NULL },
  { "-", 0, 0, NULL, NULL, NULL, NULL },
  { "Cut Named",  'X',  GDK_CONTROL_MASK | GDK_SHIFT_MASK,
    edit_named_cut_cmd_callback, NULL, NULL, NULL },
  { "Copy Named",  'C',  GDK_CONTROL_MASK | GDK_SHIFT_MASK,
    edit_named_copy_cmd_callback, NULL, NULL, NULL },
  { "Paste Named",  'V',   GDK_CONTROL_MASK | GDK_SHIFT_MASK,
    edit_named_paste_cmd_callback, NULL, NULL, NULL },
  { NULL, 0, 0, NULL, NULL, NULL, NULL },
};

MenuItem image_select_items[] =
{
  { "Toggle",  'T',  GDK_CONTROL_MASK,
    select_toggle_cmd_callback, NULL, NULL, NULL },
  { "Invert",  'I',  GDK_CONTROL_MASK,
    select_invert_cmd_callback, NULL, NULL, NULL },
  { "All",  'A',  GDK_CONTROL_MASK,
    select_all_cmd_callback, NULL, NULL, NULL },
  { "None",  'A',  GDK_CONTROL_MASK | GDK_SHIFT_MASK,
    select_none_cmd_callback, NULL, NULL, NULL },
  { "Float",  'L',  GDK_CONTROL_MASK | GDK_SHIFT_MASK,
    select_float_cmd_callback, NULL, NULL, NULL },
  { "Anchor",  'H',  GDK_CONTROL_MASK | GDK_SHIFT_MASK,
    select_anchor_cmd_callback, NULL, NULL, NULL },
  { "Sharpen",  'S',  GDK_CONTROL_MASK | GDK_SHIFT_MASK,
    select_sharpen_cmd_callback, NULL, NULL, NULL },
  { "Border",  'B',  GDK_CONTROL_MASK | GDK_SHIFT_MASK,
    select_border_cmd_callback, NULL, NULL, NULL },
  { "Feather",  'F',  GDK_CONTROL_MASK | GDK_SHIFT_MASK,
    select_feather_cmd_callback, NULL, NULL, NULL },
  { "By Color...",  0,  0,
    select_by_color_cmd_callback, NULL, NULL, NULL },
  { NULL, 0, 0, NULL, NULL, NULL, NULL },
};

MenuItem image_view_items[] =
{
  { "Zoom In",  '=',  0,
    view_zoomin_cmd_callback, NULL, NULL, NULL },
  { "Zoom Out",  '-',  0,
    view_zoomout_cmd_callback, NULL, NULL, NULL },
  { "Window Info...",  'I',  GDK_CONTROL_MASK | GDK_SHIFT_MASK,
    view_window_info_cmd_callback, NULL, NULL, NULL },
  { "-", 0, 0, NULL, NULL, NULL, NULL },
  { "New View",  0,  0,
    view_new_view_cmd_callback, NULL, NULL, NULL },
  { "Shrink Wrap",  'E',  GDK_CONTROL_MASK,
    view_shrink_wrap_cmd_callback, NULL, NULL, NULL },
  { "Close Window",  'W',  GDK_CONTROL_MASK,
    view_close_window_cmd_callback, NULL, NULL, NULL },
  { NULL, 0, 0, NULL, NULL, NULL, NULL },
};

MenuItem image_tools_items[] =
{
  { "Rect Select",  'R',  0,
    tools_select_cmd_callback, (gpointer)RECT_SELECT, NULL, NULL },
  { "Ellipse Select",  'E',  0,
    tools_select_cmd_callback, (gpointer)ELLIPSE_SELECT, NULL, NULL },
  { "Free Select",  'F',  0,
    tools_select_cmd_callback, (gpointer)FREE_SELECT, NULL, NULL },
  { "Fuzzy Select",  'Z',  0,
    tools_select_cmd_callback, (gpointer)FUZZY_SELECT, NULL, NULL },
  { "Bezier Select",  'B',  0,
    tools_select_cmd_callback, (gpointer)BEZIER_SELECT, NULL, NULL },
  { "Intelligent Scissors", 'I',  0,
    tools_select_cmd_callback, (gpointer)ISCISSORS, NULL, NULL },
  { "Move",  'M',  0,
    tools_select_cmd_callback, (gpointer)MOVE, NULL, NULL },
  { "Magnify",  'M',  GDK_SHIFT_MASK,
    tools_select_cmd_callback, (gpointer)MAGNIFY, NULL, NULL },
  { "Crop",  'C',  GDK_SHIFT_MASK,
    tools_select_cmd_callback, (gpointer)CROP, NULL, NULL },
  { "Transform",  'T',  GDK_SHIFT_MASK,
    tools_select_cmd_callback, (gpointer)ROTATE, NULL, NULL },
  { "Flip",  'F',  GDK_SHIFT_MASK,
    tools_select_cmd_callback, (gpointer)FLIP_HORZ, NULL, NULL },
  { "Text",  'T',  0, 
    tools_select_cmd_callback, (gpointer)TEXT, NULL, NULL },
  { "Color Picker",  'O',  0,
    tools_select_cmd_callback, (gpointer)COLOR_PICKER, NULL, NULL },
  { "Bucket Fill",  'B',  GDK_SHIFT_MASK,
    tools_select_cmd_callback, (gpointer)BUCKET_FILL, NULL, NULL },
  { "Blend",  'L',  0, 
    tools_select_cmd_callback, (gpointer)BLEND, NULL, NULL },
  { "Paintbrush",  'P',  0,
    tools_select_cmd_callback, (gpointer)PAINTBRUSH, NULL, NULL },
  { "Pencil",  'P',  GDK_SHIFT_MASK,
    tools_select_cmd_callback, (gpointer)PENCIL, NULL, NULL },
  { "Eraser",  'E',  GDK_SHIFT_MASK,
    tools_select_cmd_callback, (gpointer)ERASER, NULL, NULL },
  { "Airbrush",  'A',  0,
    tools_select_cmd_callback, (gpointer)AIRBRUSH, NULL, NULL },
  { "Clone",  'C',  0,
    tools_select_cmd_callback, (gpointer)CLONE, NULL, NULL },
  { "Convolve",  'V',  0,
    tools_select_cmd_callback, (gpointer)CONVOLVE, NULL, NULL },
  { NULL, 0, 0, NULL, NULL, NULL, NULL },
};

MenuItem image_dialogs_items[] =
{
  { "Brushes...",   'B',  GDK_CONTROL_MASK,
    dialogs_brushes_cmd_callback, NULL, NULL, NULL },
  { "Patterns...",  'P',  GDK_SHIFT_MASK | GDK_CONTROL_MASK,
    dialogs_patterns_cmd_callback, NULL, NULL, NULL },
  { "Palette...",   'P',  GDK_CONTROL_MASK,
    dialogs_palette_cmd_callback, NULL, NULL, NULL },
  { "Layers...",   'L',  GDK_CONTROL_MASK,
    dialogs_layers_cmd_callback, NULL, NULL, NULL },
  { "Tool Options...",   'T',   GDK_SHIFT_MASK | GDK_CONTROL_MASK,
    dialogs_tools_options_cmd_callback, NULL, NULL, NULL },
  { NULL, 0, 0, NULL, NULL, NULL, NULL },
};

MenuItem image_menu[] =
{
  { "File", 0, 0, NULL, NULL, image_file_items, NULL },
  { "Edit", 0, 0, NULL, NULL, image_edit_items, NULL },
  { "Select", 0, 0, NULL, NULL, image_select_items, NULL },
  { "View", 0, 0, NULL, NULL, image_view_items, NULL },
  { "Tools", 0, 0, NULL, NULL, image_tools_items, NULL },
  { "Filters", 0, 0, NULL, NULL, NULL, NULL },
  { "Dialogs", 0, 0, NULL, NULL, image_dialogs_items, NULL },
  { NULL, 0, 0, NULL, NULL, NULL, NULL },
};


/**************  USE THESE FUNCTIONS TO CONFIGURE MENUS  **************/


MenuItem *
get_main_menu ()
{
  return main_menu;
}

MenuItem *
get_image_menu ()
{
  return image_menu;
}

void
set_filter_menu (menu)
     MenuItem *menu;
{
  image_menu[5].subitems = menu;
}


