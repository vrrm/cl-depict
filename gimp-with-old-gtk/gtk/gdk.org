


* GDK 
  
  GDK is middleware that serves as a bridge between the toolkit and
  xlib.  It also uses xlib as a low level library for list and timer
  allocation

** xlib interaction

   Xlib is organized along a hierarchical scheme.  Windows form the
   basic building blocks of that scheme, being related to each other as
   parents or children to from a tree with one window that serves as
   the root.

   glib initialized the X Windows system and attech the root node.

** widgetness 

   the gtk is widget oriented.  One question regarding a widget is how
   they map to Windows.  For example, can a widget be made up of
   multiple windows whose ultimate parent is not contained in the
   widget?  Can they be made up of multiple windows at all?

   At least one widget, the label, doesn't create a window at all.

** Input Protocal

   xdk is simple.  Widgets are the lowest level item that can receive
   events.  Widgets can play two roles in event propigation, they can
   be 'grabbing' widget, i.e. the widget that contains the window that
   has the mouse grabbed. Or they can be an event widget, i.e. the
   widget to which the event was the target.  All widgets have a
   reference to the imput stream and can read token from the stream.
   A lexer is used to generate tokens specific to the widgets
   context.  If the widget is unable to use anything on the stream,
   either because 

   1. It's not listening.
   2. The tokens on the stream don't conform to anything the wideg is
      interested in.  I.e. match to the lex table but not the command
      tabel of the widget.
   3. it has not recieved enough tokens to process what's on the
      string, i.e. no match yet for the lex table.


   The the widet put back everything it's read from the stream and
   raises a process event to its parent with the stream. 


   - If what's on the stream can't match anything in the lexical
     table. Then elements on the stream are dropped until a partial or
     complete lexical match can be found.  In this way garbage input
     is remove automatically.

   - If what's on the lexical stream can't fully or partially match
     any command for the current widge or are parent widgets, then the
     lexical stream is cleared, in this way garbage input is removed
     automatically.

   - The raw stream and the lexical stream are shared among all
     widgets, but the command stream is specific to a widget.

   - It is the individual widget's responsibilite to clear the command
     stream. 
