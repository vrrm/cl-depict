(in-package :cl-depict)


(defmacro definterface (name params &rest body)
  (if (stringp (car body))
      (if (consp (cadr body))
	  `(progn
	     (defun ,name ,params ,(car body) ,@(cddr body))
	     ,(cadr body))
	  `(progn
	     (defun ,name ,params ,@body)
	     nil))))
