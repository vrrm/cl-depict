(in-package :cl-depict)

(defclass gtk-window (#-scl fundamental-character-output-stream
                                 #+scl ext:character-output-stream)
  ((container    :initarg :mark
                 :initform nil
                 :accessor random-typeout-stream-mark
                 :documentation "The buffer point of the associated buffer.")
   (title       :initarg :window
                 :initform nil
                 :accessor random-typeout-stream-window
                 :documentation "The hemlock window all this shit is in.")
   (type    :initarg :more-mark
                 :initform nil
                 :accessor random-typeout-stream-more-mark
                 :documentation "The mark that is not displayed when we need to more.")
   (child    :initarg :no-prompt
                 :initform nil
                 :accessor random-typeout-stream-no-prompt
                 :documentation "T when we want to exit, still collecting output.")
   (focus-widget :initarg :first-more-p
                 :initform t
                 :accessor random-typeout-stream-first-more-p
                 :documentation "T until the first time we more. Nil after.")
   (default-widget :initarg :first-more-p
                 :initform t
                 :accessor random-typeout-stream-first-more-p
                 :documentation "T until the first time we more. Nil after.")
   (accelerator-table :initarg :first-more-p
                 :initform t
                 :accessor random-typeout-stream-first-more-p
                 :documentation "T until the first time we more. Nil after.")
   (resize :initarg :first-more-p
                 :initform t
                 :accessor random-typeout-stream-first-more-p
                 :documentation "T until the first time we more. Nil after.")
   (need-resize :documentation "whether line buffered") ))
