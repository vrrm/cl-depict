(in-package :cl-depict)

(defvar *display* nil)
(defvar *screen* nil )
(defvar *root-window* nil "AKA screen-root")
(defvar *event-channel* nil)
(defvar *gc* nil)

(defun create-gc ()
  )

(defvar *display-connection-state* nil
  "Valid states: nil (uninitialized), :initialized (initialized).
   TODO: future states might be initializing, uninizializing. ")

(defun display-exit ()
  (unless *display-connection-state*
    (error (concatenate 'string
			"exit may only be called once, unless init() was called"
			"previously to start up a new session.")))
  (when *top-window*
    (xlib:destroy-window *top-window*)
    (setf *top-window* nil))
  (when *display*
    (ignore-errors
      (xlib:close-display *display*))
    (setf *display* nil))
  (setf *root-window* nil)
  (setf *screen* nil)
  (setf *display-connection-state* nil))

(defun display-init (&optional (host ""))
  (when (or *display-connection-state* *display* *screen* *root-window*)
    (error (concatenate 'string
			"init may only be called once, unless exit() was called "
			"previously to shutdown a previous session.")))
  (setf *display* (xlib:open-display host))
  (setf *screen* (first (xlib:display-roots *display*)))
  (xdk-window-init)
  (setf *root-window*
	(make-instance 'window :window-of (xlib:screen-root *screen*)))
  (setf *event-channel* (init-event-handler *display*))
  (setf *display-connection-state* :initialized))

(defmacro definterface (name params &rest body)
  (if (stringp (car body))
      (if (consp (cadr body))
	  `(progn
	     (defun ,name ,params ,(car body) ,@(cddr body))
	     ,(cadr body))
	  `(progn
	     (defun ,name ,params ,@body)
	     nil))))

;; gint gdk-events-pending (void)
;; gint gdk-event-get ((GdkEvent event))
;; void gdk-event-put ((GdkEvent event))
;; void gdk-events-record (char filename)
;; void gdk-events-playback (char filename)
;; void gdk-events-stop (void)

;; void gdk-set-debug-level (int level)
;; void gdk-set-show-events (int show-events)

;; guint32 gdk-time-get (void)
;; guint32 gdk-timer-get (void)
;; void gdk-timer-set (guint32 milliseconds)

;; gint gdk-input-add (gint source,
;; 		 GdkInputCondition condition,
;; 		 GdkInputFunction function,
;; 		 gpointer data)
;; void gdk-input-remove (gint tag)

;; gint gdk-pointer-grab (GdkWindow window,
;; 			 gint owner-events,
;; 			 GdkEventMask event-mask,
;; 			 GdkWindow confine-to,
;; 			 GdkCursor cursor,
;; 			 guint32 time)
;; void gdk-pointer-ungrab (guint32 time)

;; gint gdk-screen-width (void)
;; gint gdk-screen-height (void)

;; void gdk-flush (void)


;; / Visuals
;;  /
;; gint gdk-visual-get-best-depth (void)
;; GdkVisualType gdk-visual-get-best-type (void)
;; GdkVisual gdk-visual-get-system (void)
;; GdkVisual gdk-visual-get-best (void)
;; GdkVisual gdk-visual-get-best-with-depth (gint  depth)
;; GdkVisual gdk-visual-get-best-with-type (GdkVisualType visual-type)
;; GdkVisual gdk-visual-get-best-with-both (gint  depth,
;; 					 GdkVisualType visual-type)

;; void gdk-query-depths (gint  depths gint count)
;; void gdk-query-visual-types (GdkVisualType visual-types gint count)
;; void gdk-query-visuals (GdkVisual visuals gint count)


;; / Cursors
;;  /
;; GdkCursor gdk-cursor-new (GdkCursorType cursor-type)
;; void gdk-cursor-destroy (GdkCursor cursor)


;; / GCs
;;  /
;; GdkGC gdk-gc-new (GdkWindow window)
;; void gdk-gc-destroy (GdkGC gc)
;; void gdk-gc-set-foreground (GdkGC gc GdkColor color)
;; void gdk-gc-set-background (GdkGC gc GdkColor color)
;; void gdk-gc-set-font (GdkGC gc GdkFont font)
;; void gdk-gc-set-function (GdkGC gc GdkFunction function)
;; void gdk-gc-set-fill (GdkGC gc GdkFill  fill)
;; void gdk-gc-set-tile (GdkGC gc GdkPixmap tile)
;; void gdk-gc-set-stipple (GdkGC gc GdkPixmap stipple)
;; void gdk-gc-set-subwindow (GdkGC gc GdkSubwindowMode mode)
;; void gdk-gc-set-exposures (GdkGC gc gint exposures)
;; void gdk-gc-set-line-attributes (GdkGC gc,
;; 				 gint line-width,
;; 				 GdkLineStyle line-style,
;; 				 GdkCapStyle cap-style,
;; 				 GdkJoinStyle join-style)


;; / Pixmaps
;;  /
;; GdkPixmap gdk-pixmap-new (GdkWindow window,
;; 					gint width,
;; 					gint height,
;; 					gint depth)
;; GdkPixmap gdk-bitmap-create-from-data (GdkWindow window,
;; 					gchar data,
;; 					gint width,
;; 					gint height)
;; void gdk-pixmap-destroy (GdkPixmap pixmap)



;; / Images
;;  /
;; GdkImage gdk-image-new (GdkImageType type GdkVisual visual gint width gint height)
;; GdkImage gdk-image-get (GdkWindow window gint x gint y gint width gint height)
;; void gdk-image-put-pixel (GdkImage image gint x gint y guint32 pixel)
;; guint32 gdk-image-get-pixel (GdkImage image gint x gint y)
;; void gdk-image-destroy (GdkImage image)


;; / Color
;;  /
;; GdkColormap gdk-colormap-new (GdkVisual visual,
;; 				 gint allocate)
;; void gdk-colormap-destroy (GdkColormap colormap)

;; GdkColormap gdk-colormap-get-system (void)
;; gint gdk-colormap-get-system-size (void)

;; void gdk-colormap-change (GdkColormap colormapgint  ncolors)
;; void gdk-colors-store (GdkColormap colormapGdkColor colorsgint  ncolors)
;; gint gdk-colors-alloc (GdkColormap colormapgint  contiguousgulong planesgint  nplanesgulong pixelsgint  npixels)
;; gint gdk-color-white (GdkColormap colormapGdkColor color)
;; gint gdk-color-black (GdkColormap colormapGdkColor color)
;; gint gdk-color-parse (gchar specGdkColor color)
;; gint gdk-color-alloc (GdkColormap colormapGdkColor color)
;; gint gdk-color-change (GdkColormap colormapGdkColor color)


;; / Fonts
;;  /
;; GdkFont gdk-font-load (gchar font-name)
;; void gdk-font-free (GdkFont font)
;; gint gdk-string-width (GdkFont font gchar string)
;; gint gdk-text-width (GdkFont font gchar text gint text-length)
;; gint gdk-char-width (GdkFont font gchar character)


;; / Drawing
;;  /
;; void gdk-draw-move (GdkGC gcgint xgint y)
;; void gdk-draw-move-rel (GdkGC gcgint dxgint dy)
;; void gdk-draw-line (GdkWindow windowGdkGC gcgint x1gint y1gint x2gint y2)
;; void gdk-draw-line-rel (GdkWindow windowGdkGC gcgint dxgint dy)
;; void gdk-draw-rectangle (GdkWindow windowGdkGC gcgint filledgint xgint ygint widthgint height)
;; void gdk-draw-arc (GdkWindow windowGdkGC gcgint filledgint xgint ygint widthgint heightgint angle1gint angle2)
;; void gdk-draw-polygon (GdkWindow windowGdkGC gcgint filledGdkPoint pointsgint npoints)
;; void gdk-draw-string (GdkWindow windowGdkGC gcgint xgint ygchar string)
;; void gdk-draw-text (GdkWindow windowGdkGC gcgint xgint ygchar textgint text-length)
;; void gdk-draw-pixmap (GdkWindow windowGdkGC gcGdkPixmap pixmapgint xsrcgint ysrcgint xdestgint ydestgint widthgint height)
;; void gdk-draw-image (GdkWindow windowGdkGC gcGdkImage imagegint xsrcgint ysrcgint xdestgint ydestgint widthgint height)
;; void gdk-draw-points (GdkWindow windowGdkGC gcGdkPoint pointsgint npoints)
;; void gdk-draw-segments (GdkWindow windowGdkGC gcGdkSegment segsgint nsegs)

;; / Rectangle utilities
;;  /
;; gint gdk-rectangle-intersect (GdkRectangle src1 GdkRectangle src2 GdkRectangle dest)


;; #ifdef --cplusplus
;; )
;; #endif / --cplusplus /


;; #endif / --GDK-H-- /










;; / GDK - The General Drawing Kit (written for the GIMP)
;;  Copyright (C) 1995 Peter Mattis

;;  This program is free software you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published by
;;  the Free Software Foundation either version 2 of the License, or
;;  (at your option) any later version.

;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program if not, write to the Free Software
;;  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;  /
;; #ifndef --GDK-TYPES-H--
;; #define --GDK-TYPES-H--


;; #include <X11/keysym.h>


;; / GDK uses "glib". (And so does GTK).
;;  Also include the config file so that anybody can
;;  access the config information if needed.
;;  /
;; #include "glib.h"
;; #include "config.h"


;; / Type definitions for the basic structures.
;;  /

;; typedef struct -GdkColor GdkColor
;; typedef struct -GdkColormap GdkColormap
;; typedef struct -GdkVisual GdkVisual
;; typedef struct -GdkWindowAttr GdkWindowAttr
;; typedef struct -GdkWindow GdkWindow
;; typedef struct -GdkWindow GdkPixmap
;; typedef struct -GdkImage GdkImage
;; typedef struct -GdkGC GdkGC
;; typedef struct -GdkPoint GdkPoint
;; typedef struct -GdkRectangle GdkRectangle
;; typedef struct -GdkSegment GdkSegment
;; typedef struct -GdkFont  GdkFont
;; typedef struct -GdkCursor GdkCursor

;; typedef struct -GdkEventAny GdkEventAny
;; typedef struct -GdkEventExpose GdkEventExpose
;; typedef struct -GdkEventMotion GdkEventMotion
;; typedef struct -GdkEventButton GdkEventButton
;; typedef struct -GdkEventKey GdkEventKey
;; typedef struct -GdkEventFocus GdkEventFocus
;; typedef struct -GdkEventCrossing GdkEventCrossing
;; typedef struct -GdkEventResize GdkEventResize
;; typedef union -GdkEvent GdkEvent


;; / Types of windows.
;;  Root: There is only 1 root window and it is initialized
;;  at startup. Creating a window of type GDK-WINDOW-ROOT
;;  is an error.
;;  Toplevel: Windows which interact with the window manager.
;;  Child: Windows which are children of some other type of window.
;;   (Any other type of window). Most windows are child windows.
;;  Dialog: A special kind of toplevel window which interacts with
;;   the window manager slightly differently than a regular
;;   toplevel window. Dialog windows should be used for any
;;   transient window.
;;  Pixmap: Pixmaps are really just another kind of window which
;;   doesn't actually appear on the screen. It can't have
;;   children, either and is really just a convenience so
;;   that the drawing functions can work on both windows
;;   and pixmaps transparently. (ie. You shouldn't pass a
;;   pixmap to any procedure which accepts a window with the
;;   exception of the drawing functions).
;;  /
;; typedef enum
;; (
;;  GDK-WINDOW-ROOT,
;;  GDK-WINDOW-TOPLEVEL,
;;  GDK-WINDOW-CHILD,
;;  GDK-WINDOW-DIALOG,
;;  GDK-WINDOW-TEMP,
;;  GDK-WINDOW-PIXMAP
;; ) GdkWindowType

;; / Classes of windows.
;;  InputOutput: Almost every window should be of this type. Such windows
;;  receive events and are also displayed on screen.
;;  InputOnly: Used only in special circumstances when events need to be
;;  stolen from another window or windows. Input only windows
;;  have no visible output, so they are handy for placing over
;;  top of a group of windows in order to grab the events (or
;;  filter the events) from those windows.
;;  /
;; typedef enum
;; (
;;  GDK-INPUT-OUTPUT,
;;  GDK-INPUT-ONLY
;; ) GdkWindowClass

;; / Types of images.
;;  Normal: Normal X image type. These are slow as they involve passing
;;   the entire image through the X connection each time a draw
;;   request is required.
;;  Shared: Shared memory X image type. These are fast as the X server
;;   and the program actually use the same piece of memory. They
;;   should be used with care though as there is the possibility
;;   for both the X server and the program to be reading/writing
;;   the image simultaneously and producing undesired results.
;;  /
;; typedef enum
;; (
;;  GDK-IMAGE-NORMAL,
;;  GDK-IMAGE-SHARED,
;;  GDK-IMAGE-FASTEST
;; ) GdkImageType

;; / Types of visuals.
;;  StaticGray:
;;  Grayscale:
;;  StaticColor:
;;  PseudoColor:
;;  TrueColor:
;;  DirectColor:
;;  /
;; typedef enum
;; (
;;  GDK-VISUAL-STATIC-GRAY,
;;  GDK-VISUAL-GRAYSCALE,
;;  GDK-VISUAL-STATIC-COLOR,
;;  GDK-VISUAL-PSEUDO-COLOR,
;;  GDK-VISUAL-TRUE-COLOR,
;;  GDK-VISUAL-DIRECT-COLOR
;; ) GdkVisualType

;; / Window attribute mask values.
;;  GDK-WA-TITLE: The "title" field is valid.
;;  GDK-WA-X: The "x" field is valid.
;;  GDK-WA-Y: The "y" field is valid.
;;  GDK-WA-CURSOR: The "cursor" field is valid.
;;  GDK-WA-COLORMAP: The "colormap" field is valid.
;;  GDK-WA-VISUAL: The "visual" field is valid.
;;  /
;; typedef enum
;; (
;;  GDK-WA-TITLE = 1 << 1,
;;  GDK-WA-X = 1 << 2,
;;  GDK-WA-Y = 1 << 3,
;;  GDK-WA-CURSOR = 1 << 4,
;;  GDK-WA-COLORMAP = 1 << 5,
;;  GDK-WA-VISUAL = 1 << 6
;; ) GdkWindowAttributesType

;; / Size restriction enumeration.
;;  /
;; typedef enum
;; (
;;  GDK-MIN-SIZE = 1 << 1,
;;  GDK-MAX-SIZE = 1 << 2
;; ) GdkWindowSize

;; / GC function types.
;;  Copy: Overwrites destination pixels with the source pixels.
;;  Invert: Inverts the destination pixels.
;;  Xor: Xor's the destination pixels with the source pixels.
;;  /
;; typedef enum
;; (
;;  GDK-COPY,
;;  GDK-INVERT,
;;  GDK-XOR
;; ) GdkFunction

;; / GC fill types.
;;  Solid:
;;  Tiled:
;;  Stippled:
;;  OpaqueStippled:
;;  /
;; typedef enum
;; (
;;  GDK-SOLID,
;;  GDK-TILED,
;;  GDK-STIPPLED,
;;  GDK-OPAQUE-STIPPLED
;; ) GdkFill

;; / GC line styles
;;  Solid:
;;  OnOffDash:
;;  DoubleDash:
;;  /
;; typedef enum
;; (
;;  GDK-LINE-SOLID,
;;  GDK-LINE-ON-OFF-DASH,
;;  GDK-LINE-DOUBLE-DASH
;; ) GdkLineStyle

;; / GC cap styles
;;  CapNotLast:
;;  CapButt:
;;  CapRound:
;;  CapProjecting:
;;  /
;; typedef enum
;; (
;;  GDK-CAP-NOT-LAST,
;;  GDK-CAP-BUTT,
;;  GDK-CAP-ROUND,
;;  GDK-CAP-PROJECTING
;; ) GdkCapStyle

;; / GC join styles
;;  JoinMiter:
;;  JoinRound:
;;  JoinBevel:
;;  /
;; typedef enum
;; (
;;  GDK-JOIN-MITER,
;;  GDK-JOIN-ROUND,
;;  GDK-JOIN-BEVEL
;; ) GdkJoinStyle

;; / Cursor types.
;;  /
;; typedef enum
;; (
;;  GDK-LEFT-ARROW,
;;  GDK-RIGHT-ARROW,
;;  GDK-TEXT-CURSOR,
;;  GDK-DIRECTIONAL,
;;  GDK-PENCIL,
;;  GDK-CROSS,
;;  GDK-TCROSS,
;;  GDK-FLEUR,
;;  GDK-BI-ARROW-HORZ,
;;  GDK-BI-ARROW-VERT
;; ) GdkCursorType

;; / Event types.
;;  Nothing: No event occurred.
;;  Delete: A window delete event was sent by the window manager.
;;   The specified window should be deleted.
;;  Destroy: A window has been destroyed.
;;  Expose: Part of a window has been uncovered.
;;  MotionNotify: The mouse has moved.
;;  ButtonPress: A mouse button was pressed.
;;  ButtonRelease: A mouse button was release.
;;  KeyPress: A key was pressed.
;;  KeyRelease: A key was released.
;;  EnterNotify: A window was entered.
;;  LeaveNotify: A window was exited.
;;  FocusChange: The focus window has changed. (The focus window gets
;;  keyboard events).
;;  Resize: A window has been resized.
;;  Map: A window has been mapped. (It is now visible on the screen).
;;  Unmap: A window has been unmapped. (It is no longer visible on
;;   the screen).
;;  /
;; typedef enum
;; (
;;  GDK-NOTHING = -1,
;;  GDK-DELETE = 0,
;;  GDK-DESTROY = 1,
;;  GDK-EXPOSE = 2,
;;  GDK-MOTION-NOTIFY = 3,
;;  GDK-BUTTON-PRESS = 4,
;;  GDK-2BUTTON-PRESS = 5,
;;  GDK-3BUTTON-PRESS = 6,
;;  GDK-BUTTON-RELEASE = 7,
;;  GDK-KEY-PRESS = 8,
;;  GDK-KEY-RELEASE = 9,
;;  GDK-ENTER-NOTIFY = 10,
;;  GDK-LEAVE-NOTIFY = 11,
;;  GDK-FOCUS-CHANGE = 12,
;;  GDK-RESIZE = 13,
;;  GDK-MAP = 14,
;;  GDK-UNMAP  = 15
;; ) GdkEventType

;; / Event masks. (Used to select what types of events a window
;;  will receive).
;;  /
;; typedef enum
;; (
;;  GDK-EXPOSURE-MASK = 1 << 1,
;;  GDK-POINTER-MOTION-MASK = 1 << 2,
;;  GDK-POINTER-MOTION-HINT-MASK = 1 << 3,
;;  GDK-BUTTON-MOTION-MASK = 1 << 4,
;;  GDK-BUTTON1-MOTION-MASK = 1 << 5,
;;  GDK-BUTTON2-MOTION-MASK = 1 << 6,
;;  GDK-BUTTON3-MOTION-MASK = 1 << 7,
;;  GDK-BUTTON-PRESS-MASK = 1 << 8,
;;  GDK-BUTTON-RELEASE-MASK = 1 << 9,
;;  GDK-KEY-PRESS-MASK = 1 << 10,
;;  GDK-KEY-RELEASE-MASK = 1 << 11,
;;  GDK-ENTER-NOTIFY-MASK = 1 << 12,
;;  GDK-LEAVE-NOTIFY-MASK = 1 << 13,
;;  GDK-FOCUS-CHANGE-MASK = 1 << 14,
;;  GDK-STRUCTURE-MASK = 1 << 15,
;;  GDK-ALL-EVENTS-MASK  = 0xFFFF
;; ) GdkEventMask

;; / Types of enter/leave notifications.
;;  Ancestor:
;;  Virtual:
;;  Inferior:
;;  Nonlinear:
;;  NonlinearVirtual:
;;  Unknown: An unknown type of enter/leave event occurred.
;;  /
;; typedef enum
;; (
;;  GDK-NOTIFY-ANCESTOR  = 0,
;;  GDK-NOTIFY-VIRTUAL = 1,
;;  GDK-NOTIFY-INFERIOR  = 2,
;;  GDK-NOTIFY-NONLINEAR = 3,
;;  GDK-NOTIFY-NONLINEAR-VIRTUAL = 4,
;;  GDK-NOTIFY-UNKNOWN = 5
;; ) GdkNotifyType

;; / Types of modifiers.
;;  /
;; typedef enum
;; (
;;  GDK-SHIFT-MASK = 1 << 0,
;;  GDK-LOCK-MASK = 1 << 1,
;;  GDK-CONTROL-MASK = 1 << 2,
;;  GDK-MOD1-MASK = 1 << 3,
;;  GDK-MOD2-MASK = 1 << 4,
;;  GDK-MOD3-MASK = 1 << 5,
;;  GDK-MOD4-MASK = 1 << 6,
;;  GDK-MOD5-MASK = 1 << 7,
;;  GDK-BUTTON1-MASK = 1 << 8,
;;  GDK-BUTTON2-MASK = 1 << 9,
;;  GDK-BUTTON3-MASK = 1 << 10,
;;  GDK-BUTTON4-MASK = 1 << 11,
;;  GDK-BUTTON5-MASK = 1 << 12
;; ) GdkModifierType

;; typedef enum
;; (
;;  GDK-CLIP-BY-CHILDREN = 0,
;;  GDK-INCLUDE-INFERIORS = 1
;; ) GdkSubwindowMode

;; typedef enum
;; (
;;  GDK-INPUT-READ = 1 << 0,
;;  GDK-INPUT-WRITE = 1 << 1,
;;  GDK-INPUT-EXCEPTION = 1 << 2
;; ) GdkInputCondition

;; typedef enum
;; (
;;  GDK-OK = 0,
;;  GDK-ERROR = -1,
;;  GDK-ERROR-PARAM = -2,
;;  GDK-ERROR-FILE = -3,
;;  GDK-ERROR-MEM = -4
;; ) GdkStatus

;; typedef enum
;; (
;;  GDK-LSB-FIRST,
;;  GDK-MSB-FIRST
;; ) GdkByteOrder


;; typedef void (GdkInputFunction) (gpointer data,
;; 				 gint source,
;; 				 GdkInputCondition condition)

;; / The color type.
;;  A color consists of red, green and blue values in the
;;  range 0-65535 and a pixel value. The pixel value is highly
;;  dependent on the depth and colormap which this color will
;;  be used to draw into. Therefore, sharing colors between
;;  colormaps is a bad idea.
;;  /
;; struct -GdkColor
;; (
;;  guint32 pixel
;;  guint16 red
;;  guint16 green
;;  guint16 blue
;; )

;; / The colormap type.
;;  Colormaps consist of 256 colors.
;;  /
;; struct -GdkColormap
;; (
;;  GdkColor colors[256]
;; )

;; / The visual type.
;;  "type" is the type of visual this is (PseudoColor, TrueColor, etc).
;;  "depth" is the bit depth of this visual.
;;  "colormap-size" is the size of a colormap for this visual.
;;  "bits-per-rgb" is the number of significant bits per red, green and blue.
;;  The red, green and blue masks, shifts and precisions refer
;;  to value needed to calculate pixel values in TrueColor and DirectColor
;;  visuals. The "mask" is the significant bits within the pixel. The
;;  "shift" is the number of bits left we must shift a primary for it
;;  to be in position (according to the "mask"). "prec" refers to how
;;  much precision the pixel value contains for a particular primary.
;;  /
;; struct -GdkVisual
;; (
;;  GdkVisualType type
;;  gint depth
;;  gint colormap-size
;;  gint bits-per-rgb

;;  guint32 red-mask
;;  gint red-shift
;;  gint red-prec

;;  guint32 green-mask
;;  gint green-shift
;;  gint green-prec

;;  guint32 blue-mask
;;  gint blue-shift
;;  gint blue-prec
;; )

;; struct -GdkWindowAttr
;; (
;;  gchar title
;;  gint event-mask
;;  gint16 x, y
;;  gint16 width
;;  gint16 height
;;  GdkWindowClass wclass
;;  GdkVisual visual
;;  GdkColormap colormap
;;  GdkWindowType window-type
;;  GdkCursor cursor
;; )

;; struct -GdkWindow
;; (
;;  GdkWindowType window-type
;;  GdkVisual visual
;;  GdkColormap colormap

;;  gint16 x
;;  gint16 y
;;  guint16 width
;;  guint16 height
;;  gint16 depth

;;  GdkWindow parent
;;  GdkWindow children
;;  GdkWindow next-sibling
;;  GdkWindow prev-sibling

;;  gpointer user-data
;; )

;; struct -GdkImage
;; (
;;  GdkImageType type
;;  GdkVisual visual / visual used to create the image /
;;  GdkByteOrder byte-order
;;  guint16 width
;;  guint16 height
;;  guint16 depth
;;  guint16 bpp / bytes per pixel /
;;  guint16 bpl / bytes per line /
;;  gpointer mem
;; )

;; struct -GdkGC
;; (
;;  GdkColor foreground
;;  GdkColor background
;;  GdkFont font
;;  GdkFunction function
;;  GdkFill  fill
;;  GdkPixmap tile
;;  GdkPixmap stipple
;;  GdkSubwindowMode subwindow-mode
;;  gint graphics-exposures
;; )

;; struct -GdkPoint
;; (
;;  gint16 x
;;  gint16 y
;; )

;; struct -GdkRectangle
;; (
;;  gint16 x
;;  gint16 y
;;  guint16 width
;;  guint16 height
;; )

;; struct -GdkSegment
;; (
;;  gint16 x1
;;  gint16 y1
;;  gint16 x2
;;  gint16 y2
;; )

;; struct -GdkFont
;; (
;;  gint ascent
;;  gint descent
;; )

;; struct -GdkCursor
;; (
;;  GdkCursorType type
;; )

;; struct -GdkEventAny
;; (
;;  GdkEventType type
;;  GdkWindow window
;;  gint8 send-event
;; )

;; struct -GdkEventExpose
;; (
;;  GdkEventType type
;;  GdkWindow window
;;  GdkRectangle area
;; )

;; struct -GdkEventMotion
;; (
;;  GdkEventType type
;;  GdkWindow window
;;  guint32 time
;;  gint16 x
;;  gint16 y
;;  guint state
;;  gint16 is-hint
;; )

;; struct -GdkEventButton
;; (
;;  GdkEventType type
;;  GdkWindow window
;;  guint32 time
;;  gint16 x
;;  gint16 y
;;  guint state
;;  guint button
;; )

;; struct -GdkEventKey
;; (
;;  GdkEventType type
;;  GdkWindow window
;;  guint32 time
;;  guint state
;;  guint keyval
;; )

;; struct -GdkEventCrossing
;; (
;;  GdkEventType type
;;  GdkWindow window
;;  GdkWindow subwindow
;;  GdkNotifyType detail
;; )

;; struct -GdkEventFocus
;; (
;;  GdkEventType type
;;  GdkWindow window
;;  gint16 in
;; )

;; struct -GdkEventResize
;; (
;;  GdkEventType type
;;  GdkWindow window
;;  gint16 width
;;  gint16 height
;; )

;; union -GdkEvent
;; (
;;  GdkEventType type
;;  GdkEventAny any
;;  GdkEventExpose expose
;;  GdkEventMotion motion
;;  GdkEventButton button
;;  GdkEventKey key
;;  GdkEventCrossing crossing
;;  GdkEventFocus focus-change
;;  GdkEventResize resize
;; )
