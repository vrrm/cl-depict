(in-package :cl-depict)

(defun process-events-function (display channel)
  (loop for event =
       (xlib:process-event display
			   :handler (lambda (&rest args)  args)
			   :discard-p t)
     while event
     do (chanl:send channel event)))


(defun init-event-handler (display)
  (let ((channel (make-instance 'chanl:unbounded-channel)))
    (bordeaux-threads:make-thread #'
     (lambda ()
       (process-events-function display channel)))
    channel))



;;; (setf event (chanl:recv *channel* :blockp nil))
