(in-package :cl-depict)

(defun xdk-window-init ()
  "Set root window (which in X already exists.  Set up anything else
   needed to create and manage windows."
  (setf *root-window* (make-instance 'window :window-of
				     (xlib:screen-root *screen*))))

;; (defun create-top-window (width height &key (background-color 'black))
;;   (xlib:create-window
;;    :parent *root-window*
;;    :x 0
;;    :y 0
;;    :width width
;;    :height height
;;    :background (get-background background-color)
;;    :event-mask (xlib:make-event-mask :key-press
;; 				     :button-press)))

(defclass window ()
  ((window-of :initarg :window-of :accessor window-of)))

(defmethod create-window ((parent window) width height x y &rest keyword-pairs
			  &key (border 'white) (background 'black)
			    &allow-other-keys)
  (let ((window (apply 'xlib:create-window
		       :parent (window-of parent)
		       :x x
		       :y y
		       :width width
		       :height height
		       :border (get-background border)
		       :background (get-background background)
		       :event-mask (xlib:make-event-mask :button-press)
		       :allow-other-keys t
		       keyword-pairs)))
    (make-instance 'window
		   :window-of window
		   :gc (xlib:create-gcontext
			:drawable window
			:background (xlib:screen-black-pixel *screen*)
			:foreground (xlib:screen-white-pixel *screen*)
			:line-width 0 :line-style :solid))))

(defmethod create-drawable-window ((parent window) height width x y)
  (let* ((drawable-window (create-window parent height width x y)))
    (with-slots (window-of) drawable-window
      (xlib:map-window window-of)
      (xlib:display-force-output *display*))))

(defmethod destroy-window ((w window))
  (with-slots (window-of gc) w
    (xlib:free-gcontex gc)
    (xlib:destroy-window window-of)
    (xlib:display-force-output *display*)))

(defmethod window-show ((w window))
  (xlib:map-window (window-of w))
  (xlib:display-force-output *display*))

(defmethod window-hide ((w window))
  (xlib:unmap-window (window-of w))
  (xlib:display-force-output *display*))

(defmethod window-move ((w window) x y)
  (with-slots (window-of) w
    (setf (xlib:drawable-x window-of) x)
    (setf (xlib:drawable-y window-of) y)
    (xlib:display-force-output *display*)))

(defun get-background (color)
  (cond ((eq color 'black) (xlib:screen-black-pixel *screen*))
	((eq color 'white) (xlib:screen-white-pixel *screen*))
	((member color '(green blue red))
	 (xlib:alloc-color (xlib:window-colormap *root-window*) color))
	(t (error "Invalid color specified for get-background: ~s" color))))

(defvar top-window nil)

(defun create-app-window (height width x y)
  (let ((app-window (create-window *root-window* height width x y)))
    (with-slots (window-of) app-window
      (xlib:map-window window-of)
      (xlib:display-force-output *display*)
      app-window)))

(defun test-create-app-window ()
  (unless *display-connection-state*
    (display-init))
  (setf top-window (create-app-window 400 400 0 0)))


(defmethod draw-line ((w window) x1 y1 x2 y2)
  (with-slots (window-of gc) w
    (xlib:draw-line window-of gc x1 y1 x2 y2)
    (xlib:display-force-output *display*)))

(defmethod draw-polygon ((w window) path)
  (when (> (lenth path) 1)
    (let* ((from-point (car path))
	   (from-x (car from-point))
	   (from-y (cadr from-point))
	   (path (cdr path)))
      (dolist (from-point path)
	(let ((to-x (car to-point))
	      (to-y (cadr to-point)))
  (with-slots (window-of gc) w
    (xlib:draw-line window-of gc x1 y1 x2 y2)
    (xlib:display-force-output *display*)))


(defclass window ()
  ((window-of :initarg :window-of :accessor window-of)
   (gc :initarg :gc :accessor gc)))

(defun test-draw-line ()
  (let ((gc (xlib:create-gcontext :drawable (window-of top-window)
				 :background (xlib:screen-black-pixel *screen*)
				 :foreground (xlib:screen-white-pixel *screen*)
				 :line-width 0 :line-style :solid)))
    (xlib:draw-line (window-of top-window) gc 10 10 50 50)
    (xlib:display-force-output *display*)))



;; (defun create-window (&optional parent)
;;   "If no parent is specified, then the window is a top-window, replacing the existing one."
;;   (unless parent
;;     (when *top-window*
;;       (xlib:destroy-window *top-window*))
;;     (setf *top-window* (create-top-window 400 400))
;;     (setf parent *top-window*))
;;   (make-instance 'window :window-of (create-child-window parent
;; 							 (truncate width 4)
;; 							 (truncate height 4)
;; 							 across
;; 							 down
;; 							 :background 'blue
;; 							 :border-width 5
;; 							 :border 'white)))


;; void gdk-window-reparent (GdkWindow window,
;; 				 GdkWindow new-parent,
;; 				 gint x,
;; 				 gint y)
;; void gdk-window-clear (GdkWindow window)
;; void gdk-window-clear-area (GdkWindow window,
;; 				 gint x,
;; 				 gint y,
;; 				 gint width,
;; 				 gint height)
;; void gdk-window-raise (GdkWindow window)
;; void gdk-window-lower (GdkWindow window)

;; void gdk-window-set-user-data (GdkWindow window,
;; 				 gpointer user-data)
;; void gdk-window-set-size (GdkWindow window,
;; 				 gint width,
;; 				 gint height)
;; void gdk-window-set-sizes (GdkWindow window,
;; 				 gint min-width,
;; 				 gint min-height,
;; 				 gint max-width,
;; 				 gint max-height,
;; 				 gint flags)
;; void gdk-window-set-position (GdkWindow window,
;; 				 gint x,
;; 				 gint y)
;; void gdk-window-set-title (GdkWindow window,
;; 				 gchar  title)
;; void gdk-window-set-background (GdkWindow window,
;; 				 GdkColor color)
;; void gdk-window-set-cursor (GdkWindow window,
;; 				 GdkCursor cursor)
;; void gdk-window-set-colormap (GdkWindow window,
;; 				 GdkColormap colormap)
;; void gdk-window-get-user-data (GdkWindow window,
;; 				 gpointer data)
;; gint gdk-window-get-origin (GdkWindow window,
;; 				 gint x,
;; 				 gint y)
;; GdkWindow gdk-window-get-pointer (GdkWindow window,
;; 				 gint x,
;; 				 gint y,
;; 				 GdkModifierType mask)
;; GdkWindow gdk-window-get-parent (GdkWindow window)
;; GdkWindow gdk-window-get-toplevel (GdkWindow window)
