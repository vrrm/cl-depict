(in-package :cl-depict)

(defclass gtk-window-private (#-scl fundamental-character-output-stream
                                 #+scl ext:character-output-stream)
  (
   ;; (gdk-window    :initarg :mark
   ;;               :initform nil
   ;;               :accessor random-typeout-stream-mark
   ;;               :documentation "The buffer point of the associated buffer.")
   (x-window       :initarg :window
                 :initform nil
                 :accessor random-typeout-stream-window
                 :documentation "The hemlock window all this shit is in.")
   (x-display    :initarg :more-mark
                 :initform nil
                 :accessor random-typeout-stream-more-mark
                 :documentation "The mark that is not displayed when we need to more.")
   (destroyed-p    :initarg :no-prompt
                 :initform nil
                 :accessor random-typeout-stream-no-prompt
                 :documentation "T when we want to exit, still collecting output.")))

(defclass gdk-window ()
  ((window-type    :initarg :mark
		   :initform nil
		   :accessor random-typeout-stream-mark
		   :documentation "The buffer point of the associated buffer.")
   (visual    :initarg :mark
	      :initform nil
	      :accessor random-typeout-stream-mark
	      :documentation "The buffer point of the associated buffer.")
   (colormap    :initarg :mark
		:initform nil
		:accessor random-typeout-stream-mark
		:documentation "The buffer point of the associated buffer.")
   (x    :initarg :x
	 :accessor x
	 :documentation "x coordinate ocation of the window.")
   (y    :initarg :y
	 :accessor y
	 :documentation "x coordinate ocation of the window.")
   (width    :initarg :width
	     :accessor width
	     :documentation "The buffer point of the associated buffer.")
   (height    :initarg :height
	      :accessor height
	      :documentation "The buffer point of the associated buffer.")
   (background :initarg :background
	       :accessor background
	       :documentation "The buffer point of the associated buffer.")
   (depth    :initarg :mark
	     :initform nil
	     :accessor random-typeout-stream-mark
	     :documentation "The buffer point of the associated buffer.")
   (parent    :initarg :mark
	      :initform nil
	      :accessor random-typeout-stream-mark
	      :documentation "The buffer point of the associated buffer.")
   (children    :initarg :mark
		:initform nil
		:accessor random-typeout-stream-mark
		:documentation "The buffer point of the associated buffer.")
   (next-sibling    :initarg :mark
		    :initform nil
		    :accessor random-typeout-stream-mark
		    :documentation "The buffer point of the associated buffer.")
   (prev-sibling    :initarg :mark
		    :initform nil
		    :accessor random-typeout-stream-mark
		    :documentation "The buffer point of the associated buffer.")
   (user-data    :initarg :mark
                 :initform nil
                 :accessor random-typeout-stream-mark
                 :documentation "The buffer point of the associated buffer.")
   (need-resize :documentation "whether line buffered") ))

(defvar width 300)
(defvar host "")
;; (defvar *gdk-top-window*
;;   (make-instance 'gdk-window
;; 		 :background (xlib:screen-black-pixel screen)
;; 		 :width 150
;; 		 :height 150
;; 		 :x 0
;; 		 :y 0))

(defun gdk-make-window (screen background &optional  (width 0) (height 0)  (x 0)  (y 0))
    (make-instance 'gdk-window
		   :background background
		   :width 300
		   :height 300
		   :x 0
		   :y 0))

(defun x-window-new (root-window screen bg &optional  (width 0) (height 0)  (x 0)  (y 0))
  (let ((window  (gdk-make-window screen   width width x y )))
    (xlib:create-window
     :parent root-window
     :x (x window)
     :y (y window)
     :width (width  window)
     :height (height  window)
     :background (background  window)
     :event-mask (xlib:make-event-mask :key-press
				       :button-press))))

(defun graphic-x (width height across down &optional (host ""))
  (let* (
	 (display (xlib:open-display host))
	 (screen (first (xlib:display-roots display)))
	 (black (xlib:screen-black-pixel screen))
	 (white (xlib:screen-white-pixel screen))
	 (root-window (xlib:screen-root screen))
	 (green (xlib:alloc-color
		 (xlib:window-colormap root-window)
		 'green))
	 (blue (xlib:alloc-color
		(xlib:window-colormap root-window)
		'blue))
	 (red (xlib:alloc-color
	       (xlib:window-colormap root-window)
	       'red))
	 (bg (funcall 'xlib:screen-black-pixel screen))

	 (top-window (gdk-window-new root-window screen bg 300 300 0 0))
	 (red-window (gdk-window-new top-window screen red (truncate width 4) (truncate height 4) 0 0))
	   ;; (red-window (xlib:create-window
	   ;; 		:parent top-window
	   ;; 		:width (truncate width 4)
	   ;; 		:height (truncate height 4)
	   ;; 		:x across
	   ;; 		:y 0
	   ;; 		:background red
	   ;; 		:event-mask (xlib:make-event-mask :button-press)))
	   (green-window (xlib:create-window
			  :parent top-window
			  :x 0
			  :y down
			  :width (truncate width 4)
			  :height (truncate height 4)
			  :background green
			  :event-mask (xlib:make-event-mask :button-press)))
	   (blue-window (xlib:create-window
			 :parent top-window
			 :x across
			 :y down
			 :width (truncate width 4)
			 :height (truncate height 4)
			 :background blue
			 :border-width 5
			 :border white
			 :event-mask (xlib:make-event-mask :button-press))))
	 (xlib:map-window top-window)
	 (xlib:map-window red-window)
	 (xlib:map-window green-window)
	 (xlib:map-window blue-window)
	 (xlib:event-case (display :force-output-p t
				   :discard-p t)
	   (:button-press (window)
			  (cond ((eq window red-window)
				 (xlib:destroy-window red-window)
				 nil)
				((eq window green-window)
				 (xlib:destroy-window blue-window)
				 nil)
				((eq window blue-window)
				 (xlib:destroy-window green-window)
				 nil)
				(t t)))
	   (:key-press ()
		       (xlib:circulate-window-down top-window)
		       nil))
	 (xlib:destroy-window top-window)
	 (xlib:close-display display)))



  ;; private->xwindow = XCreateWindow (private->xdisplay, xparent,
  ;; 				    window->x, window->y,
  ;; 				    window->width, window->height,
  ;; 				    0, window->depth, class, xvisual,
  ;; 				    xattributes_mask, &xattributes);


(defun test-graphic-x ()
  (graphic-x 300 300 150 150))

(defun gtk-window-show (window)
)


;; struct
;; {
;;   GtkContainer container;
;;   gchar *title;
;;   GtkWindowType type;
;;   GtkWidget *child;
;;   GtkWidget *focus_widget;
;;   GtkWidget *default_widget;
;;   gint need_resize;
;;   GList *accelerator_tables;
;;   GtkWindowResizeHook resize;
;; };

	 ;; (top-window (xlib:create-window
	 ;; 	     :parent root-window
	 ;; 	     :x 0
	 ;; 	     :y 0
	 ;; 	     :width width
	 ;; 	     :height height
	 ;; 	     :background black
	 ;; 	     :event-mask (xlib:make-event-mask :key-press
	 ;; 					       :button-press)))



;; struct _GdkWindow
;; {
;;   GdkWindowType  window_type;
;;   GdkVisual     *visual;
;;   GdkColormap   *colormap;
;;   gint16   x;
;;   gint16   y;
;;   guint16  width;
;;   guint16  height;
;;   gint16   depth;
;;   GdkWindow  *parent;
;;   GdkWindow  *children;
;;   GdkWindow  *next_sibling;
;;   GdkWindow  *prev_sibling;
;;   gpointer user_data;
;; };


;; struct _GdkWindowPrivate
;; {
;;   GdkWindow window;
;;   Window xwindow;
;;   Display *xdisplay;
;;   unsigned int destroyed : 1;
;; };







;; gdk-window-new
;; gdk-window-destroy
;; gdk-window-show
;; gdk-window-hide
;; gdk-window-move
;; gdk-window-reparent
;; gdk-window-clear
;; gdk-window-clear-area
;; gdk-window-raise
;; gdk-window-lower
;; gdk-window-set-user-data
;; gdk-window-set-size
;; gdk-window-set-sizes
;; gdk-window-set-position
;; gdk-window-set-title
;; gdk-window-set-background
;; gdk-window-set-cursor
;; gdk-window-set-colormap
;; gdk-window-get-user-data
;; gdk-window-get-origin
;; gdk-window-get-pointer
;; gdk-window-get-parent
;; gdk-window-get-toplevel
;; gdk-cursor-new
;; gdk-cursor-destroy
